#version 330 core

layout(location = 0) in vec2 UV;

uniform vec2 pos;
uniform vec2 size;

out vec2 fUV;

void main() {
	fUV = UV;
	gl_Position = vec4((UV * size + pos) * 2 - 1, 0, 1);
}