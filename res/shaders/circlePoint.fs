#version 330 core

out vec4 color;

uniform sampler2D tex;

uniform vec4 uColor = vec4(1,1,1,1);

void main() {
	color = texture(tex,gl_PointCoord) * uColor;
}