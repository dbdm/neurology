#version 330 core

layout(location = 0) in vec2 vPos;

uniform vec2 pos;
uniform vec2 size;

void main() {
	gl_Position = vec4((vPos * size + pos) * 2 - 1, 0, 1);
}