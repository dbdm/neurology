#include "glc_cu.h"
#include "sdlc.h"
#include <iostream>

#include <cuda_gl_interop.h>
#include <vector>
#include <string>
#include <fstream>

#include "matmath_cu.h"

namespace neuro {

	#define handleGLError() mGL->handle_error(glGetError(), __FILE__, __LINE__);

	GLController* mGL;

	Texture::~Texture() {
		glDeleteTextures(1, &tex);
	}

	CudaTexture::~CudaTexture() {
		delete tex;
		cudaGraphicsUnregisterResource(texGraphicsResource);
	}

	Program::~Program() {
		glDeleteProgram(ID);
	}

	void GLController::bindTexture(Texture * t, int channel) {
		glActiveTexture(GL_TEXTURE0 + channel);
		glBindTexture(t->DEFAULT_BINDING_LOCATION, t->tex);
	}

	bool GLController::handle_error(GLenum error, const char * func, int line) {
		if (error != 0 && error != 1280) { // Don't return opengl INVALID_ENUM errors because they don't mean anything
			std::cout << "OpenGL Error: In " << func << " on line " << line << ": " << gluErrorString(error) << " (" << error << ")\n" << std::endl;
			return true;
		}
		return false;
	}

	CudaTexture* GLController::allocCudaTexture(glm::ivec2 size) {
		CudaTexture* ret = new CudaTexture();
		ret->tex = new Texture();

		// Create the texture
		glGenTextures(1, &(ret->tex->tex));
		glBindTexture(GL_TEXTURE_2D, ret->tex->tex);

 		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, size.x, size.y, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);

		ret->tex->size = size;

		cudaGraphicsGLRegisterImage(&(ret->texGraphicsResource), ret->tex->tex, GL_TEXTURE_2D, cudaGraphicsMapFlagsNone); // register the pixel buffer object with CUDA

		return ret;
	}

	__device__ float char2Float(unsigned char in) { return ((float) in) / 255.f; }

	// copy the data over from idata to odata
	__global__ void copy_kernel(cudaSurfaceObject_t odata, unsigned char* idata, int width, int height) {
		int index = threadIdx.x + blockIdx.x * blockDim.x;
		int x = index/height;
		int y = index%height;

		int iindex = 4*index;
		
		if(x < width) {
			uchar4 data = make_uchar4(idata[iindex],idata[iindex+1],idata[iindex+2],idata[iindex+3]);
			surf2Dwrite(data, odata, x * sizeof(uchar4), height-y-1);
		}
	}

	void GLController::uploadData(glm::u8vec4* data, CudaTexture* tex) {
		cudaGraphicsMapResources(1, &(tex->texGraphicsResource));

		cudaArray_t viewCudaArray;

		cudaGraphicsSubResourceGetMappedArray(&viewCudaArray, (tex->texGraphicsResource), 0, 0);

		cudaResourceDesc viewCudaArrayResourceDesc;
		viewCudaArrayResourceDesc.resType = cudaResourceTypeArray;
		viewCudaArrayResourceDesc.res.array.array = viewCudaArray;

		cudaSurfaceObject_t viewCudaSurfaceObject;

		cudaCreateSurfaceObject(&viewCudaSurfaceObject, &viewCudaArrayResourceDesc);

		// Now that the stream is mapped to the graphics resource,
		// And that we have extracted the texture's life energy,
		// And have sacrificed it's soul to Q̾ͧ̏̉̇̍͆̎̆ͧͬ̚͏҉̢̛͇̙̥̥̼̻̻̫̫͖͎̘̤͍̠̬t̸̢̢̙̲̥̘̩̫̦̻̙͎̥̤̼͕͙̙̝́͗ͫ͌͋̽͂̍̓͊̆̔̽͘͝,̶̧͕̜͕̫̩͍͇̲̹̩͔̒̔̿͌̔͋ͤ̀́́͟͠h̷̛̗̦̳̖̮͇͎̪̪͍͕͗̾̇̎̃ͬ͂͂ͯ̅͌̀̂̾́̚ͅủ̵̧̧̖̤̙̱͎̣̱͓͍͚̝̻̘͍͐̚̚̕͟a̵ͭͧ̿͗͌͋͑̋̌ͥͫͣ̇̓҉̢҉̨̥͚͉̜̰̺̞͚'̴̧̛͚͈͍̟̫̜̳͚̭̺̠͍̖̟̺͑́ͣͯ̆̎̔ͧ̂͐̋ͧͣ͜͟ͅḓ̩̮͔̭͈͖̱̟̘̥̩͍̺̺̪͓̯̎̎͗ͥͬ̍͘͜͡ā̾̓̽̐̌̊͋̿̃ͥ̂̽ͣ͞҉͘͏̝̘̞̞ ̷̦̯̣͇̼̮̰̿̂ͤ͂ͥͨ͋̊ͭ̎̃ͧ̾͊̕͟͞t͏,̢he͟ '҉f͜o ̶ r'̵,̷g̀ot͜t;͜e̕n,
		// We can memcpy from CUDA to OpenGL

		// std::cout << viewCudaSurfaceObject << std::endl;
		// std::cout << viewCudaArray << std::endl;
		// std::cout << tex->texGraphicsResource << std::endl;

		int blocksPerGrid = (tex->tex->size.x * tex->tex->size.y + THREADS_PER_BLOCK - 1) / THREADS_PER_BLOCK;
		copy_kernel<<<blocksPerGrid, THREADS_PER_BLOCK>>>(viewCudaSurfaceObject, (unsigned char*) data, tex->tex->size.x, tex->tex->size.y);

		cudaDestroySurfaceObject(viewCudaSurfaceObject);

		cudaGraphicsUnmapResources(1, &(tex->texGraphicsResource));
	}

	void GLController::init() {
		glewExperimental = GL_TRUE;									// OpenGL 3.3 needs this to function
		handle_error(glewInit(), __FUNCTION__, __LINE__);			// Initialize GLEW
		mSDL->handleError(__FUNCTION__, __LINE__);					// Make sure we didn't break anything
		handleGLError();			// It's okay if this returns "invalid enumerant"

		// clear to white
		glClearColor(0.8f, 1.f, 1.f, 1.f);

		glEnable(GL_TEXTURE_2D);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		/* End various initialization stuff */

		handleGLError();

		glGenVertexArrays(1, &vAID);
		glBindVertexArray(vAID);

		// Vertices for a square.
		glm::vec2 squareArrayBufferData[] = {
			{0,0}, {0,1}, {1,1},
			{1,1}, {1,0}, {0,0}
		};

		glGenBuffers(1, &squareArrayBuffer); // Tell openGL to allocate a new buffer
		glBindBuffer(GL_ARRAY_BUFFER, squareArrayBuffer); // Bind the buffer to the location: GL_ARRAY_BUFFER
		glBufferData(GL_ARRAY_BUFFER, 12 * sizeof(float), squareArrayBufferData, GL_STATIC_DRAW); // Upload the squareArrayBufferData to the buffer bound to the location: GL_ARRAY_BUFFER


		renderTextureProgram = createProgram("renderTexture");
		colorRenderProgram = createProgram("colors");
	}

	GLController::~GLController() {

	}

	GLuint Program::getUniformLocation(std::string name) {
		return glGetUniformLocation(ID, name.c_str());
	}

	void GLController::bindProgram(Program* p) {
		glUseProgram(p->ID);
	}

	void GLController::renderTexture(Texture* t, glm::vec2 pos, glm::vec2 size) {
		bindProgram(renderTextureProgram);

		glEnableVertexAttribArray(0);

		bindTexture(t, 0);

		glUniform2fv(renderTextureProgram->getUniformLocation("pos"), 1, &(pos[0]));
		glUniform2fv(renderTextureProgram->getUniformLocation("size"), 1, &(size[0]));
		glUniform1i(renderTextureProgram->getUniformLocation("tex"), 0);

		glBindBuffer(GL_ARRAY_BUFFER, squareArrayBuffer);
		
		glVertexAttribPointer(0,2,GL_FLOAT,GL_FALSE,0,(void*)0);

		glDrawArrays(GL_TRIANGLES, 0, 6);

		glDisableVertexAttribArray(0);
	}





	GLuint loadShaders(const char * vertex_file_path,const char * fragment_file_path) {
		// Create the shaders
		GLuint VertexShaderID = glCreateShader(GL_VERTEX_SHADER);
		GLuint FragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);

		// Read the Vertex Shader code from the file
		std::string VertexShaderCode;
		std::ifstream VertexShaderStream(vertex_file_path, std::ios::in);
		if(VertexShaderStream.is_open()){
			std::string Line = "";
			while(getline(VertexShaderStream, Line))
				VertexShaderCode += "\n" + Line;
			VertexShaderStream.close();
		}else{
			printf("Impossible to open %s. Are you in the right directory ? Don't forget to read the FAQ !\n", vertex_file_path);
			getchar();
			return 0;
		}

		// Read the Fragment Shader code from the file
		std::string FragmentShaderCode;
		std::ifstream FragmentShaderStream(fragment_file_path, std::ios::in);
		if(FragmentShaderStream.is_open()){
			std::string Line = "";
			while(getline(FragmentShaderStream, Line))
				FragmentShaderCode += "\n" + Line;
			FragmentShaderStream.close();
		}

		GLint Result = GL_FALSE;
		int InfoLogLength;

		// Compile Vertex Shader
		printf("Compiling shader : %s\n", vertex_file_path);
		char const * VertexSourcePointer = VertexShaderCode.c_str();
		glShaderSource(VertexShaderID, 1, &VertexSourcePointer , NULL);
		glCompileShader(VertexShaderID);

		// Check Vertex Shader
		glGetShaderiv(VertexShaderID, GL_COMPILE_STATUS, &Result);
		glGetShaderiv(VertexShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
		if ( InfoLogLength > 0 ){
			std::vector<char> VertexShaderErrorMessage(InfoLogLength+1);
			glGetShaderInfoLog(VertexShaderID, InfoLogLength, NULL, &VertexShaderErrorMessage[0]);
			printf("%s\n", &VertexShaderErrorMessage[0]);
		}

		// Compile Fragment Shader
		printf("Compiling shader : %s\n", fragment_file_path);
		char const * FragmentSourcePointer = FragmentShaderCode.c_str();
		glShaderSource(FragmentShaderID, 1, &FragmentSourcePointer , NULL);
		glCompileShader(FragmentShaderID);

		// Check Fragment Shader
		glGetShaderiv(FragmentShaderID, GL_COMPILE_STATUS, &Result);
		glGetShaderiv(FragmentShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
		if ( InfoLogLength > 0 ) {
			std::vector<char> FragmentShaderErrorMessage(InfoLogLength+1);
			glGetShaderInfoLog(FragmentShaderID, InfoLogLength, NULL, &FragmentShaderErrorMessage[0]);
			printf("%s\n", &FragmentShaderErrorMessage[0]);
		}

		// Link the program
		printf("Linking program\n");
		GLuint ProgramID = glCreateProgram();
		glAttachShader(ProgramID, VertexShaderID);
		glAttachShader(ProgramID, FragmentShaderID);
		glLinkProgram(ProgramID);

		// Check the program
		glGetProgramiv(ProgramID, GL_LINK_STATUS, &Result);
		glGetProgramiv(ProgramID, GL_INFO_LOG_LENGTH, &InfoLogLength);
		if ( InfoLogLength > 0 ){
			std::vector<char> ProgramErrorMessage(InfoLogLength+1);
			glGetProgramInfoLog(ProgramID, InfoLogLength, NULL, &ProgramErrorMessage[0]);
			printf("%s\n", &ProgramErrorMessage[0]);
		}

		glDetachShader(ProgramID, VertexShaderID);
		glDetachShader(ProgramID, FragmentShaderID);
		glDeleteShader(VertexShaderID);
		glDeleteShader(FragmentShaderID);

		return ProgramID;
	}

	Program* GLController::createProgram(std::string name) {
		std::string vPath = "res/shaders/" + name + ".vs";
		std::string fPath = "res/shaders/" + name + ".fs";
		Program* p = new Program();
		p->ID = loadShaders(vPath.c_str(), fPath.c_str());
		return p;
	}

	GraphBuffer::GraphBuffer(int maxPoints, glm::vec2 pos, glm::vec2 size, glm::vec4 lineColor): pos(pos), size(size), lineColor(lineColor) {
		this->maxPoints = maxPoints;
		numPoints = 0;
		buf = 0;
		data = new glm::vec2[maxPoints+1];
		glGenBuffers(1,&buf);
		glBindBuffer(GL_ARRAY_BUFFER, buf);
		glBufferData(GL_ARRAY_BUFFER, 2*maxPoints * sizeof(glm::vec2), data, GL_DYNAMIC_DRAW);
	}

	void GraphBuffer::streamData() {
		glBindBuffer(GL_ARRAY_BUFFER, buf);
		glBufferData(GL_ARRAY_BUFFER, 2*maxPoints * sizeof(glm::vec2), NULL, GL_DYNAMIC_DRAW);
		glBufferSubData(GL_ARRAY_BUFFER, 0, 2*numPoints * sizeof(glm::vec2), data);
	}

	void GraphBuffer::popPoint() {
		numPoints--;
	}

	void GraphBuffer::__pushPoint(float height) {
		glm::vec2 pos = glm::vec2(((float) numPoints+0.5f) / maxPoints,height);
		if(numPoints==0) {
			data[0]=pos;
			data[1]=pos;
		} else {
			data[2*numPoints-1]=pos;
			data[2*numPoints]=pos;
			data[2*numPoints+1]=pos;
		}
		numPoints++;
	}

	void GraphBuffer::pushPoint(float height) {
		__pushPoint(height);
		streamData();
	}

	void GraphBuffer::pushPoints(std::vector<float> points) {
		for(int i = 0; i < points.size(); i++)
			__pushPoint(points[i]);
		streamData();
	}

	void GraphBuffer::draw() {
		mGL->bindProgram(mGL->colorRenderProgram);

		glEnableVertexAttribArray(0);

		glUniform2fv(mGL->colorRenderProgram->getUniformLocation("pos"), 1, &(pos[0]));
		glUniform2fv(mGL->colorRenderProgram->getUniformLocation("size"), 1, &(size[0]));
		glUniform4fv(mGL->colorRenderProgram->getUniformLocation("color"), 1, &(lineColor[0]));

		glBindBuffer(GL_ARRAY_BUFFER, buf);
		
		glVertexAttribPointer(0,2,GL_FLOAT,GL_FALSE,0,(void*)0);

		glDrawArrays(GL_LINES, 0, 2*numPoints);

		glDisableVertexAttribArray(0);
	}


}