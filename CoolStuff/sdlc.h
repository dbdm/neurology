/*
 * sdlcontroller.h
 *
 *  Created on: Mar 22, 2015
 *      Author: Dylan
 */

#ifndef SDLCONTROLLER_H_
#define SDLCONTROLLER_H_

#include <SDL.h>
#include <map>
#include <string>
#include <chrono>

namespace neuro {

	class SDLController;
	extern SDLController * mSDL;

	class SDLController {
	private:
		SDL_Window* window;
		SDL_GLContext glContext;
		bool sdlReady = false;

	public:
		int windowWidth, windowHeight; // The actual width and height of the window
		int viewportWidth, viewportHeight; // The window's screen resolution

		/* If fullscreen is set to true, then windowWidth and windowHeight are overridden */
		SDLController(int viewportWidth, int viewportHeight, int windowWidth, int windowHeight, bool fullScreen, std::string name);
		virtual ~SDLController();

		void sleep(long ms);

		void swapBuffer();

		bool handleEvents();

		void setTitle(const std::string& title);

		bool isSDLInitialized();

		void clean();

		bool handleError(const char * func, int line);
	};
}

#endif