#ifndef GLC_H_
#define GLC_H_

#include <GL/glew.h>
#include <GL/gl.h>
#include <glm/glm.hpp>

#include <cuda_gl_interop.h>
#include <string>
#include <vector>

namespace neuro {

	// Even though everything is black-and-white, we use RGBA for simplicity's sake
	struct Texture {
		GLuint tex; // the pixel buffer allows us to access the texture from CUDA
		glm::ivec2 size;
		int DEFAULT_BINDING_LOCATION = GL_TEXTURE_2D;
	
		~Texture();
	};

	struct CudaTexture {
		Texture* tex;
		cudaGraphicsResource_t texGraphicsResource = 0; // Manages CUDA operations

		~CudaTexture();
	};


	// Very not rigorous program class
	struct Program {
		GLuint ID;

		GLuint getUniformLocation(std::string name);

		~Program();
	};

	// Draw line graphs with this
	struct GraphBuffer {
		int maxPoints, numPoints;
		glm::vec2* data;
		GLuint buf;
		
		glm::vec2 pos, size;
		glm::vec4 lineColor;

		GraphBuffer(int maxPoints, glm::vec2 pos, glm::vec2 size, glm::vec4 lineColor);

		void streamData();
		void __pushPoint(float height);

		void popPoint();
		void pushPoint(float height);
		void pushPoints(std::vector<float> points);

		void draw();
	};

	class GLController {
	private:
		GLuint vAID;
		GLuint squareArrayBuffer; // for drawing, you guessed it, squares

	public:
		Program* renderTextureProgram;
		Program* colorRenderProgram;
		
		void init();

		CudaTexture* allocCudaTexture(glm::ivec2 size); // Allocate a CudaTexture
		void uploadData(glm::u8vec4* data, CudaTexture* tex); // upload the data into the CudaTexture

		// No framebuffer functions, cause they're not really necessary for this application. In fact, all we need to do is to edit texture data, then draw that texture to the screen, along with some other miscellaneous draws.
		void bindTexture(Texture* t, int channel);
		void renderTexture(Texture* t, glm::vec2 pos, glm::vec2 size); // pos and size are [0,1]
		bool handle_error(GLenum error, const char * func, int line);

		Program* createProgram(std::string name);
		void bindProgram(Program* p);

		~GLController();
	};

	extern GLController* mGL;


}



#endif