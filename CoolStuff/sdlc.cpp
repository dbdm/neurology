#include "sdlc.h"

#include <SDL.h>
#include <gl/glew.h>
#include <gl/gl.h>
#include <iostream>

namespace neuro {
	SDLController * mSDL;

	// This will handle any general errors
	bool SDLController::handleError(const char * func, int line) {
		const char * error = SDL_GetError();
		if (*error /* First character */!= '\0') {
			std::cout << "SDL ERROR: In " << func << " on line " << line << ": " << error;

			SDL_ClearError();
			return true;
		}

		return false;
	}

		// Create our window, and the GL context
	SDLController::SDLController(int viewportWidth, int viewportHeight, int windowWidth, int windowHeight, bool fullScreen, std::string name) {
		this->viewportWidth = viewportWidth;
		this->viewportHeight = viewportHeight;

		// Initialize and show window
		if (SDL_Init(SDL_INIT_EVERYTHING) < 0) {
			std::cout << "SDL could not be initialized" << std::endl;
			return;
		}

		// Use the core profile
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

		// Use OpenGL 3.3 (3.1 is pain incarnate, let's not deal with that)
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
		SDL_GL_SetAttribute(SDL_GL_ACCELERATED_VISUAL, 1);

		// Turn on double buffering
		SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
		SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24); // 24-bit depth buffer (we don't even use the depth buffer?)

		//Create our window with specified width and height
		window = SDL_CreateWindow(name.c_str(), SDL_WINDOWPOS_CENTERED,
		SDL_WINDOWPOS_CENTERED, windowWidth, windowHeight,
				SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL
						| (fullScreen ? SDL_WINDOW_MAXIMIZED | SDL_WINDOW_BORDERLESS : SDL_WINDOW_RESIZABLE));

		if(fullScreen) {
			SDL_DisplayMode display;
			// Get desktop screen resolution
			if(SDL_GetDesktopDisplayMode(0, &display) == 0) {
				this->windowWidth = display.w;
				this->windowHeight = display.h;
			} else {
				handleError(__FUNCTION__, __LINE__);
				// In case of error, use provided dimensions
				this->windowWidth  = windowWidth;
				this->windowHeight = windowHeight;
			}
		}
		else {
			// Use provided dimensions
			this->windowWidth = windowWidth;
			this->windowHeight = windowHeight;
		}

		glContext = SDL_GL_CreateContext(window);	// Add OpenGL context

		handleError(__FUNCTION__, __LINE__);
		SDL_GL_MakeCurrent(window, glContext);
		handleError(__FUNCTION__, __LINE__);

		SDL_GL_SetSwapInterval(0);					// Swap buffers according to monitor's refresh rate
		sdlReady = true;
	}

	SDLController::~SDLController() {
		sdlReady = false;
		SDL_GL_DeleteContext(glContext);
		SDL_DestroyWindow(window);
		SDL_Quit();
	}

	void SDLController::clean() {
		SDL_Event event;	// Discard all false events
		while (SDL_PollEvent(&event)) {}
	}

	void SDLController::sleep(long ms) {
		SDL_Delay(ms);
	}

	void SDLController::swapBuffer() {
		SDL_GL_SwapWindow(window);
	}

	void SDLController::setTitle(const std::string& title) {
		SDL_SetWindowTitle(window, title.c_str());
	}

	bool SDLController::handleEvents() {
		SDL_Event event;

		while (SDL_PollEvent(&event)) {
			switch (event.type) {
				case SDL_QUIT: return false;
			}
		}

		return true;
	}



}