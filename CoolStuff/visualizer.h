#ifndef VISUALIZER_CU_H
#define VISUALIZER_CU_H

#include <SDL.h>

#include <GL/glew.h>
#include <GL/gl.h>
#include <glm/glm.hpp>

// CUDA includes
#include <cuda_runtime.h>
#include <cuda_gl_interop.h>

#include "matmath_cu.h"
#include "glc_cu.h"
#include "neuralnet_cu.h"

namespace neuro {

	// Gives instructions on how to render individual layer
	struct NetVisualizerLayer {
		// The number of the visualization for each layer, columns are automatically calculated
		// For convolutional/pooling layers, this details how many feature layers are rendered per row (if we have 25 feature layers, and this is set to 5, we will render 5x5 feature layers)
		// For fully connected layers, this details how many neurons are rendered per row (if we have 400 neurons, and we set this to 10, we will render a 10x40 rectangle)
		int rows;
		glm::vec2 pos; // relative position of this layer in the entire visualization of this network
		glm::vec2 size; // relative size of this to entire visualization of network
		CudaTexture* tex = nullptr; // We create this texture in parent vizualization

		NetVisualizerLayer(int rows, glm::vec2 pos, glm::vec2 size): rows(rows), pos(pos), size(size) {}
		~NetVisualizerLayer() {}
	};

	struct FeedForwardNeuralNetVisualization {
		GPUFeedForwardNeuralNetwork* net;
		std::vector<NetVisualizerLayer> layerInfo; // There should be a layerInfo for every layer in the network, including the input. To exclude a layer, just make it's size 0.
		
		glm::vec2 pos = glm::vec2(0), size = glm::vec2(1);

		FeedForwardNeuralNetVisualization(GPUFeedForwardNeuralNetwork* net, std::vector<NetVisualizerLayer> layerInfo, glm::vec2 pos, glm::vec2 size);
		void generateVisualization(GPU_MAT X); // create layer textures
		void draw();
	};

	struct ConvolutionalNetVisualization {
		//GPUConvolutionalNeuralNetwork* net;
		GPU_MAT *a, *z;

		std::vector<NetVisualizerLayer> layerInfo;
	
		glm::vec2 pos = glm::vec2(0), size = glm::vec2(1);
	};

	class NetVisualizer {
	public:
		void init();

		void visualizeMatrix(GPU_MAT m, glm::vec2 pos, glm::vec2 size); // draw the matrix to this position on the screen
		void copyMatrixToTexture(GPU_MAT m, CudaTexture* tex); // assumes texture has already been allocated

		void renderLoop();

		int running = true;
	};

	extern NetVisualizer* mNetVisualizer;

}


#endif