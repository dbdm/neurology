#ifndef MATMATH_CU_H
#define MATMATH_CU_H

#include <vector>
#include <cmath>

#include <tuple>
#include <algorithm>
#include <string>

#include <iostream>

#define BLOCK_SIZE 16
#define VECTOR_SIZE 4
#define BATCH_SIZE 10000

#define THREADS_PER_BLOCK 64

#define THREAD_DIM dim3(16,16)


#define MATRIX_ACCESS(M,ROW,COL,ROWS) M[ROW + COL*ROWS]

namespace neuro {

	// Static alternative to dynamic memory management
	class DeviceStack {
	public:
		char* handle; // Where our data begins
		int curHandlePos = 0;
		
		std::vector<int> objects;
		
		DeviceStack(size_t size);
		
		~DeviceStack();
		
		void* malloc(int size) {
			size = size - (size%16) + 16;
			objects.push_back(size);
			curHandlePos += size;
			// std::cout << "size: " << size << " handle: " << curHandlePos << "objects: " << objects.size() << std::endl;
			return (void*) (handle + curHandlePos - size);
		}
		
		template<typename T> T* makeNew(int size) { 
			return (T*) (this->malloc(size*sizeof(T))); 
		}
		
		// pops num objects off the stack (deallocates them)
		void pop(unsigned int num) {
			if(num > objects.size()) for(int j=0;j<100;j++) std::cout << "STACK UNDERFLOW: " << ((int)num)-((int)objects.size()) << std::endl;
			for(unsigned int i = 0; i < num; i++) {
				curHandlePos -= objects.back();
				objects.pop_back();
			}
		}
	};
	
	extern DeviceStack* mDeviceStack;
	
	struct GPU_MAT {
		float* data;
		int rows, cols;

		bool stackAllocated = false;
		
		int size() { return rows * cols; }

		GPU_MAT(int rows, int cols, float* data): rows(rows), cols(cols) {
			this->data = data;
		}
		
		GPU_MAT(int rows, int cols);
		
		// Only use if dynamically allocated
		void dealloc();
		
		float operator()(int row, int col) {
			return data[row + col*rows];
		}
		
		float operator[](int index) { return data[index]; }
		
		GPU_MAT(const GPU_MAT& g): data(g.data), rows(g.rows), cols(g.cols) {}
		GPU_MAT(): data(NULL), rows(0), cols(0) {}
	};
	
	struct CPU_MAT {
		float* data;
		int rows, cols;
		int size() { return rows * cols; }
		
		float operator()(int row, int col) {
			return data[row + col*rows];
		}
		
		CPU_MAT(int rows, int cols, float* data): data(data), rows(rows), cols(cols)  {}
	};

	inline void randInitializeWeights(CPU_MAT in) {
		for(int i = 0; i < in.size(); i++) {
			in.data[i] = (float) ((rand()%512)-256 + (rand()%256)/256.f);
		}
	}

	void copyGPUMATtoCPUMAT(GPU_MAT in, CPU_MAT out);

	void copyCPUMATtoGPUMAT(CPU_MAT in, GPU_MAT out);

	inline void printCPUMAT(CPU_MAT in) {
		for(int i = 0; i < in.rows; i++) {
			for(int j = 0; j < in.cols; j++) {
				std::cout << in(i,j) << " ";
			}
			std::cout << std::endl;
		}
	}

	GPU_MAT stackAllocateGPUMAT(int rows, int cols);


	
	void CUDA_CHAD_INIT();

	template <typename mat>
	void printMatSize(mat& m) {
		std::cout << m.rows << ", " << m.cols << std::endl;
	}

	void runFunctionTests();

	// MATH FUNCTIONS: 

	void multiply_host(GPU_MAT a, GPU_MAT b, GPU_MAT c); // c := a * b

	void clone(GPU_MAT in, GPU_MAT out); // out := in

	void add_host(GPU_MAT a, GPU_MAT b, GPU_MAT c); // c := a + b

	void subtract_host(GPU_MAT a, GPU_MAT b, GPU_MAT c); // c := a - b

	void padOnes_inplace_host(GPU_MAT in); // in(:,0) := ones

	void getRows_host(GPU_MAT in, GPU_MAT out, int start); // out := in(start:start+out.rows,:)

	GPU_MAT getCols_host(GPU_MAT in, int start, int numCols); // return &in(start:start+numCols)

	GPU_MAT getCol_host(GPU_MAT in, int col); // return &in(col)

	void addEquals_host(GPU_MAT a, GPU_MAT b); // a += b

	void subtractEquals_host(GPU_MAT a, GPU_MAT b); // a -= b

	void cwiseProduct_host(GPU_MAT a, GPU_MAT b, GPU_MAT c); // c := a .* b

	void add_host(float a, GPU_MAT b, GPU_MAT c); // c := a + b

	void subtract_host(GPU_MAT a, float b, GPU_MAT c); // c := a - b

	void subtract_host(float a, GPU_MAT b, GPU_MAT c); // c := a - b

	void multiply_host(float a, GPU_MAT b, GPU_MAT c); // c := a * b

	void sigmoid_host(GPU_MAT in, GPU_MAT out); // out := g(in)

	void sigmoidDerivative_host(GPU_MAT in, GPU_MAT out); // out := g'(in)

	void transpose_host(GPU_MAT in, GPU_MAT out); // out := in'

	void zeros(GPU_MAT out); // out := zeros(out.rows,out.cols)

	GPU_MAT unpadOnes(GPU_MAT in); // return &(:,1:end)

	void padOnes_host(GPU_MAT in, GPU_MAT out); // out := [ones, in]

	void ln_host(GPU_MAT in, GPU_MAT out); // out := ln(in)

	float sum_host(GPU_MAT in); // return sum(in)

	float magnitude_host(GPU_MAT in); // return sqrt(sum(in .* in))

	void getRows_array(GPU_MAT* arr, int num, int begin, GPU_MAT* ret);

}


#endif