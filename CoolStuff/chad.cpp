#include "types.h"
#include "neuralnet_cu.h"
#include "io.h"
#include "visualizer.h"

#include <vector>
#include <cstdlib>

#include <ctime>
#include <cmath>

#include <SDL.h>

#include <GL/glew.h>
#include <GL/gl.h>
#include "glc_cu.h"
#include "sdlc.h"

#undef main

namespace neuro {
	float randrange = 0.1f;

	float getRand() {
		return ((rand() / (float) RAND_MAX) * 2.f - 1.f) * randrange;
	}
	Matrix randInitializeWeights(int m, int n) {
		Matrix ret(m,n);
		for(int i = 0; i < ret.size(); i++) {
			ret.data()[i] = getRand();
		}
		return ret;
	}

	CPU_MAT convertMatrixToCPUMAT(Matrix in) {
		float* data = new float[in.size()];
		memcpy(data,in.data(),sizeof(float) * in.size());
		return CPU_MAT(in.rows(),in.cols(),data);
	}

	std::vector<CPU_MAT> convertMatrixArrayToCPUMATs(std::vector<Matrix> in) {
		std::vector<CPU_MAT> ret;
		for(unsigned int m = 0; m < in.size(); m++) ret.push_back(convertMatrixToCPUMAT(in[m]));
		return ret;
	}

	struct WrapperNet {
		std::vector<Matrix> weights;
		int layers;

		GPUFeedForwardNeuralNetwork gpuNet;

		WrapperNet(int inputSize, int outputSize, std::vector<int> hiddenLayerSizes, Matrix X, Matrix Y);
	};

	WrapperNet::WrapperNet(int inputSize, int outputSize, std::vector<int> hiddenLayerSizes, Matrix X, Matrix Y) {
		std::vector<int> allLayers;
		layers = hiddenLayerSizes.size()+2;
		
		allLayers.push_back(inputSize);
		for(int i = 0; i < layers - 2; i++) allLayers.push_back(hiddenLayerSizes[i]);
		allLayers.push_back(outputSize);

		for(int i = 0; i < layers - 1; i++) {
			Matrix w = randInitializeWeights(allLayers[i]+1, allLayers[i+1]);
			weights.push_back(w);
		}

		gpuNet = GPUFeedForwardNeuralNetwork(convertMatrixArrayToCPUMATs(weights));

		gpuNet.train(convertMatrixToCPUMAT(X),convertMatrixToCPUMAT(Y));
	}

	void multiply_derp(CPU_MAT a, CPU_MAT b, CPU_MAT c) {
		Matrix _a = Matrix(Eigen::Map<Matrix>(a.data, a.rows, a.cols));
		Matrix _b = Matrix(Eigen::Map<Matrix>(b.data, b.rows, b.cols));

		Matrix _c = _a * _b;
		memcpy(c.data, _c.data(), _c.size() * sizeof(float));
	}
}

using namespace neuro;

int main(int argc, int** argv) {
	neuro::CUDA_CHAD_INIT();
	// runFunctionTests();
	// exit(1);
	srand(time(0));

	mNetVisualizer = new NetVisualizer();
	mNetVisualizer->init();

	glewExperimental = GL_TRUE;			

	int nTraining = 60000;
	int nOffs = 0;
	Matrix images = loadMNISTImages("res/train-images.idx3-ubyte", nTraining, nOffs);
	Matrix labels = loadMNISTLabels("res/train-labels.idx1-ubyte", nTraining, nOffs);

	int nTest = 10000;
	Matrix testImages = loadMNISTImages("res/t10k-images.idx3-ubyte", nTest);
	Matrix testLabels = loadMNISTLabels("res/t10k-labels.idx1-ubyte", nTest);

	neuro::WrapperNet net(images.cols(), labels.cols(), {75, 50}, images, labels);

	mSDL->swapBuffer();

	int* test = net.gpuNet.classify(convertMatrixToCPUMAT(testImages));

	float nCorrect = 0;
	for (int i = 0; i < testImages.rows(); i++) {
		if (testLabels(i, test[i]) == 1) nCorrect++;
	}

	std::cout << "Test set: " << (nCorrect / testImages.rows() * 100.f) << "%" << std::endl;

	int curMatrix = 0;
	bool running=true;
	GraphBuffer graphBuffer(100,glm::vec2(0,0.5),glm::vec2(0.5,0.5),glm::vec4(1,0,0,1));
	for(int i = 0; i < 100; i++) {
		graphBuffer.pushPoint( (i/100.f) * (i/100.f) );
	}

	FeedForwardNeuralNetVisualization viz(
		&(net.gpuNet), 
		{
			NetVisualizerLayer(28, glm::vec2(0.05, 0.1), glm::vec2(0.25,0.8)),
			NetVisualizerLayer(15, glm::vec2(0.35, 0.1), glm::vec2(0.20,0.8)),
			NetVisualizerLayer(10, glm::vec2(0.60, 0.1), glm::vec2(0.20,0.8)),
			NetVisualizerLayer(10, glm::vec2(0.85, 0.1), glm::vec2(0.05,0.8))
		}, 
		glm::vec2(0.5,0), glm::vec2(0.5,0.5)
	);
	while(running) {

		glClear(GL_COLOR_BUFFER_BIT);
		GPU_MAT g = stackAllocateGPUMAT(1,images.cols());
		CPU_MAT a = convertMatrixToCPUMAT(images.row(curMatrix++));

		copyCPUMATtoGPUMAT(a,g);

		viz.generateVisualization(g);

		a.rows = 28;
		a.cols = 28;
		g.rows = 28;
		g.cols = 28;

		mNetVisualizer->visualizeMatrix(g, glm::vec2(0,0), glm::vec2(0.5,0.5));

		delete[] a.data;

		viz.draw();

		graphBuffer.draw();

		mSDL->swapBuffer();
		running = mSDL->handleEvents();
		mSDL->sleep(2000);

		mDeviceStack->pop(1);

	}

	return 0;
}