#include "visualizer.h"

#include "glc_cu.h"
#include "sdlc.h"

#include <iostream>
#include <cuda_gl_interop.h>

#include "neuralnet_cu.h"

namespace neuro {

	// convert floats in range [0,1] to RGB
	__global__ void convertFloatsToNormalizedChars(unsigned char* out, float* in, int numElements) {
		int index = 4*(threadIdx.x + blockIdx.x * blockDim.x);

		if(index < numElements) {
			unsigned char value = (unsigned char) (in[index/4]*255.f);
			out[index] = value;
			out[index+1] = value;
			out[index+2] = value;
			out[index+3] = 255; // alpha channel
		}
	}

	void NetVisualizer::visualizeMatrix(GPU_MAT m, glm::vec2 pos, glm::vec2 size) {
		CudaTexture* tempTex = mGL->allocCudaTexture(glm::ivec2(m.cols,m.rows));
		copyMatrixToTexture(m,tempTex);
		mGL->renderTexture(tempTex->tex,pos,size);
		delete tempTex;
	}

	void NetVisualizer::copyMatrixToTexture(GPU_MAT m, CudaTexture* tex) {
		unsigned char* unsignedCharArray = mDeviceStack->makeNew<unsigned char>(m.size()*4);

		int blocksPerGrid = (m.size() + THREADS_PER_BLOCK - 1) / THREADS_PER_BLOCK;

		convertFloatsToNormalizedChars<<<blocksPerGrid, THREADS_PER_BLOCK>>>(unsignedCharArray, m.data, m.size()*4);

		mGL->uploadData((glm::u8vec4*) unsignedCharArray, tex);

		mDeviceStack->pop(1);

	}

	FeedForwardNeuralNetVisualization::FeedForwardNeuralNetVisualization(GPUFeedForwardNeuralNetwork* net, std::vector<NetVisualizerLayer> layerInfo, glm::vec2 pos, glm::vec2 size) {
		if(layerInfo.size() != net->layers) std::cout << "S problem happened on line " << __LINE__ << " in file " << __FILE__ << ". your layers do not equal your layer info or something, you should fix." << std::endl;

		// Allocate the textures for the layers
		layerInfo[0].tex = mGL->allocCudaTexture(glm::ivec2(net->weights[0].rows/layerInfo[0].rows, layerInfo[0].rows));
		for(int i = 1; i < net->layers; i++) {
			layerInfo[i].tex = mGL->allocCudaTexture(glm::ivec2(net->weights[i-1].cols/layerInfo[i].rows, layerInfo[i].rows));
		}

		this->net = net;
		this->layerInfo = layerInfo;
		this->pos = pos;
		this->size = size;
	}
	void FeedForwardNeuralNetVisualization::generateVisualization(GPU_MAT X) {
		GPU_MAT *a = new GPU_MAT[net->layers], *z = new GPU_MAT[net->layers];

		createZandAMatrices(1, net, z, a, true);

		GPU_MAT H = stackAllocateGPUMAT(a[net->layers-1].rows,a[net->layers-1].cols);

		feedforward_propagate_host(X, net, z, a, H);

		for(int i = 0; i < net->layers; i++) {
			GPU_MAT t;
			if(i != net->layers-1)
				t = unpadOnes(a[i]);
			else
				t = a[i];
			t.cols = layerInfo[i].tex->tex->size.x;
			t.rows = layerInfo[i].tex->tex->size.y;
			mNetVisualizer->copyMatrixToTexture(t,layerInfo[i].tex);
		}

		mDeviceStack->pop(2*net->layers+1);
		delete[] a;
		delete[] z;
	}
	void FeedForwardNeuralNetVisualization::draw() {
		for(int i = 0; i < net->layers; i++) {
			glm::vec2 pos = this->pos + layerInfo[i].pos * this->size;
			glm::vec2 size = this->size * layerInfo[i].size;
			mGL->renderTexture(layerInfo[i].tex->tex,pos,size);
		}
	}

	void NetVisualizer::init() {
		mSDL = new SDLController(1024,768,1024,768,false,"C.H.A.D.");
		mGL = new GLController();
		mGL->init();
		glClearColor(1.f, 1.f, 1.f, 1.f);
		glClear(GL_COLOR_BUFFER_BIT);
		mSDL->swapBuffer();
	}

	void NetVisualizer::renderLoop() {

	}



}