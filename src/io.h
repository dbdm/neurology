#ifndef IO_H_
#define IO_H_

#include <string>
#include <caffe/caffe.hpp>

#include "types.h"

namespace neuro {
	// To be used as paths
	#define PROTO_PREF	std::string("res/proto/")
	#define SNAP_PREF	std::string("snap/")
	#define NET_SUF		std::string("network.prototxt")

	// This will load from the network from: res/proto/$(net_dir)/network.prototxt
	//  and the weights from:   res/proto/$(net_dir)/snap/$(model_name).caffemodel
	caffe::Net<Scalar> loadNetwork(std::string net_dir, std::string model_name);

	std::string getNetString(std::string net_dir);
	std::string getSnapString(std::string net_dir, std::string model_name);
	caffe::Net<Scalar> * createNetwork(std::string net_dir, std::string model_name);
	
	Matrix takeWebcamImage(int * returnCode);

	// This function will apply the distortions in order from index 0 to index 1
	// to the img0 in the text file and output to img1.
	// The textfile should have the format:
	//   img0.png img1.png
	//   img0.png img1.png [etc.]
	// Note: Images are converted to grayscale in the process
	void distortImageset(std::string textfile, std::vector<Matrix(*)(Matrix)> distortions);
	
	//   Inputs images of strings then converts them into 30x30 pixel character training data.
	//   This first Normalizes the height of all string images to 30 pixels then uses a 30x30
	// sliding window with a stride of one pixel to segment each image into many subimages and
	// then reads from the text file corresponding to each string for the characters that
	// correspond to each subimage. If the center of the subimage contains a labeled point,
	// the subimage is outputed to outputDir/labeled/<label>_<index>.png and if it does not,
	// it's outputed to outputDir/bottom/<index>.png, where <index> is the number of times that
	// label was used.
	//   The string images and label files should be stored in imageDir and labelDir respectively
	// and with names of the format <index>.png for the images and <index>.txt for the labels. All
	// indices start at 0 and should count up sequentially.
	//   All lines of the label file should look like this:
	//     <label> <x> <y> \n
	//   where the label is a number and the x and y coordinates start from the top-left.
	void segmentStrings(std::string imageDir, std::string labelDir, std::string outputDir, int nImages);

	// Returns the byte offset if there is one and -1 if there isn't
	int getPacketOffs(char * beg, int numBytes, int packetSize, const char * cmp);
	int char4ToIntLE(char * beg); // Big Endian

	std::vector<char> loadBytes(std::string filename);
	std::vector<int> loadMNISTLabels(std::string fileName, unsigned int numLabels, unsigned int offset=0);
	std::vector<Matrix> loadMNISTImages(std::string filename, unsigned int numLabels, unsigned int offset=0);
}

#endif