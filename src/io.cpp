#include <fstream>
#include <sstream>
#include <iostream>
#include <unordered_map>

#include "io.h"
#include "algs.h"

namespace neuro {

	std::string getNetString(std::string net_dir) {
		return PROTO_PREF + net_dir + std::string("/") + NET_SUF;
	}
	std::string getSnapString(std::string net_dir, std::string model_name) {
		return PROTO_PREF + net_dir + std::string("/") + SNAP_PREF + model_name + std::string(".caffemodel");
	}

	caffe::Net<Scalar> * createNetwork(std::string net_dir, std::string model_name) {
		caffe::Net<Scalar> * n = new caffe::Net<Scalar>(getNetString(net_dir), caffe::TEST);
		n->CopyTrainedLayersFrom(getSnapString(net_dir, model_name));

		return n;
	}


	const int zoom_slider_max = 1000;
	Matrix frame, newFrame;
	int zoom_slider = 0;
	bool init_zoom = false;

	void on_cam_trackbar(int, void*) {
		cv::Rect newRect(0, 0, frame.cols, frame.rows);

		Scalar zx = zoom_slider / (Scalar) zoom_slider_max * (frame.cols - 1);
		Scalar zy = zoom_slider / (Scalar) zoom_slider_max * (frame.rows - 1);

		newRect += cv::Point(zx / 2, zy / 2);
		newRect -= cv::Size(zx, zy);

		cv::resize(frame(newRect), newFrame, frame.size());

		// Rotate 90 degrees
		transpose(newFrame, newFrame);  
		flip(newFrame, newFrame, 1);

		cv::imshow("Image Selector", newFrame);
	}

	cv::Size webcamSize = {3264, 2448};
	cv::Size resizedSize = {3264/3, 2448/3};

	Matrix takeWebcamImage(int * returnCode) {
		cv::VideoCapture cap(0);

		if (!cap.isOpened()) {
			std::cout << "Could not open webcam!" << std::endl;
			return Matrix();
		}

		cap.set(CV_CAP_PROP_FRAME_WIDTH, webcamSize.width);
		cap.set(CV_CAP_PROP_FRAME_HEIGHT, webcamSize.height);

		cap.read(frame);
		
		cv::namedWindow("Image Selector", 1);
		cv::createTrackbar("Zooooooom", "Image Selector", &zoom_slider, zoom_slider_max);

		while (true) {
			Matrix temp;

			if (!cap.read(temp)) {
				std::cout << "Could not read frame from webcam!" << std::endl;
				break;
			}

			cv::resize(temp, frame, resizedSize);
			
			on_cam_trackbar(0, 0);

			auto key_code = cv::waitKey(1);

			if 		(key_code == 27	/* Escape */) *returnCode = -1;
			else if (key_code == 32 /* Space  */) *returnCode = 0;
			else if (key_code == (int)('g')) *returnCode = 1;
			if (key_code != -1) break;
		}

		cv::destroyWindow("Image Selector");

		return newFrame;
	}

	int char4ToIntLE(char * beg) {
		return (beg[0] << 24) + (beg[1] << 16) + (beg[2] << 8) + beg[3];
	}

	int getPacketOffs(char * beg, int numBytes, int packetSize, const char * cmp) {
		for (int i = 0; i < numBytes; i += packetSize)
			if (memcmp(&beg[i], cmp, packetSize) == 0) return i;
		return -1;
	}


	struct CharCenter {
		int id;

		// Note that after modifying and image, this center is not going to be valid
		//  unless you account for that modification.
		cv::Point center;
	};

	std::vector<CharCenter> loadStringCenters(std::string textfile) {
		std::ifstream file(textfile);

		if (!file.is_open()) {
			std::cout << "Could not open text file \"" << textfile <<"\"!" << std::endl;
			return {};
		}

		std::string line;
		std::vector<CharCenter> centers;
		while (std::getline(file, line)) {
			if (line.length() < 4) continue;

			std::stringstream parser(line);
			CharCenter c;
			
			parser >> c.id;
			parser >> c.center.x;
			parser >> c.center.y;

			centers.push_back(c);
		}

		return centers;
	}

	// Note: this isn't efficient at all lol
	void segmentStrings(std::string imageDir, std::string labelDir, std::string outputDir, int nImages) {
		std::unordered_map<int, int> labelCounter;
		cv::Size windowSize = {60, 60};
		int stride = 4;

		for (int i = 0; i < nImages; i++) {
			Matrix img = toGrayscale(cv::imread(imageDir + std::to_string(i) + ".png"));
			auto centers = loadStringCenters(labelDir + std::to_string(i) + ".txt");
			auto subimgs = imageSweep(img, windowSize, stride);

			// Label all the subimages
			for (int j = 0; j < (int) subimgs.size(); j++) {
				// This ignores the y-values of the centers
				// The closest label within windowSize.width / 8 to the center will be labeled
				int imgX = windowSize.width / 2 + stride * j; // width / 2 to get to the center
				int minDist = windowSize.width / 6, label = -1; // Label of -1 is bottom

				for (int k = 0; k < (int) centers.size(); k++)
					// We have to transpose the read center's x because we added width / 2 padding to the image on either side
					if (std::abs(centers[k].center.x + windowSize.width / 2 - imgX) < minDist) {
						label = centers[k].id;
						minDist = std::abs(centers[k].center.x + windowSize.width / 2 - imgX);
					}

				if (label < 0) // Output Bottom
					cv::imwrite(outputDir + std::string("bottom/") + std::to_string(labelCounter[label]++) + std::string(".png"),
						subimgs[j], {CV_IMWRITE_PNG_COMPRESSION, 9});
				else
					cv::imwrite(outputDir + std::string("labeled/") + std::to_string(label) + std::string("_") + std::to_string(labelCounter[label]++) + std::string(".png"),
						subimgs[j], {CV_IMWRITE_PNG_COMPRESSION, 9});
			}
		}
	}

	void distortImageset(std::string textfile, std::vector<Matrix(*)(Matrix)> distortions) {
		std::cout << "Beginning distortions..." << std::endl;

		std::ifstream file(textfile);

		if (!file.is_open()) {
			std::cout << "Could not open text file \"" << textfile <<"\"!" << std::endl;
			return;
		}

		std::string line;
		while (std::getline(file, line)) {
			std::stringstream imgreader(line);
			std::string img0, img1;

			imgreader >> img0;
			imgreader >> img1;

			Matrix input = toGrayscale(cv::imread(img0));
			Matrix output = input; // So the following loop works
			for (auto func : distortions) {
				input = output;
				output = func(input);
			}

			cv::imwrite(img1, output, {CV_IMWRITE_PNG_COMPRESSION, 9});
		}

		std::cout << "Distortions finished." << std::endl;
	}

	std::vector<char> loadBytes(std::string fileName) {
		std::ifstream ifs(fileName.c_str(), std::ios::binary|std::ios::ate);

		if (!ifs.is_open()) {
			std::cout << "Could not open file \"" << fileName <<"\"!" << std::endl;
			return std::vector<char>();
		}

		std::ifstream::pos_type pos = ifs.tellg();
		std::vector<char> data(pos);

		ifs.seekg(0, std::ios::beg);
		ifs.read(&data[0], pos);

		return data;
	}

	std::vector<Matrix> loadMNISTImages(std::string fileName, unsigned int numImages, unsigned int offset) {
		auto data = loadBytes(fileName);
		if (data.size() == 0)
			return std::vector<Matrix>();

		auto offs = [&](int n){ return &data[n * 4]; };

		// Check Magic Number
		if (char4ToIntLE(offs(0)) != 0x00000803) {
			std::cout << "Error reading \"" << fileName << "\": Magic Number does not match training data.";
			return std::vector<Matrix>();
		}

		unsigned int numTrainingData = char4ToIntLE(offs(1));
		unsigned int h = char4ToIntLE(offs(2)), w = char4ToIntLE(offs(3));
		unsigned int curByte = 16;

		if (offset + numImages < numTrainingData && offset + numImages > 0) numTrainingData = numImages;
		else numTrainingData -= offset;
		curByte += offset;

		std::vector<Matrix> out;

		for (unsigned int i = 0; i < numTrainingData; i++) {
			Matrix tempImage(h, w, CV_8U);
			for (unsigned int y = 0; y < h; y++)
				for (unsigned int x = 0; x < w; x++)
					tempImage.at<unsigned char>(y, x) = 255 - (unsigned char) data[curByte + x + y * w + i * w * h];
			out.push_back(tempImage);
		}

		return out;
	}

	std::vector<int> loadMNISTLabels(std::string fileName, unsigned int numLabels, unsigned int offset) {
		auto data = loadBytes(fileName);
		if (data.size() == 0)
			return std::vector<int>();

		auto offs = [&](int n){ return &data[n * 4]; };

		// Check Magic Number
		if (char4ToIntLE(offs(0)) != 0x00000801) {
			std::cout << "Error reading \"" << fileName << "\": Magic Number does not match label data.";
			return std::vector<int>();
		}

		unsigned int numTrainingData = char4ToIntLE(offs(1));
		unsigned int curByte = 8;

		if (offset + numLabels < numTrainingData && offset + numLabels > 0) numTrainingData = numLabels;
		else numTrainingData -= offset;
		std::vector<int> out;
		curByte += offset;

		for (unsigned int i = 0; i < numTrainingData; i++) {
			unsigned char y = data[curByte++];
			out.push_back(y);
		}

		return out;
	}
}