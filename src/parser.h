#ifndef PARSER_H_
#define PARSER_H_

#include "types.h"

namespace neuro {

	std::string parseExpression(std::string expression);
	Scalar solveExpression(std::string expression);

	Scalar integrate(std::string expr, std::string var, Scalar low, Scalar high);
}

#endif // PARSER_H_