#ifndef ALGS_H_
#define ALGS_H_

#include <vector>

#include "types.h"

namespace neuro {
	
	// IMAGE PROCESSING
	Matrix toGrayscale(Matrix in);

	// Input is the original, greyscale source image
	std::vector<cv::Rect> getContourBoxes(Matrix img, int min = 50, int max = 100);
	std::vector<cv::Rect> getEdgeBoxes(Matrix img, cv::Size add = {40, 40});
	std::vector<cv::Rect> expandCenterRadius(std::vector<cv::Rect> v);

	Matrix normalize(Matrix in);
	Matrix cleanImage(Matrix in);
	Matrix prepareProblem(Matrix in, cv::Size desired_size = {200, 200});

	Matrix cropToFit(Matrix in);
	
	// Crops the image then resizes it to dims and pads back to the original image size
	Matrix dimNormalize(Matrix in, cv::Size dims, bool noCrop = false, bool noPad = false, cv::Size desired_size = {0, 0});
	Matrix widthNormalize(Matrix in, int width);
	Matrix heightNormalize(Matrix in, int height);

	// The types (kernal shapes) are:
	//  cv::MORPH_RECT
	//  cv::MORPH_CROSS
	//  cv::MORPH_ELLIPSE
	Matrix erodeImage(Matrix img, int erosionSize, int erosionType);
	Matrix dilateImage(Matrix img, int dilationSize, int dilationType);

	// Sets every pixel under maxVal to 0
	Matrix binaryThreshold(Matrix img, int maxVal);

	// First height normalizes the image then separates it into subimages using a sweeping window of
	//  the specified size with the specified stride. Note: this pads the image with half of
	//  windowSize.width on each side.
	std::vector<Matrix> imageSweep(Matrix img, cv::Size windowSize, int stride);

	// Preprocessing for Committees
	namespace pre {
		template <int W>
		inline Matrix widthNormalize(Matrix in) { return neuro::widthNormalize(in, W); }
		inline Matrix normalize(Matrix in) { return neuro::normalize(in); }
	};

	void boundRect(cv::Rect& r, Matrix m);

	int demo_step(Matrix image, std::string name, std::string label = "", std::vector<cv::Rect> rectangles = std::vector<cv::Rect>(), cv::Size forceSize = {0, 0});
}

#endif