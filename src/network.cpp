#include <caffe/layers/memory_data_layer.hpp>
#include <unordered_map>

#include "network.h"
#include "io.h"

namespace neuro {

	Network::Network(std::string net_dir, std::string model_name, Matrix(*preprocessing)(Matrix)) {
		_pre = preprocessing;
		_net = createNetwork(net_dir, model_name);
	}

	Network::~Network() {
		delete _net;
	}

	int Network::classify(Matrix& image, Scalar * conf) {
		Matrix img;
		if (_pre > 0) img = _pre(image);
		else img = image;

		Scalar loss;
		std::vector<Matrix> images(1, img);
		std::vector<int> labels(1, 0);

		caffe::shared_ptr<caffe::MemoryDataLayer<Scalar>> md_layer =
			boost::dynamic_pointer_cast<caffe::MemoryDataLayer<Scalar>>(_net->layers()[0]);
		if (!md_layer) {
			std::cout << "The first layer is not a MemoryDataLayer!\n";
			return -1;
		}

		md_layer->AddMatVector(images, labels);
		const std::vector<caffe::Blob<Scalar>*>& result = _net->Forward(&loss);

		Scalar max_conf = 0;
		int max_ind = 0;

		for (int i = 0; i < result.back()->count(); i++)
			if (result.back()->cpu_data()[i] > max_conf) {
				max_ind = i;
				max_conf = result.back()->cpu_data()[i];
			}

		if (conf > 0) *conf = max_conf;

		std::cout << "Identified " << max_ind << " with confidence " << max_conf << std::endl;

		return max_ind;
	}





	AoINetwork::AoINetwork(int type, int aoiNum, Scalar scale) : _scale(scale) {
		std::string path = std::string("type") + std::to_string(type) + std::string("/aoi") + std::to_string(aoiNum);
		_pos0 = new Network("AoI", path + std::string("_pos0"));
		_pos1 = new Network("AoI", path + std::string("_pos1"));
	}

	AoINetwork::~AoINetwork() {
		delete _pos0;
		delete _pos1;
	}

	cv::Point decodePos(int pos, int width, Scalar scale) {
		int w = width / scale;
		cv::Point p(pos % w, (int) (pos / w));
		p.x *= scale;
		p.y *= scale;
		return p;
	}

	cv::Rect AoINetwork::classify(Matrix& img) {
		cv::Point p1 = decodePos(_pos0->classify(img), img.cols, _scale);
		cv::Point p2 = decodePos(_pos1->classify(img), img.cols, _scale);

		return cv::Rect(p1, p2);
	}





	ProblemNetwork::ProblemNetwork(int type, int nAoIs, Scalar scale) {
		for (int i = 0; i < nAoIs; i++)
			_aois.push_back(new AoINetwork(type, i, scale));
	}

	ProblemNetwork::~ProblemNetwork() {
		for (AoINetwork * aoi : _aois)
			delete aoi;
	}

	std::vector<cv::Rect> ProblemNetwork::classify(Matrix& img) {
		std::vector<cv::Rect> ret;

		for (AoINetwork * aoi : _aois)
			ret.push_back(aoi->classify(img));

		return ret;
	}


	AoICommittee::AoICommittee(int type, int aoiNum, int nCommittees, Scalar scale)
		: AoICommittee(type, aoiNum, {nCommittees, nCommittees}, scale) {}
	
	AoICommittee::AoICommittee(int type, int aoiNum, std::vector<int> nCommittees, Scalar scale) : _scale(scale) {
		std::string path = std::string("type") + std::to_string(type) + std::string("/aoi") + std::to_string(aoiNum);
		
		for (int i = 0; i < nCommittees[0]; i++) {
			_pos0.push_back(new Network("AoI", path + std::string("_pos0_com") + std::to_string(i)));
		}
		for (int i = 0; i < nCommittees[1]; i++) {
			_pos1.push_back(new Network("AoI", path + std::string("_pos1_com") + std::to_string(i)));
		}
	}

	AoICommittee::~AoICommittee() {
		for (auto n : _pos0) delete n;
		for (auto n : _pos1) delete n;
	}

	Scalar acceptableRadius = 40;

	cv::Point averageAoIs(std::vector<cv::Point> ps) {
		cv::Point totalPoint = {0, 0};
		for (auto p : ps)
			totalPoint = totalPoint + p;
		cv::Point mean = totalPoint * (1.0 / ps.size());

		for (int i = ps.size() - 1; i >= 0; i--)
			if (cv::norm(ps[i] - mean) > acceptableRadius) {
				ps.erase(ps.begin() + i);
				return averageAoIs(ps);
			}

		return mean;
	}

	std::vector<cv::Point> decodeCommittee(std::vector<Network*> aois, Matrix& img, Scalar scale) {
		std::vector<cv::Point> ret;
		for (auto n: aois)
			ret.push_back(decodePos(n->classify(img), img.cols, scale));
		return ret;
	}

	cv::Rect AoICommittee::classify(Matrix& img) {
		cv::Point pos0 = averageAoIs(decodeCommittee(_pos0, img, _scale));
		cv::Point pos1 = averageAoIs(decodeCommittee(_pos1, img, _scale));
		return cv::Rect(pos0, pos1);
	}

	std::vector<int> fillVec(int n, int size) {
		std::vector<int> ret;
		for (int i = 0; i < size; i++) ret.push_back(n);
		return ret;
	}

	ProblemCommittee::ProblemCommittee(int type, int nAoIs, int nCommittees, Scalar scale)
		: ProblemCommittee(type, nAoIs, fillVec(nCommittees, nAoIs * 2), scale) {}

	ProblemCommittee::ProblemCommittee(int type, int nAoIs, std::vector<int> nCommittees, Scalar scale) {
		for (int aoi = 0; aoi < nAoIs; aoi++) {
			std::vector<int> coms = {nCommittees[2*aoi], nCommittees[2*aoi+1]};
			_aois.push_back(new AoICommittee(type, aoi, coms, scale));
		}
	}
	ProblemCommittee::~ProblemCommittee() {
		for (auto n: _aois)
			delete n;
	}

	std::vector<cv::Rect> ProblemCommittee::classify(Matrix& img) {
		std::vector<cv::Rect> ret;

		for (AoICommittee * aoi : _aois)
			ret.push_back(aoi->classify(img));

		return ret;
	}	

	GradingCommittee::GradingCommittee(int nCommitteesN, int nCommitteesA) {
		for (int i = 0; i < nCommitteesN; i++) {
			num_pos0.push_back(new Network("Grading", "aoi0_pos0_com" + i));
			num_pos1.push_back(new Network("Grading", "aoi0_pos1_com" + i));
		}
		
		for (int i = 0; i < nCommitteesA; i++) {
			ans_pos0.push_back(new Network("Grading", "aoi1_pos0_com" + i));
		}
	};
	
	GradingCommittee::~GradingCommittee() {
		for (auto n: num_pos0)
			delete n;
		for (auto n: num_pos1)
			delete n;
		for (auto n: ans_pos0)
			delete n;
	};
	
	std::vector<cv::Point> decodeGradingCommittee(std::vector<Network*> aois, Matrix& img, Scalar y) {
		std::vector<cv::Point> ret;
		for (auto n: aois)
			ret.push_back(cv::Point(2 * n->classify(img), y));
		return ret;
	}

	std::vector<cv::Rect> GradingCommittee::classify(Matrix& img) {
		auto numpos0 = averageAoIs(decodeGradingCommittee(num_pos0, img, 0));
		auto numpos1 = averageAoIs(decodeGradingCommittee(num_pos1, img, img.rows));
		auto anspos0 = averageAoIs(decodeGradingCommittee(ans_pos0, img, 0));
		auto anspos1 = cv::Point(img.cols, img.rows);
		
		return {cv::Point{numpos0, numpos1}, cv::Point{anspos0, anspos1}};
	};


	Committee::Committee(std::vector<Network*> membs) : _members(membs) {}

	Committee::~Committee() {
		for (Network * member : _members)
			delete member;
	}

	int Committee::classify(Matrix& img) {
		std::unordered_map<int, int> votingBox;

		for (Network * member : _members)
			votingBox[member->classify(img)]++;

		int maxVotes = 0, index = 0;
		for (auto iter : votingBox)
			if (iter.second > maxVotes) {
				index = iter.first;
				maxVotes = iter.second;
			}

		return index;
	}
}