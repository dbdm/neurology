#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <string>
#include <vector>
#include <algorithm>

#include <caffe/caffe.hpp>
#include <caffe/util/io.hpp>
#include <glog/logging.h>

#include "io.h"
#include "fixcaffe.h"
#include "algs.h"
#include "types.h"
#include "pipeline.h"
#include "exprtk.hpp"

using namespace neuro;

void main(int argc, char ** argv) {
	// segmentStrings("res/zip/images/", "res/zip/labels/", "res/zip/out/", 50);
	
	Pipeline p;
	p.solveSheet(toGrayscale(cv::imread("res/testsheet.png")));

	// distortImageset("../../dropbox/workspaces/c++/neuro/res/db/points_wn/wnData1.txt", {pre::widthNormalize<10>});
	// distortImageset("../../dropbox/workspaces/c++/neuro/res/db/points_wn/wnData3.txt", {pre::widthNormalize<14>});
	// distortImageset("../../dropbox/workspaces/c++/neuro/res/db/points_wn/wnData4.txt", {pre::widthNormalize<16>});
	// distortImageset("../../dropbox/workspaces/c++/neuro/res/db/points_wn/wnData5.txt", {pre::widthNormalize<18>});
	// distortImageset("../../dropbox/workspaces/c++/neuro/res/db/points_wn/wnData2.txt", {pre::widthNormalize<12>});
	// distortImageset("../../dropbox/workspaces/c++/neuro/res/db/points_wn/wnData6.txt", {pre::widthNormalize<20>});

	// return;

	// Loop until img, loaded from the webcam, is empty
	// Matrix img;
	// int code;
	// while (img = takeWebcamImage(&code), code != -1) {
	//	if (code == 0)
	//		p.solveSheet(toGrayscale(img));
	//	if (code == 1)
	//		p.gradeSheet(toGrayscale(img));
	// 	cv::destroyAllWindows();
	// }
}