#include <algorithm>

#include <caffe/caffe.hpp>

#include "algs.h"

namespace neuro {

	std::vector<cv::Rect> getEdgeBoxes(Matrix source, cv::Size upsize) {
		cv::Point trans(upsize.width / 2, upsize.height / 2);

		std::vector<cv::Rect> contours_rect = getContourBoxes(source);

		demo_step(source, "Contours", "", contours_rect);

		for (int i = 0; i < contours_rect.size(); i++) 
			contours_rect[i] = (contours_rect[i] - trans) + upsize;

		demo_step(source, "Expanded Contours", "", contours_rect);

		// Remove boxes that are too concentrated
		for (int passes = 0; passes < 2; passes++)
			for (int i = (int) contours_rect.size()-1; i >= 0; i--) {
				for (int j = i - 1; j >= 0; j--) {
					cv::Rect intersect = contours_rect[i] & contours_rect[j];

					if (intersect.area() > 0) {
						contours_rect[j] = contours_rect[i] | contours_rect[j];
						contours_rect.erase(contours_rect.begin() + i);
						break;
					}
				}
			}

		demo_step(source, "Combined Contours", "", contours_rect);

		for (auto& r : contours_rect)
			boundRect(r, source);

		return contours_rect;
	}

	std::vector<cv::Rect> getContourBoxes(Matrix img, int min, int max) {
		img = binaryThreshold(img, 200);
		Matrix edges; // This wil contain the edges
		cv::Canny(img, edges, min, max);

		std::vector<std::vector<cv::Point>> contours;
		std::vector<cv::Vec4i> hierarchy;

		cv::findContours(edges, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, cv::Point(0, 0));

		std::vector<std::vector<cv::Point>> contours_poly(contours.size());
		std::vector<cv::Rect> contours_rect(contours.size());

		// Generate Edge Boxes

		for (int i = 0; i < contours.size(); i++) {
			cv::approxPolyDP(Matrix(contours[i]), contours_poly[i], 3, true);
			contours_rect[i] = (cv::boundingRect(Matrix(contours_poly[i])));
			boundRect(contours_rect[i], img);
		}

		return contours_rect;
	}

	Scalar scale = 1/3.0;
	Scalar inter = 0.9;

	std::vector<cv::Rect> expandCenterRadius(std::vector<cv::Rect> v) {
		for (int passes = 0; passes < 2; passes++)
			for (int i = (int) v.size()-1; i >= 0; i--) {
				for (int j = i - 1; j >= 0; j--) {
					cv::Point p0 = cv::Point(v[i].x + v[i].width / 2, v[i].y + v[i].height / 2);
					cv::Point p1 = cv::Point(v[j].x + v[j].width / 2, v[j].y + v[j].height / 2);
					Scalar area = (v[i] & v[j]).area();

					if (cv::norm(p0 - p1) < std::max(v[i].width, v[i].height) * scale ||
						cv::norm(p0 - p1) < std::max(v[j].width, v[j].height) * scale ||
						area > inter * v[i].area() || area > inter * v[j].area()) {

						v[j] = v[i] | v[j];
						v.erase(v.begin() + i);
						break;
					}
				}
			}

		return v;
	}

	Matrix prepareProblem(Matrix source, cv::Size desired_size) {
		Matrix reduced = cropToFit(source);

		Scalar scaleX = desired_size.width / (Scalar) reduced.cols, scaleY = desired_size.height / (Scalar) reduced.rows;
		Scalar scale = std::min(scaleX, scaleY);

		Matrix resized = dimNormalize(reduced, cv::Size(reduced.cols * scale, reduced.rows * scale), true, false, desired_size);
		//Matrix normalized = normalize(resized);

		return resized;
	}

	Matrix cropToFit(Matrix img) {
		std::vector<cv::Rect> crects = getContourBoxes(img);

		cv::Rect bigRect;
		for (int i = 0; i < crects.size(); i++)
			if (i == 0) bigRect = crects[i];
			else bigRect = bigRect | crects[i];
		boundRect(bigRect, img);
	
		return img(bigRect);
	}

	Matrix dimNormalize(Matrix img, cv::Size dims, bool noCrop, bool noPad, cv::Size desired_size) {
		Matrix cropped = noCrop ? img : cropToFit(img);
		Matrix resized;
		cv::resize(cropped, resized, cv::Size(
			dims.width  ? dims.width  : cropped.cols,
			dims.height ? dims.height : cropped.rows));
		resized = normalize(resized);

		if (noPad) return resized;

		if (desired_size.width <= 0) desired_size.width = img.cols;
		if (desired_size.height <= 0) desired_size.height = img.rows;

		Matrix padded;
		int top = (desired_size.height - resized.rows) / 2; if (top < 0) top = 0;
		int bottom = (desired_size.height - resized.rows) - top; if (bottom < 0) bottom = 0;
		int left = (desired_size.width - resized.cols) / 2; if (left < 0) left = 0;
		int right = (desired_size.width - resized.cols) - left; if (right < 0) right = 0;

		cv::copyMakeBorder(resized, padded, top, bottom, left, right, cv::BORDER_CONSTANT, 255);

		return padded;
	}

	Matrix widthNormalize(Matrix img, int w) {
		return dimNormalize(img, {w, 0});
	}

	Matrix heightNormalize(Matrix img, int h) {
		return dimNormalize(img, {0, h});
	}

	std::vector<Matrix> imageSweep(Matrix img, cv::Size windowSize, int stride) {
		if (stride <= 0) stride = 1;
		
		Matrix resized = cropToFit(img);
		double scale = windowSize.height / resized.rows, trueStride = scale * stride;
		cv::resize(resized, resized, {int(scale * resized.cols), windowSize.height});
		cv::copyMakeBorder(resized, resized, 0, 0, windowSize.width / 2, windowSize.width / 2, cv::BORDER_CONSTANT, 255);
		
		std::vector<Matrix> subimages;

		for (double offs = 0; offs < resized.cols - windowSize.width; offs += trueStride)
			subimages.push_back(resized(cv::Rect(offs, 0, windowSize.width, windowSize.height)));

		return subimages;
	}

	void boundRect(cv::Rect& r, Matrix m) {
		if (r.x > m.cols) r.x = m.cols - r.width;
		if (r.x + r.width > m.cols) r.width = m.cols - r.x;
		if (r.x < 0) r.x = 0;
		if (r.y > m.rows) r.y = m.rows - r.height;
		if (r.y + r.height > m.rows) r.height = m.rows - r.y;
		if (r.y < 0) r.y = 0;
		if (r.width <= 0) r.width = 1;
		if (r.height <= 0) r.height = 1;
	}

	Matrix toGrayscale(Matrix in) {
		Matrix ret;
		if(in.channels() != 1)
			cv::cvtColor(in, ret, cv::COLOR_BGR2GRAY);
		else return in;
		return ret;
	}

	Matrix normalize(Matrix a) {
		cv::Scalar mean, stdDev;
		cv::meanStdDev(a, mean, stdDev);

		Matrix b;
		a.convertTo(b, CV_8U, 1.0 / (double) stdDev.val[0], - mean.val[0] / (double) stdDev.val[0] + 255);

		double minVal, maxVal;
		Matrix c;
		cv::minMaxLoc(b, &minVal, &maxVal); // Find minimum and maximum intensities
		if (maxVal - minVal < 50) a = b; // If the values are to close, don't do anything
		b.convertTo(c, CV_8U, Scalar(255) / (maxVal - minVal), -minVal * Scalar(255) / (maxVal - minVal));

		return c;
	}

	Matrix binaryThreshold(Matrix img, int maxVal) {
		Matrix out;
		cv::threshold(img, out, maxVal, 255, 3);
		return out;
	}

	Matrix cleanImage(Matrix in) {
		demo_step(in, "Original Image");

		Matrix a = in;
		// cv::fastNlMeansDenoising(in, a, 10);

		Matrix b = normalize(a);

		return b;
	}

	Matrix erodeImage(Matrix img, int erosionSize, int erosionType) {
		Matrix elem = cv::getStructuringElement(erosionType,
			cv::Size(erosionSize, erosionSize),
			cv::Point(erosionSize / 2.0, erosionSize / 2.0));

		Matrix ret;
		cv::erode(img, ret, elem);
		return ret;
	}

	Matrix dilateImage(Matrix img, int dilationSize, int dilationType) {
		Matrix elem = cv::getStructuringElement(dilationType,
			cv::Size(dilationSize, dilationSize),
			cv::Point(dilationSize / 2.0, dilationSize / 2.0));

		Matrix ret;
		cv::dilate(img, ret, elem);
		return ret;
	}

	int demo_step(Matrix image, std::string name, std::string label, std::vector<cv::Rect> rectangles, cv::Size forceSize) {
		if (forceSize.width * forceSize.height <= 0) forceSize = image.size();

		Matrix img;
		cv::cvtColor(image, img, cv::COLOR_GRAY2BGR);

		double scale = 1, desired = 200;
		if (img.rows < desired) scale = desired / img.rows;
		if (img.cols * scale < desired) scale = desired / img.cols;

		cv::resize(img, img, cv::Size(img.cols * scale, img.rows * scale), 0, 0, cv::INTER_NEAREST);

		for (cv::Rect r : rectangles) {
			r.x *= scale;
			r.y *= scale;
			r.height *= scale;
			r.width *= scale;
			cv::rectangle(img, r, 0);
		}

		int baseline = 0;
		auto size = getTextSize(name, cv::FONT_HERSHEY_SIMPLEX, 0.5, 1, &baseline);

		cv::putText(img, name, {0, size.height}, cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(255, 0, 0));
		cv::putText(img, label, {0, img.rows - 3}, cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(0, 0, 255));

		cv::namedWindow("Demo", CV_WINDOW_AUTOSIZE);
		cv::imshow("Demo", img);
		int key = cv::waitKey();
		cv::destroyWindow("Demo");
		return key;
	}
}