#ifndef PIPELINE_H_
#define PIPELINE_H_

#include <unordered_map>

#include "network.h"
#include "types.h"
#include "io.h"

namespace neuro {

	struct Answer {
		cv::Point center;
		std::string text;
	};

	struct GradingAnswer {
		int number;
		std::string text;
	};

	enum ProblemType : int {
		Standard,
		Text,
		Stacked,
		LongDivision,
		Bottom
	};

	class Pipeline {
		Network * stage2;
		Committee * stage3_char_class;
		ProblemCommittee * stage3_stacked, * stage3_longDivision, * stage3_integral;
		GradingCommittee * stage3_grading;

	public:
		Pipeline();
		virtual ~Pipeline();


		std::vector<GradingAnswer> gradeSheet(Matrix img);
		std::string gradeProblem(Matrix img);

		std::vector<Answer> solveSheet(Matrix img);
		std::string solve(Matrix img);
		std::string solveLine(Matrix image);
		std::string solveStacked(Matrix img);
		std::string solveLongDivision(Matrix img);
		std::string solveIntegral(Matrix img, std::vector<cv::Rect> contours, int intChar, int * lastChar);

		std::vector<std::string> parseAoIs(Matrix img, std::vector<cv::Rect> AoIs);
		std::string parseString(Matrix img);
		std::string classifyChar(Matrix img);

		std::string calculate(std::string in);

		// Problem Identification
		ProblemType getProblemType(Matrix img);


		static std::unordered_map<ProblemType, std::string> getProblemString;
	};
}


#endif