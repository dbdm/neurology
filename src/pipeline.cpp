#include <ctype.h>
#include <sstream>
#include <iostream>

#include "pipeline.h"
#include "algs.h"
#include "parser.h"

namespace neuro {


	Pipeline::Pipeline() {
		stage2 = new Network("identification", "test_iter_47427");
		stage3_stacked = new ProblemCommittee(2, 2, 3);
		stage3_longDivision = new ProblemCommittee(3, 2, 3);
		stage3_integral = new ProblemCommittee(5, 2, 3);
		stage3_grading = new GradingCommittee(3, 3);
		stage3_char_class = new Committee(std::vector<Network*>{
			new Network("classification", "net1", pre::widthNormalize<10>), new Network("classification", "net2", pre::widthNormalize<12>), 
			new Network("classification", "net3", pre::widthNormalize<14>), new Network("classification", "net4", pre::widthNormalize<16>) 
			//new Network("classification", "net5", pre::widthNormalize<18>), new Network("classification", "net6", pre::widthNormalize<20>), 
			//new Network("classification", "net7")
		}); // For character classification
	}

	Pipeline::~Pipeline() {
		delete stage2;
		delete stage3_stacked;
		delete stage3_longDivision;
		delete stage3_char_class;
		delete stage3_integral;
		delete stage3_grading;
	}

	std::vector<GradingAnswer> Pipeline::gradeSheet(Matrix img) {
		Matrix clean = cleanImage(img);
		std::vector<cv::Rect> boxes = getEdgeBoxes(clean, {100, 0});

		std::vector<GradingAnswer> ret;
		for (int i = 0; i < boxes.size(); i++) {
			cv::Rect r = boxes[i];
			if (r.width < 50 || r.height < 20) continue;
			ret.push_back({i+1, this->gradeProblem(clean(r))});
			if (ret.back().text != "")
				demo_step(clean(r), "Answer!", ret.back().text);
		}
	}

	std::string Pipeline::gradeProblem(Matrix img) {
		return "";
	}

	std::vector<Answer> Pipeline::solveSheet(Matrix img) {
		Matrix clean = cleanImage(img);
		std::vector<cv::Rect> boxes = getEdgeBoxes(clean, {80, 40});

		std::vector<Answer> ret;
		for (auto& r : boxes) {
			if (r.width < 50 || r.height < 50) continue;
			ret.push_back({cv::Point(r.x + r.width / 2, r.y + r.height / 2), this->solve(clean(r))});
			if (ret.back().text != "")
				demo_step(clean(r), "Answer!", ret.back().text);
		}

		return ret;
	}


	std::string Pipeline::solve(Matrix img) {
		Matrix prep = prepareProblem(img);
		prep = normalize(prep);
		ProblemType id = getProblemType(prep);

		switch (id) {
			case Standard: return solveLine(prep);
			case Stacked: return solveStacked(prep);
			case LongDivision: return solveLongDivision(prep);
			case Bottom: break;
		}

		return "";
	}

	ProblemType Pipeline::getProblemType(Matrix img) {
		ProblemType label = (ProblemType) stage2->classify(img);
		if (demo_step(img, "Problem Identification", std::to_string((int) label)) != 32)
			return Bottom;
		return label;
	}
	
	std::string Pipeline::solveLine(Matrix img) {
		std::vector<cv::Rect> aois;

		aois.push_back(cv::Rect(0, 0, img.cols, img.rows));

		auto v = parseAoIs(img, aois);

		return parseExpression(v[0]);
	}

	std::string Pipeline::solveStacked(Matrix img) {
		std::vector<cv::Rect> aois = stage3_stacked->classify(img);

		demo_step(img, "AoI Generation", "", aois);

		auto v = parseAoIs(img, aois);

		return parseExpression(v[0] + v[1]);
	}

	std::string Pipeline::solveLongDivision(Matrix img) {
		std::vector<cv::Rect> aois = stage3_longDivision->classify(img);

		demo_step(img, "AoI Generation", "", aois);

		// Expand the second AoI under the division symbol and remove it
		Matrix edges; // This wil contain the edges
		cv::Canny(img, edges, 100, 200);

		std::vector<std::vector<cv::Point>> contours;
		std::vector<cv::Vec4i> hierarchy;

		cv::findContours(edges, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, cv::Point(0, 0));

		std::vector<std::vector<cv::Point>> contours_poly(contours.size());
		std::vector<cv::Rect> contours_rect(contours.size());

		// Generate Edge Boxes

		int divSymbol = 0;
		int maxval = 0;

		for (int i = 0; i < contours.size(); i++) {
			cv::approxPolyDP(Matrix(contours[i]), contours_poly[i], 3, true);
			contours_rect[i] = (cv::boundingRect(Matrix(contours_poly[i])));
			boundRect(contours_rect[i], img);
			if (contours_rect[i].area() > maxval) {
				maxval = contours_rect[i].area();
				divSymbol = i;
			}
		}

		aois[0] = contours_rect[divSymbol];

		cv::polylines(img, {contours_poly[divSymbol]}, false, 255, 16);

		demo_step(img, "Processed", "", aois);

		auto v = parseAoIs(img, aois);

		return parseExpression("(" + v[0] + ")" + "/(" + v[1] + ")");
	}

	bool sortByY(const cv::Rect& a, const cv::Rect& b) { return a.y < b.y; }

	std::string Pipeline::solveIntegral(Matrix img, std::vector<cv::Rect> contours, int intChar, int * lastChar) {
		// Find where the integral ends
		int lastContour = -1;
		for (int i = intChar + 1; i < contours.size(); i++) {
			std::string c = classifyChar(img(contours[i]));
			if (c == "d" && i+1 < contours.size()) {
				lastContour = i+1;
				break;
			}
		}
		*lastChar = lastContour;
		if (lastContour < 0) return "";

		// Get the integral bounds
		cv::Size upsize = {40, img.rows - contours[intChar].height};
		cv::Point trans = {0, upsize.height / 2};
		cv::Rect integralAoI = (contours[intChar] - trans) + upsize;
		boundRect(integralAoI, img);
		Matrix aoiImg = img(integralAoI);
		int desired_width = 200;
		int left = (desired_width - aoiImg.cols) / 2; if (left < 0) left = 0;
		int right = (desired_width - aoiImg.cols) - left; if (right < 0) right = 0;
		cv::copyMakeBorder(aoiImg, aoiImg, 0, 0, left, right, cv::BORDER_CONSTANT, 255);

		if (aoiImg.rows != desired_width | aoiImg.cols != desired_width) return "";

		std::vector<cv::Rect> aois;

		std::sort(aois.begin(), aois.end(), sortByY);
		for (auto& aoi : aois) aoi.x = aoi.x - left + integralAoI.x;
		auto v = parseAoIs(img, aois);

		Scalar low = solveExpression(v[0]);
		Scalar high = solveExpression(v[1]);

		for (auto& aoi : aois) cv::rectangle(img, aoi, 255, CV_FILLED);

		// The variable we're integrating over
		std::string var = classifyChar(img(contours[lastContour]));
		cv::Rect expr(cv::Point(contours[intChar].x + contours[intChar].width, 0),
			cv::Point(contours[lastContour-1].x, img.rows));

		std::string expr_s = parseString(img(expr));

		return std::to_string(integrate(expr_s, var, low, high));
	}

	void expandAoIRect(cv::Rect& aoi, Matrix img) {
		Matrix edges; // This will contain the edges
		cv::Canny(img, edges, 50, 150);

		std::vector<std::vector<cv::Point>> contours;
		std::vector<cv::Vec4i> hierarchy;

		cv::findContours(edges, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, cv::Point(0, 0));

		std::vector<std::vector<cv::Point>> contours_poly(contours.size());
		std::vector<cv::Rect> contours_rect(contours.size());

		for (int i = 0; i < contours.size(); i++) {
			cv::approxPolyDP(Matrix(contours[i]), contours_poly[i], 3, true);
			contours_rect[i] = cv::boundingRect(Matrix(contours_poly[i]));

			if ((contours_rect[i] & aoi).area() > 0) aoi = aoi | contours_rect[i];
		}

		boundRect(aoi, img);
	}

	bool sortByX(const cv::Rect& a, const cv::Rect& b) { return a.x < b.x; }

	std::vector<std::string> Pipeline::parseAoIs(Matrix img, std::vector<cv::Rect> AoIs) {
		std::vector<std::string> output;

		for (auto it = AoIs.begin(); it != AoIs.end(); it++) {
			auto aoi = *it;
			if (aoi.area() <= 0) continue;
			boundRect(aoi, img);
			expandAoIRect(aoi, img);
			Matrix aoi_img = img(aoi);
			
			output.push_back(parseString(aoi_img));
		}

		return output;
	}

	std::string Pipeline::parseString(Matrix image) {
		Matrix img = normalize(image);

		auto v = expandCenterRadius(getContourBoxes(erodeImage(binaryThreshold(img, 155), 7, cv::MORPH_ELLIPSE)));
		demo_step(img, "Contours", "", v);
		std::sort(v.begin(), v.end(), sortByX);
		std::string ret;

		bool superContext = false;

		for (int i = 0; i < v.size(); i++) {
			cv::Rect r = v[i];
			if (r.area() > 0) {
				boundRect(r, img);
				
				// If the character's bottom is higher than the center of the image, it's
				//  a superscript.
				if (r.y + r.height < img.rows / 2) {
					if (!superContext) {
						superContext = true;
						ret += "^(";
					}
				} else {
					if (superContext)
						ret += ")";
					superContext = false;
				}

				std::string c = classifyChar(img(r));

				if (c == "int") {
					int lastChar;
					c = solveIntegral(img, v, i, &lastChar);
					if (lastChar > 0 && c != "") i = lastChar + 1;
				}

				ret += c;
			}
		}

		if (superContext) ret += ")";

		return ret;
	}
	
	std::string charToString(char in) {
		std::stringstream ss;
		ss << in;
		return ss.str();
	}

	std::string makestr(int label) {
		switch (label) {
			case 10: return "-";
			case 11: return "+";
			case 12: return "*";
			case 13: return "/";
			case 14: return "=";
			case 15: return "/";
			// 52 Letter Labels Here
			case 68: return "(";
			case 69: return ")";
			case 70: return "[";
			case 71: return "]";
			case 72: return "int";
		}
		
		
		if (label >= 16 && label <= 41)
			return charToString('A' + (label - 16));
		else if (label >= 42 && label <= 67) {
			return charToString('a' + (label - 42));

		return std::to_string(label);
	}

	std::string Pipeline::classifyChar(Matrix img) {
		if (getContourBoxes(img).size() == 0) return "";

		Matrix scaled = prepareProblem(img, {30, 30});
		int label = stage3_char_class->classify(scaled);
		
		if (label < 10) {
			demo_step(scaled, "Char", std::to_string(label), {}, {200, 200});
			return std::to_string(label);
		} else {
			demo_step(scaled, "Char", makestr(label), {}, {200, 200});
			return makestr(label);
		}
	}
}