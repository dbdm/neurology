#include <exprtk.hpp>

#include "parser.h"

namespace neuro {
	typedef exprtk::symbol_table<Scalar> symbol_table_t;
	typedef exprtk::expression<Scalar>     expression_t;
	typedef exprtk::parser<Scalar>             parser_t;

	std::string parseExpression(std::string expr) {
		if (expr.back() == '=') expr.pop_back();
		return expr + "  =  " + std::to_string(solveExpression(expr));
	}

	Scalar solveExpression(std::string expr) {
		if (expr.back() == '=') expr.pop_back();
		
		symbol_table_t symbol_table;
		symbol_table.add_constants();

		expression_t expression;
		expression.register_symbol_table(symbol_table);

		parser_t parser;
		parser.compile(expr, expression);

		return expression.value();
	}

	Scalar integrate(std::string expr, std::string var, Scalar low, Scalar high) {
		Scalar v;

		symbol_table_t symbol_table;
		symbol_table.add_variable(var, v);
		symbol_table.add_constants();

		expression_t expression;
		expression.register_symbol_table(symbol_table);

		parser_t parser;
		parser.compile(expr, expression);

		return exprtk::integrate(expression, var, low, high);
	}
}