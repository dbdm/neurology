#ifndef TYPES_H_
#define TYPES_H_

#include <opencv2/opencv.hpp>
#include <opencv2/opencv_modules.hpp>

namespace neuro {

	typedef float Scalar;
	typedef cv::Mat Matrix;

}

#endif