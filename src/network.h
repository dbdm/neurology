#ifndef NETWORK_H_
#define NETWORK_H_

#include <caffe/caffe.hpp>
#include <string>

#include "types.h"

namespace neuro {
	
	struct Network {
		caffe::Net<Scalar> * _net;
		Matrix(*_pre)(Matrix);

		Network(std::string net_dir, std::string model_name, Matrix(*preprocessing)(Matrix) = 0);
		virtual ~Network();

		int classify(Matrix& img, Scalar* confidence = 0);
	};


	struct AoINetwork {
		Network * _pos0, * _pos1;
		Scalar _scale;

		// Files in the format res/proto/AoI/snap/type$(type)/aoi$(aoiNum)_pos1 and _pos2
		AoINetwork(int type, int aoiNum, Scalar scale = 10);
		virtual ~AoINetwork();

		cv::Rect classify(Matrix& img);
	};

	struct ProblemNetwork {
		std::vector<AoINetwork*> _aois;

		ProblemNetwork(int type, int nAoIs, Scalar scale = 10);
		virtual ~ProblemNetwork();

		std::vector<cv::Rect> classify(Matrix& img);
	};

	struct AoICommittee {
		std::vector<Network*> _pos0, _pos1;
		Scalar _scale;

		AoICommittee(int type, int aoiNum, int nCommittees, Scalar scale = 10);
		AoICommittee(int type, int aoiNum, std::vector<int> nCommittees, Scalar scale = 10);
		virtual ~AoICommittee();

		cv::Rect classify(Matrix& img);
	};

	struct ProblemCommittee {
		std::vector<AoICommittee*> _aois;

		ProblemCommittee(int type, int nAoIs, int nCommittees, Scalar scale = 10);
		ProblemCommittee(int type, int nAoIs, std::vector<int> nCommittees, Scalar scale = 10);
		virtual ~ProblemCommittee();

		std::vector<cv::Rect> classify(Matrix& img);
	};

	struct GradingCommittee {
		std::vector<Network*> num_pos0, num_pos1, ans_pos0;

		GradingCommittee(int nCommitteesN, int nCommitteesA);
		virtual ~GradingCommittee();

		std::vector<cv::Rect> classify(Matrix& img);
	};



	// The committee will take ownership of the networks
	// Voting is a perfect democracy, unlike ours
	struct Committee {
		std::vector<Network*> _members;

		Committee(std::vector<Network*> membs);
		virtual ~Committee();

		int classify(Matrix& img);
	};

}


#endif