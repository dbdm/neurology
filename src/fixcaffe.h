#ifndef FIX_CAFFE_H_
#define FIX_CAFFE_H_

#include "layer_factory.cpp"
#include <caffe/common.hpp>

// Have an error with "Unknown Layer Type"? Add the layer here:
#include <caffe/layers/memory_data_layer.hpp>
#include <caffe/layers/inner_product_layer.hpp>
#include <caffe/layers/softmax_loss_layer.hpp>
#include <caffe/layers/argmax_layer.hpp>
#include <caffe/layers/accuracy_layer.hpp>
#include <caffe/layers/batch_norm_layer.hpp>
#include <caffe/layers/dropout_layer.hpp>

namespace caffe {
	extern INSTANTIATE_CLASS(MemoryDataLayer);
	extern INSTANTIATE_CLASS(InnerProductLayer);
	extern INSTANTIATE_CLASS(SoftmaxWithLossLayer);
	extern INSTANTIATE_CLASS(ArgMaxLayer);
	extern INSTANTIATE_CLASS(AccuracyLayer);
	extern INSTANTIATE_CLASS(DropoutLayer);
	extern INSTANTIATE_CLASS(BatchNormLayer);
}


#endif