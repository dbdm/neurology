GFX_VERSION = 52
COMPILE_OBJECTS = io.obj pipeline.obj network.obj chad.obj algs.obj parser.obj
COMPILE_ARGS = -I./src/ -I./include/ -DUSE_OPENCV -Xcompiler -bigobj
SHARED_ARGS = -arch=compute_$(GFX_VERSION) -code=sm_$(GFX_VERSION) -Xcompiler -W0,-MD -O3
OPENCV = opencv_calib3d2410 opencv_contrib2410 opencv_core2410 opencv_features2d2410 opencv_flann2410 opencv_gpu2410 opencv_highgui2410 opencv_imgproc2410 opencv_legacy2410 opencv_ml2410 opencv_nonfree2410 opencv_objdetect2410 opencv_ocl2410 opencv_photo2410 opencv_stitching2410 opencv_superres2410 opencv_ts2410 opencv_video2410 opencv_videostab2410
HDF5 = hdf5 hdf5_tools hdf5_cpp hdf5_f90cstub hdf5_fortran hdf5_hl hdf5_hl_cpp hdf5_hl_f90cstub hdf5_hl_fortran
LIBS = cublas cublas_device shlwapi gflags LevelDb libglog libprotobuf lmdb $(OPENCV) $(HDF5) szip zlib libopenblas.dll libcaffe
LINKER_ARGS = -LC:/MinGW/lib $(foreach LIB, $(LIBS), -l$(LIB)) -Xlinker -LTCG,-opt:noref
DEBUG_ARGS = -DNEURO_DEBUG
OUTPUT_BIN = chad
OUTPUT_DIR = ./bin/
SOURCE_DIR = ./src/


benchmark: SHARED_ARGS += -DNEURO_BENCHMARK
benchmark: all

debug: SHARED_ARGS += $(DEBUG_ARGS)
debug: all

all: $(OUTPUT_DIR)$(OUTPUT_BIN)
all: run

$(OUTPUT_DIR)$(OUTPUT_BIN): $(COMPILE_OBJECTS)
$(OUTPUT_DIR)$(OUTPUT_BIN): link

link:
	nvcc $(SHARED_ARGS) $(LINKER_ARGS) $(foreach OBJ, $(COMPILE_OBJECTS), $(OUTPUT_DIR)$(OBJ)) -o $(OUTPUT_DIR)$(OUTPUT_BIN)

run:
	echo
	$(OUTPUT_DIR)$(OUTPUT_BIN)

%_cu.obj: $(SOURCE_DIR)%_cu.cpp
	nvcc -x cu $(SHARED_ARGS) $(COMPILE_ARGS) -c $< -o $(OUTPUT_DIR)$@

%.obj: $(SOURCE_DIR)%.cpp
	nvcc -x c++ $(SHARED_ARGS) $(COMPILE_ARGS) -c $< -o $(OUTPUT_DIR)$@

clean:
	rm -f $(OUTPUT_DIR)*