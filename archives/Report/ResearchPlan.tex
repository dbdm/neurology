\documentclass[12pt,twocolumn]{article}
\title{Using Artificial Intelligence Systems for Autonomous Visual Comprehension and Handwriting Generation \\ \large Research Plan and Post Project Summary}
\author{Daniel Bolya and Dylan McLeod}
\date{}

\begin{document}

\maketitle

\section{Note}
	This research plan and post project summary is a much shorter version of a currently 30-page report we are writing on the subject. Thus, this paper doesn't delve into implementation and other such intricacies. In addition, the reference section at the end is from that 30-page report and thus all citations from the bibliography do not appear in this document.

\section{Rationale}
	This project began when we wanted to use machine learning to do things as a human would. Because of the complexity and length of completing a math problem---especially handwritten ones--we decided solving handwritten math homework would be a good candidate to apply machine learning. Going into the problem, we both had taken an online course on machine learning and have read numerous papers on both machine learning and neural networks. Through subsequent research and brainstorming, we found that convolutional neural networks arranged in a pipeline would be the best way to process this math homework. Thus, we drew up a pipeline that could in theory solve problems as well as a human would. As for writing the answer, we were initially going to use deep belief networks to generate characters by reversing the process used to identify them. But through further research, we found that we can use a method similar to what Alex Graves developed to generate better handwriting and accomplish our goal.

\section{Hypothesis}
	Multiple neural networks can be used in a pipeline to parse various types of math problems and generate a handwritten answer as well as a human would (with at least 95\% accuracy).

\section{Procedure}
	The pipeline can be described as a series of five steps, namely: problem division, problem identification, problem parsing, problem solving, and writing the answer. The input of this pipeline is an image containing the entire sheet of homework and will be taken from a standard webcam. The output will be the online strokes that, when animated out, complete the assignment.
		\subsection{Problem Divison}
			Because of the entirely regular nature of math homework ( it's just black ink on white paper), this stage uses no neural networks---a simple algorithm to calculate likely problem candidates already exists. One can simply take the contours of the image and combine those close together. This results in problem areas which are then sent to and solved in the following steps. In the case of error, uncaught false positives will be taken care of in the problem identification step.
		\subsection{Problem Identification}
			Next, problem identification takes an input image of a problem, and classifies the problem as being in one of many pre-defined categories based on their structure. Examples include long division, stacked multiplication, addition, subtraction, etc. This can be done via a single convolutional neural network, with the output of the network being the problem's most likely category. If the problem is unclassifiable, problem identification will output $bot$, and the pipeline will be aborted for this problem. Training data can be created for this step by generating images of problems using both handwritten and font symbols (a process that applies to all steps), then training the network on them.
		\subsection{Problem Parsing}
			Each of these classified structures has a slightly different parsing step. However, every category relies on the same principle, which we will call areas of interest (AoIs). For each classification we define $N$ AoIs and create $2N$ neural networks that each locate a bound for a certain AoI. For each AoI, we train two bounds that form the most likely rectangle containing that AoI when combined. In the case of long division, we would define two AoIs: the locations of the dividend and the divisor. Once we've run the neural networks for each AoI and now know where they are located, we can parse each AoI using standard OCR techniques and output the characters they represent. Finally, we assemble the contained characters of each AoI to obtain a textual representation of the problem. In the case of long division, this would look like: $AoI_{dividend} \div AoI_{divisor}$. If any text field is unreadable, or any AoI unlocatable, problem parsing will output $\bot$, and the pipeline will be aborted for this problem.
		\subsection{Problem Solving}
			Problem solving is the easiest part of the pipeline. It takes a textual representation of the problem, and outputs the solution to the problem. We can accomplish this via Wolfram Alpha, or any other similar service.
		\subsection{Writing the Answer}
			For the handwriting generation part of problem writing, we will use Alex Graves' method (Graves, 2014). As for finding out where to write the answer, we will have a special network for each different structure classified in the problem identification step that output where to write the problem and what size. These networks are trained using a rectangle of center $X$ and size $S$. The output text of handwriting generation network will be scaled and translated using the size and position of the rectangle these networks output.

\section{Risk and Safety}
	As the project is a simple math-solving program, there are no physical or physiological risks. There is, however, a risk that the pipeline won't perform with the intended accuracy.

\section{Data Analysis}
	Because our pipeline is intended for real-world use, an accuracy measured through test data will never represent the intricacies of real-world images. Nevertheless, we can still measure our pipeline's performance on test sets separate from the training set to get a general idea of how well we have accomplished our initial goal---even if that measurement won't be fully accurate.
	
	Overall, the current model of the pipeline is not very accurate. Though some neural network component as fairly good accuracy (Problem Identification at over 95\%), others have abysmal accuracy (AoI's at around 50\% each and character classification at not much more). As for pipeline components that use image processing, their accuracy is hard to tell. Lower quality images produce lower quality results while pristine images are naturally easy to process with these algorithms.

\section{Discussion}
	\subsection{Accuracy and Speed}
		The pipeline developed in this project could be made more accurate in multiple ways. For problem division, one could use an additional convolutional network trained on bounding boxes as in (Jaderberg et al., 2014) in order to get a higher rate of recall. To increase speed the algorithms use could be further optimized and implemented on the GPU so as to make use of increased computing power. Apart from that, the quality of training data is also a factor that can influence accuracy.
	
	\subsection{Areas of Interest}
		Areas of interest seem to be a good way to have a network identify rectangles. The AoI’s networks we used were trained on an output grid of pixels scaled down by 10 in each direction such that p = x + width * y (so that 0 corresponds to the top-left corner, 19 the top right corner, and  399 the bottom left one). The networks used each took only around 20 minutes to train and worked well immediately. It is interesting to note that the accuracy of those networks on a test set is very low (40-50\% on convergence) because of its 400 outputs. Yet even with such low accuracy, in practice a few pixels off is just as good as exactly correct. Though if the areas of interest consistently exclude important regions of characters, we found that you can take the contours of the image that intersect the rectangle formed and include that in the rectangle for better results.

	\subsection{Network Size}
		One thing to note and something that can be changed is that we used very large networks for this application. Since we were training this on a modern GPU, it didn't take too long but it's worth investigating whether the same results can be obtained with smaller networks. Such information would be  desirable because it could speed up the runtime of the program.

	\subsection{Committees}
		Because problems come in all shapes and sizes it might be useful to use committees for better classification. What you would do is train more networks with inputs of different sizes and then during classification run the input over all those networks and choose the label with the highest confidence over all networks or the highest average confidence. This would make it so that problems that are much longer (such as extremely long type 0 problems) won't be caught up in stage 2. This process has been recently used for character classification (Cire\c{s}an et al., 2012), so we could use it for the same purpose.

\section{Conclusion}
	All in all, it is definitely possible to create a robust pipeline to solve a complex task such as interpreting and finding the answers to math homework. While we originally thought the pipeline would be all networks, a lot of it became image processing. In essence, an ideal balance of image processing and neural networks can produce amazing results. In the future, such pipelines may dominate the field of artificial intelligence.
	
	While the use of a neural network pipeline for solving math homework is an interesting task, the same pipeline could be easily modified to support a wide range of other applications. One such application, for instance, is grading free response tests and homework assignments. By training the pipeline on a different set of problem types, and by providing a key, it's possible to grade free response answers to a wide range of problems autonomously.

\nocite{*}
\bibliographystyle{apalike}
\bibliography{Report}


\end{document}
