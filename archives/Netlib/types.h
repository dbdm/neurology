/*
 * types.h
 *
 *  Created on: Nov 4, 2015
 *      Author: Daniel
 */

#ifndef TYPES_H_
#define TYPES_H_

#include <cmath>
#include <Eigen/Core>

#include "vglm.h"

namespace neuro {

	typedef float Scalar;
	const Scalar ONE = Scalar(1);
	typedef Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic> Matrix;
	typedef Eigen::Matrix<Scalar, 1,              Eigen::Dynamic> Vector;

	struct Size {
		int w, h;

		// For convenience because I use this operation a lot
		int prod() { return w * h; }

		template <typename Archive>
		void serialize(Archive& a) {
			a(w, h);
		}
	};

	template <typename ... argtypes>
	struct Enact {
		template <Scalar(*func)(Scalar, argtypes...)>
		struct Enactor {
			static Matrix call(Matrix x, argtypes ... args) {
				for (int i = 0; i < x.rows(); i++)
					for (int j = 0; j < x.cols(); j++)
						x(i, j) = func(x(i, j), args...);
				return x;
			}
		};

		typedef Matrix(*EnactorFunction)(Matrix, argtypes...);
	};

	template <>
	struct Enact<> {
		template <Scalar(*func)(Scalar)>
		struct Enactor {
			static Matrix call(Matrix x) {
				for (int i = 0; i < x.rows(); i++)
					for (int j = 0; j < x.cols(); j++)
						x(i, j) = func(x(i, j));
				return x;
			}
		};

		typedef Matrix(*EnactorFunction)(Matrix);
	};

	inline Scalar sigmoid_s(Scalar x) { return 1/(1+exp(-x)); }
	inline Scalar sigmoid_derivative_s(Scalar x) { Scalar sig = sigmoid_s(x); return sig * (ONE - sig); }

	inline Scalar add_s(Scalar x, Scalar y) { return x + y; }


#define DEFINE_ENACT(name, func, enac) const enac::EnactorFunction name = enac::Enactor<func>::call;

	// Best way to declare functions
	DEFINE_ENACT(add, add_s, Enact<Scalar>);
	DEFINE_ENACT(sigmoid, sigmoid_s, Enact<>);
	DEFINE_ENACT(sigmoid_derivative, sigmoid_derivative_s, Enact<>);
	DEFINE_ENACT(floor, std::floor, Enact<>);
	DEFINE_ENACT(ln, std::log, Enact<>);

	typedef Enact<>::EnactorFunction ComponentwiseFunction;

	inline Vector sumRows(Matrix m) {
		Vector ret(m.rows());
		for(int i = 0; i < m.rows(); i++) {
			ret(i) = 0;
			for(int j = 0; j < m.cols(); j++) {
				ret(i) += m(i,j);
			}
		}
		return ret;
	}
}



#endif /* TYPES_H_ */
