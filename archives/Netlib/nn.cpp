#include "nn.h"
#include "nnfuncs_cu.h"

#include <ctime>
#include <cstdlib>

namespace neuro {

	void printMat(float * mat, int m, int n) {
		for (int row = 0; row < m; row++) {
			std::cout << "\t";
			for (int col = 0; col < n; col++)
				std::cout << mat[row + col * m] << " ";
			std::cout << std::endl;
		}
	}

	void printGPUMat(GPUMat& a) {
		float * derp = new float[a.size()];
		a.fetch(derp);
		printMat(derp, a.rows(), a.cols());
		delete derp;
	}

	void initRandWeights(float * w, int m, int n, float lower, float upper) {
		for (int i = 0; i < m * n; i++)
			w[i] = rand() / (float) RAND_MAX * (upper - lower) + lower;
	}

	Matrix GPUMatToMatrix(GPUMat m) {
		float * derp = new float[m.size()];
		m.fetch(derp);
		Matrix out(Eigen::Map<Matrix>(derp, m.rows(), m.cols()));
		return out;
	}
	std::vector<Matrix> GPUMatToMatrix_vector(std::vector<GPUMat> v) {
		std::vector<Matrix> out;

		for (GPUMat& m : v)
			out.push_back(GPUMatToMatrix(m));

		return out;
	}

	GPUMat MatrixToGPUMat(Matrix m) { 
		GPUMat out(m.rows(), m.cols(), m.data());
		return out;
	}
	std::vector<GPUMat> MatrixtoGPUMat_vector(std::vector<Matrix> v) {
		std::vector<GPUMat> out;

		for (Matrix& m : v)
			out.push_back(MatrixToGPUMat(m));

		return out;
	}

	Matrix GPUNeuralNetworkBase::classify(Matrix X, int sections) {
		if (sections < 1) sections = 1;
		int batchSize = X.rows() / sections;

		Matrix out(X.rows(), 1);

		for (int i = 0; i < sections; i++)
			out.block(i * batchSize, 0, batchSize, 1) = (*this)(X.block(i * batchSize, 0, batchSize, X.cols()));

		return out;
	}


	GPUFeedForwardNeuralNetwork::GPUFeedForwardNeuralNetwork(int inputLength, int outputLength, std::vector<int> hiddenLayerSizes)
			: GPUNeuralNetworkBase(FEED_FORWARD, inputLength, outputLength) {

		_layers = (int)hiddenLayerSizes.size() + 2;
		std::vector<int> allLayers;

		allLayers.push_back(inputLength);
		for(int i = 0; i < _layers - 2; i++) allLayers.push_back(hiddenLayerSizes[i]);
		allLayers.push_back(outputLength);

		for(int i = 0; i < _layers - 1; i++) {
			int m = (allLayers[i]+1), n = allLayers[i+1];
			
			float * w = new float[n * m];
			initRandWeights(w, m, n, -0.1, 0.1);

			GPUMat _w(allLayers[i]+1, allLayers[i+1]);
			_w.populate(w);
			_weights.push_back(_w);

			delete[] w;
		}
	}

	float GPUFeedForwardNeuralNetwork::cost(GPUMat& X, GPUMat& Y) {
		GPUMat H(Y.rows(), Y.cols());

		delete _feedForward(H, X);

		GPUMat J(Y.rows(), Y.cols());
		float out = logistic_cost_host(J, H, Y);
		mStack->free(J, H);

		return out;
	}

	void GPUFeedForwardNeuralNetwork::grad(GPUMat * out, GPUMat& X, GPUMat& Y) {
		grad(out, X, Y, GPUMat(1, 1), GPUMat(1, 1), 0, false);
	}

	void GPUFeedForwardNeuralNetwork::grad(GPUMat * out, GPUMat& X, GPUMat& Y, GPUMat& lastDeltaOut, GPUMat& H, FeedForwardData * ff, bool takeFF) {
		GPUMat * delta = new GPUMat[_layers];

		if (!takeFF) {
			H = GPUMat(Y.rows(), Y.cols());
			ff = _feedForward(H, X);
		}

		delta[_layers - 1] = GPUMat(H.rows(), H.cols());
		sub_host(delta[_layers - 1], H, Y);

		for (int l = _layers - 2; l >= int(!takeFF); l--) {
			delta[l] = GPUMat(delta[l+1].rows(), _weights[l].rows() - 1);
			GPUMat tempDelta(delta[l+1].rows(), _weights[l].rows());

			delta[l].allocate();		// Ensure we allocate in the correct order because
			tempDelta.allocate();		//  tempDelta will be deleted in this loop.

			mult_transpose_second_host(tempDelta, delta[l+1], _weights[l]);
			unpad_ones_host(delta[l], tempDelta);

			if (l != 0)
				mult_comp_host(delta[l], delta[l], sigmoid_derivative_host(ff->z[l]));
			else {
				GPUMat tempX = sigmoid_derivative_host(copy(X));
				mult_comp_host(delta[l], delta[l], tempX);
				tempX.free();
			}

			
			tempDelta.free();

			if (l == 0)
				copy_host(lastDeltaOut, delta[0]);
		}

		for (unsigned int l = 0; l < _layers-1; l++) {
			GPUMat tempOut(out[l].rows(), out[l].cols());
			mult_transpose_first_host(tempOut, ff->a[l], delta[l + 1]);
			add_host(out[l], out[l], tempOut);
			tempOut.free();
		}

		if (!takeFF) {
			mStack->free(H);
			delete ff;
		}

		for (unsigned int i = 0 ; i < _weights.size() + 1; i++)
			delta[i].free();
		delete[] delta;
	}

	void GPUFeedForwardNeuralNetwork::train(Matrix X, Matrix Y, int iter, int gpuSections, int sections) {
		if (sections < 1) sections = 1;

		float alpha = 3;
		int m = X.rows();
		int batchSize = m / sections;
		int gpuBatchSize = batchSize / gpuSections;

		GPUMat X_batch(batchSize, _nIn), Y_batch(batchSize, _nOut);
		GPUMat X_gpuBatch(gpuBatchSize, _nIn), Y_gpuBatch(gpuBatchSize, _nOut);

		GPUMat * grad = new GPUMat[_weights.size()];

		for (unsigned int i = 0; i < _weights.size(); i++)
			grad[i] = zeros(_weights[i].rows(), _weights[i].cols());

		if (sections == 1) {
			X_batch.populate(X.data());
			Y_batch.populate(Y.data());
		}


		for (int it = 0; it < iter; it++) {
			for (int batch = 0; batch < sections; batch++) {
				if (sections > 1) {
					mStack->free(X_batch, Y_batch);

					X_batch.populate(Matrix(X.block(batch * batchSize, 0, batchSize, X.cols())).data());
					Y_batch.populate(Matrix(Y.block(batch * batchSize, 0, batchSize, Y.cols())).data());
				}

				for (int gpuBatch; gpuBatch < gpuSections; gpuBatch++) {
					block_host(X_gpuBatch, X_batch, gpuBatch * gpuBatchSize, 0);
					block_host(Y_gpuBatch, Y_batch, gpuBatch * gpuBatchSize, 0);
					this->grad(grad, X_gpuBatch, Y_gpuBatch);
					mStack->free(X_gpuBatch, Y_gpuBatch);
				}
			}

			for (unsigned int i = 0; i < _weights.size(); i++) {
				add_host(_weights[i], grad[i], _weights[i], -alpha/m * expf(-0.001 * it)); // W = -a/m * G + W
				zeros_host(grad[i]);
			}

			if (it % 10 == 0) 
				std::cout << "[ " << it << " ] Current Cost: " << cost(X_batch, Y_batch) << std::endl;
		}

		for (unsigned int i = 0; i < _weights.size(); i++)
			grad[i].free();

		mStack->free(X_batch, Y_batch);

		delete[] grad;
	}

	Matrix GPUFeedForwardNeuralNetwork::operator()(Matrix Xh) {
		GPUMat H(Xh.rows(), _nOut);
		GPUMat X(Xh.rows(), Xh.cols(), Xh.data());

		delete _feedForward(H, X);

		GPUMat O(X.rows(), 1);
		classify_host(O, H);

		float * o = new float[O.size()];
		O.fetch(o);

		mStack->free(X, H, O);

		Matrix ret = Matrix(Eigen::Map<Matrix>(o, Xh.rows(), 1));
		delete o;
		return ret;
	}

	FeedForwardData::~FeedForwardData() {
		for (GPUMat& m : a)
			m.free();
		for (GPUMat& m : z)
			m.free();
	}

	FeedForwardData * GPUFeedForwardNeuralNetwork::_feedForward(GPUMat& output, GPUMat& X) {
		// To ensure correct delete order
		output.allocate();

		FeedForwardData * ret = new FeedForwardData();

		ret->a.push_back(pad_ones(X));
		ret->z.push_back(GPUMat(1, 1));

		for (int i = 0; i < _layers - 1; i++) {
			GPUMat _z(ret->a[i].rows(), _weights[i].cols());
			mult_host(_z, ret->a[i], _weights[i]);
			ret->z.push_back(_z);

			if (i < _layers - 2) {
				GPUMat _a(_z.rows(), _z.cols() + 1);			// Create the matrix to hold the inplace padding
				CudaPtr<float> unpadded = _a.data();			// Copy the data pointer so we can modify it
				unpadded._ptr += _a.rows();						// Have the pointer start at the unpadded data
				GPUMat _temp(_z.rows(), _z.cols(), unpadded);	// Create a temp matrix (no allocation) representing unpadded portion
				copy_host(_temp, _z);							// Copy _z into unpadded section of _a
				sigmoid_host(_temp);							// Perform sigmoid on upadded portion of _a
				pad_ones_inplace_host(_a);						// Set the first col of _a to 1's, it's now S(_z) padded

				ret->a.push_back(_a);
			}
			else
				ret->a.push_back(sigmoid_host(copy(ret->z[i + 1])));
		}

		copy_host(output, ret->a[_layers - 1]);
		return ret;
	}

	void GPUFeedForwardNeuralNetwork::free() {
		for (GPUMat& m : _weights)
			m.free();
	}








	/*          CONVOLUTIONAL          */

	GPUConvolutionalNeuralNetwork::GPUConvolutionalNeuralNetwork(glm::ivec2 inputSize, int outputSize,
			std::vector<ConvolutionalIteration> iters, std::vector<int> fullyConnectedHiddenLayerSizes) :
			GPUNeuralNetworkBase(CONVOLUTIONAL, inputSize.x * inputSize.y, outputSize) {

		_iters = iters;
		_inSize = inputSize;

		int nBranches = 1;
		glm::ivec2 a_size = _inSize;

		for (auto& i : _iters) {
			GPUMat _w(i.nFeatures, i.featureSize.x * i.featureSize.y + 1);
			
			float * w = new float[_w.size()];
			initRandWeights(w, _w.rows(), _w.cols(), -0.1, 0.1);

			_w.populate(w);
			delete w;
			_weights.push_back(_w);


			i.inSize = a_size;
			i.convSize = (i.inSize - i.featureSize) / i.stride + 1;
			i.nBranches = nBranches;
			nBranches *= i.nFeatures;
			i.outSize = (a_size = i.convSize / i.poolSize);
		}

		_net = new GPUFeedForwardNeuralNetwork(nBranches * a_size.x * a_size.y, _nOut, fullyConnectedHiddenLayerSizes);
		_initialized = true;
	}

	float GPUConvolutionalNeuralNetwork::cost(GPUMat& X, GPUMat& Y) {
		GPUMat H(Y.rows(), Y.cols());

		delete _feedForward(H, X);

		GPUMat J(Y.rows(), Y.cols());
		float out = logistic_cost_host(J, H, Y);
		mStack->free(J, H);

		return out;
	}

	void GPUConvolutionalNeuralNetwork::grad(GPUMat * out, GPUMat& X, GPUMat& Y) {
		GPUMat H(Y.rows(), Y.cols()); // Network output
		ConvolutionalFeedForwardData * cffd = _feedForward(H, X);

		/* INITIALIZATION */

		// Output (pooled) -> unpooled -> unconvoluted (output)
		// Note: The output error of one layer is the unconvoluted error of the next layer
		GPUMat * delta_a = new GPUMat[_weights.size()];		// Output error

		GPUMat delta_fullCon(cffd->convOut.rows(), cffd->convOut.cols()); // Feed forward error
		delta_fullCon.allocate();
		
		for (int i = 0; i < _weights.size(); i++) {
			auto iter = _iters[i];

			delta_a[i] = GPUMat(X.rows() * iter.nBranches * iter.nFeatures, OP(iter.outSize));
			delta_a[i].allocate();
		}


		/* COMPUTATION */
		_net->grad(out + _weights.size(), cffd->convOut, Y, delta_fullCon, H, cffd->ffd, true);

		auto last = _iters.back();
		convolution_output_reorder_reverse_host(delta_a[_weights.size() - 1], delta_fullCon,
												X.rows(), last.nFeatures * last.nBranches, last.outSize);

		for (int i = _weights.size() - 1; i >= 0; i--) {
			auto iter = _iters[i];

			// Reverse Pooling
			GPUMat delta_h(cffd->h[i].rows(), cffd->h[i].cols());
			maxpool_reorder_reverse_host(delta_h, delta_a[i], cffd->p[i], iter.convSize, iter.nBranches * X.rows(), iter.poolSize);

			// Reverse Activation
			GPUMat dh(cffd->h[i].rows(), cffd->h[i].cols());
			dh = copy(cffd->h[i]);
			sigmoid_derivative_optimized_host(dh, dh);
			mult_comp_host(delta_h, delta_h, dh); // Remember chain rule!
			dh.free();

			// Reverse Weight Application
			GPUMat delta_conv(1 + OP(iter.featureSize), OP(iter.convSize) * X.rows() * iter.nBranches);
			mult_transpose_first_host(delta_conv, _weights[i], delta_h);

			// Compute Gradient
			GPUMat tempGrad(_weights[i].rows(), _weights[i].cols());
			mult_transpose_second_host(tempGrad, delta_h, cffd->x[i]);
			add_host(out[i], out[i], tempGrad);
			tempGrad.free();

			// Reverse Convolution
			if (i > 0) // Don't need to compute the input error for the first layer
				im2cols_reverse_unpad(delta_a[i-1], delta_conv, iter.inSize, iter.convSize, iter.featureSize, iter.stride);
			mStack->free(delta_conv, delta_h);
		}


		/* CLEAN UP */

		delete cffd;
		mStack->free(H, delta_fullCon);
		for (unsigned int i = 0; i < _weights.size(); i++)
			mStack->free(delta_a[i]);
		delete[] delta_a;
	}

	void GPUConvolutionalNeuralNetwork::train(Matrix X, Matrix Y, int iter, int gpuSections, int sections) {
		if (sections < 1) sections = 1;
		if (gpuSections < 1) gpuSections = 1;

		float alpha = 3;
		int m = X.rows();
		int batchSize = m / sections;
		int gpuBatchSize = batchSize / gpuSections;

		GPUMat X_batch(batchSize, OP(_inSize)),
			   Y_batch(batchSize, _nOut), 
			   X_gpuBatch(gpuBatchSize, OP(_inSize)),
			   Y_gpuBatch(gpuBatchSize, _nOut);

		GPUMat * grad = new GPUMat[_weights.size() + _net->_weights.size()];

		for (unsigned int i = 0; i < _weights.size(); i++)
			grad[i] = zeros(_weights[i].rows(), _weights[i].cols());
		for (unsigned int i = 0; i < _net->_weights.size(); i++)
			grad[i + _weights.size()] = zeros(_net->_weights[i].rows(), _net->_weights[i].cols());

		if (sections == 1) {
			X_batch.populate(X.data());
			Y_batch.populate(Y.data());
		}


		for (int it = 0; it < iter; it++) {
			for (int batch = 0; batch < sections; batch++) {
				if (sections > 1) {
					mStack->free(X_batch, Y_batch);

					X_batch.populate(Matrix(X.block(batch * batchSize, 0, batchSize, X.cols())).data());
					Y_batch.populate(Matrix(Y.block(batch * batchSize, 0, batchSize, Y.cols())).data());
				}

				for (int gpuBatch; gpuBatch < gpuSections; gpuBatch++) {
					block_host(X_gpuBatch, X_batch, gpuBatch * gpuBatchSize, 0);
					block_host(Y_gpuBatch, Y_batch, gpuBatch * gpuBatchSize, 0);
					this->grad(grad, X_gpuBatch, Y_gpuBatch);
					mStack->free(X_gpuBatch, Y_gpuBatch);
				}
			}

			for (unsigned int i = 0; i < _weights.size(); i++) {
				add_host(_weights[i], grad[i], _weights[i], -alpha/m * expf(-0.001 * it)); // W = -a/m * G + W
				zeros_host(grad[i]);
			}

			for (unsigned int i = 0; i < _net->_weights.size(); i++) {
				add_host(_net->_weights[i], grad[_weights.size() + i], _net->_weights[i], -alpha/m * expf(-0.001 * it)); // W = -a/m * G + W
				zeros_host(grad[_weights.size() + i]);
			}

			if (it % 10 == 0) 
				std::cout << "[ " << it << " ] Current Cost: " << cost(X_batch, Y_batch) << std::endl;
		}

		for (unsigned int i = 0; i < _weights.size() + _net->_weights.size(); i++)
			grad[i].free();

		mStack->free(X_batch, Y_batch);

		delete[] grad;
	}

	Matrix GPUConvolutionalNeuralNetwork::operator()(Matrix Xh) {
		GPUMat H(Xh.rows(), _nOut);
		GPUMat X(Xh.rows(), Xh.cols(), Xh.data());

		delete _feedForward(H, X);

		GPUMat O(X.rows(), 1);
		classify_host(O, H);

		float * o = new float[O.size()];
		O.fetch(o);

		mStack->free(X, H, O);

		Matrix ret = Matrix(Eigen::Map<Matrix>(o, Xh.rows(), 1));
		delete o;
		return ret;
	}

	ConvolutionalFeedForwardData * GPUConvolutionalNeuralNetwork::_feedForward(GPUMat& output, GPUMat& X) {
		output.allocate();

		ConvolutionalFeedForwardData * cffd = new ConvolutionalFeedForwardData();

		// Allocate all the necessary matrices
		for (auto& i : _iters) {
			GPUMat _x(1 + OP(i.featureSize), OP(i.convSize) * X.rows() * i.nBranches);
			_x.allocate();
			cffd->x.push_back(_x);

			GPUMat _h(i.nFeatures, OP(i.convSize) * X.rows() * i.nBranches);
			_h.allocate();
			cffd->h.push_back(_h);

			GPUMat _p(X.rows() * i.nBranches * i.nFeatures, OP(i.outSize));
			_p.allocate();
			cffd->p.push_back(_p);
		}

		auto last = _iters.back();
		cffd->convOut = GPUMat(X.rows(), OP(last.outSize) * last.nBranches * last.nFeatures);
		cffd->convOut.allocate();
	
		GPUMat tempA(1, 1);	// This is temp variable that isn't passed on, so allocate it after everything else

		for (int i = 0; i < _iters.size(); i++) {
			auto iter = _iters[i];

			// Convolute
			im2cols_pad_host(cffd->x[i], (i == 0 ? X : tempA), iter.inSize, iter.convSize, iter.featureSize, iter.stride);
			
			// Apply weights and activation function
			sigmoid_host(mult_host(cffd->h[i], _weights[i], cffd->x[i]));

			// Pool and output to the next layer	
			mStack->free(tempA);
			tempA = GPUMat(cffd->p[i].rows(), cffd->p[i].cols());
			maxpool_reorder_host(tempA, cffd->p[i], cffd->h[i], iter.convSize, iter.nBranches * X.rows(), iter.poolSize);
		}

		convolution_output_reorder_host(cffd->convOut, tempA, X.rows(), last.nBranches * last.nFeatures, last.outSize);
		tempA.free();

		cffd->ffd = _net->_feedForward(output, cffd->convOut);

		return cffd;
	}


	void GPUConvolutionalNeuralNetwork::free() {
		_net->free();
		delete _net;

		for (GPUMat& m : _weights)
			m.free();
	}

	ConvolutionalFeedForwardData::~ConvolutionalFeedForwardData() {
		for (GPUMat& m : x) m.free();
		for (GPUMat& m : h) m.free();
		for (GPUMat& m : p) m.free();
		convOut.free();
		delete ffd;
	}
}