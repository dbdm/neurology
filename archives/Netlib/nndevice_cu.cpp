#include <cstdlib>

namespace neuro {

	#define get(mat, _y, _x, _m) mat[(_y) + (_x) * (_m)]

	#define iftid \
		int tid = blockDim.x * (blockIdx.x + bp.blockOffs) + threadIdx.x; \
		if (tid < bp.nElements) \

	struct BatchParams {
		unsigned long long nElements;
		unsigned long long blockOffs;
	};


	__device__
	float sigmoid_ondevice(float a) {
		return 1 / (1 + expf(-a));
	}

	__global__
	void sigmoid_device(BatchParams bp, float * c, float * a) {
		iftid
			// c[tid] = max(0.f, a[tid]);
			c[tid] = sigmoid_ondevice(a[tid]);
	}

	__global__
	void sigmoid_derivative_inplace_device(BatchParams bp, float * a) {
		iftid {
			float sig = sigmoid_ondevice(a[tid]);
			a[tid] = sig * (1.f - sig);
		}
	}

	__global__
	void sigmoid_derivative_optimized_device(BatchParams bp, float * Y, float * X) {
		iftid
			Y[tid] = X[tid] * (1.f - X[tid]);
	}

	__global__
	void logistic_cost_nosum_device(BatchParams bp, float * J, float * H, float * Y, float invM) {
		iftid
			J[tid] = - invM * (Y[tid] * log(H[tid] + 0.0001f) + (1.f - Y[tid]) * log(1.f - H[tid] + 0.0001f));
	}

	__global__
	void add_device(BatchParams bp, float * c, float * a, float n) {
		iftid
			c[tid] = a[tid] + n;
	}

	__global__
	void sub_device(BatchParams bp, float * c, float * a, float n) {
		iftid
			c[tid] = n - a[tid];
	}

	__global__
	void compmult_device(BatchParams bp, float * c, float * a, float * b) {
		iftid
			c[tid] = a[tid] * b[tid];
	}

	__global__
	void classify_device(BatchParams bp, float * c, float * a, int n) {
		iftid {
			int winner = 0;
			float confidence = 0.f;
			for (int i = 0; i < n; i++) {
				if (a[tid + i * bp.nElements] > confidence) {
					winner = i;
					confidence = a[tid + i * bp.nElements];
				}
			}
			c[tid] = winner;
		}
	}

	__global__
	void set_device(BatchParams bp, float * a, float n) {
		iftid
			a[tid] = n;
	}

	__global__
	void pad_ones_device(BatchParams bp, float * out, float * in, int m) {
		iftid {
			int col = tid / m;
		
			if (col == 0)
				out[tid] = 1.f;
			else
				out[tid] = in[tid - m];
		}
	}

	__global__
	void unpad_ones_device(BatchParams bp, float * out, float * in, int m) {
		iftid
			out[tid] = in[tid + m];
	}

	__global__
	void copy_device(BatchParams bp, float * out, float * in) {
		iftid
			out[tid] = in[tid];
	}

	__global__
	void im2cols_device(BatchParams bp, float * Y, float * X,
		int imageWidth, int imageHeight, int convWidth, int convHeight,
		int scanWidth, int scanHeight, int strideX, int strideY, int xRows) {

		iftid {
			// tid = y + scanHeight * ( x + scanWidth * ( scanY + convHeight * ( scanX + convWidth * row ) ) )
			int upTick = 1; // Multiplicitave counter to unroll tid with the fewest operatons
			int upTick2 = 1; // This one lags behnd by 1 iteration to isolate the variable we need

			int y     = (tid % (upTick *= scanHeight));
			int x     = (tid % (upTick *= scanWidth))	/ (upTick2 *= scanHeight);
			int scanY = (tid % (upTick *= convHeight))	/ (upTick2 *= scanWidth);
			int scanX = (tid % (upTick *= convWidth))	/ (upTick2 *= convHeight);
			int row   = (tid)							/ upTick; // This equals the next iteration of upTick2

			get(Y, 1 + y + x * scanHeight, scanY + convHeight * (scanX + convWidth * row), (scanWidth * scanHeight + 1)) // Padding
				= get(X, row, (scanY * strideY + y) + imageHeight * (scanX * strideX + x), xRows);
		}
	}

	__global__
	void im2cols_reverse_device(BatchParams bp, float * X, float * Y,
		int imageWidth, int imageHeight, int convWidth, int convHeight,
		int scanWidth, int scanHeight, int strideX, int strideY, int xRows) {

		iftid {
			// tid = y + scanHeight * ( x + scanWidth * ( scanY + convHeight * ( scanX + convWidth * row ) ) )
			int upTick = 1; // Multiplicitave counter to unroll tid with the fewest operatons
			int upTick2 = 1; // This one lags behnd by 1 iteration to isolate the variable we need

			int y     = (tid % (upTick *= scanHeight));
			int x     = (tid % (upTick *= scanWidth))	/ (upTick2 *= scanHeight);
			int scanY = (tid % (upTick *= convHeight))	/ (upTick2 *= scanWidth);
			int scanX = (tid % (upTick *= convWidth))	/ (upTick2 *= convHeight);
			int row   = (tid)							/ upTick; // This equals the next iteration of upTick2

			get(Y, 1 + y + x * scanHeight, scanY + convHeight * (scanX + convWidth * row), (scanWidth * scanHeight + 1)) // Padding
				= get(X, row, (scanY * strideY + y) + imageHeight * (scanX * strideX + x), xRows);
		}
	}

	__global__
	void maxpool_reorder_device(BatchParams bp, float * Y, float * I, float* X, int xRows,
		int convWidth, int convHeight, int nConvs, int poolWidth, int poolHeight) {

		iftid {
			// tid = poolY + (convHeight / poolHeight) * ( poolX + (convWidth / poolWidth) * ( conv + nConvs * row ) )
			int upTick = 1; // Multiplicitave counter to unroll tid with the fewest operatons
			int upTick2 = 1; // This one lags behnd by 1 iteration to isolate the variable we need

			int outHeight = convHeight / poolHeight;
			int outWidth = convWidth / poolWidth;

			int poolY = (tid % (upTick *= outHeight));
			int poolX = (tid % (upTick *= outWidth))    / (upTick2 *= outHeight);
			int conv  = (tid % (upTick *= nConvs))      / (upTick2 *= outWidth);
			int row   = (tid)                           / upTick; // This equals the next iteration of upTick2

			float _max = 0; // Initialize this to the first element later
			int _ind = 0;

			for (int x = 0; x < poolWidth; x++)
			for (int y = 0; y < poolHeight; y++) {
				float n = get(X, row, (poolY * poolHeight + y) + convHeight * ((poolX * poolWidth + x) + convWidth * conv), xRows);
				if (x == 0 && y == 0) _max = n;
				else if (n > _max) {
					_max = n;
					_ind = y + poolHeight * x;
				}
			}
			
			get(Y, row + xRows * conv, poolY + outHeight * poolX, xRows * nConvs) = _max;
			get(I, row + xRows * conv, poolY + outHeight * poolX, xRows * nConvs) = _ind;
		}
	}

	__global__
	void maxpool_reorder_reverse_device(BatchParams bp, float* X, float * Y, float * I, int xRows,
		int convWidth, int convHeight, int nConvs, int poolWidth, int poolHeight) {

		iftid {
			// tid = poolY + (convHeight / poolHeight) * ( poolX + (convWidth / poolWidth) * ( conv + nConvs * row ) )
			int upTick = 1; // Multiplicitave counter to unroll tid with the fewest operatons
			int upTick2 = 1; // This one lags behnd by 1 iteration to isolate the variable we need

			int outHeight = convHeight / poolHeight;
			int outWidth = convWidth / poolWidth;

			int poolY = (tid % (upTick *= outHeight));
			int poolX = (tid % (upTick *= outWidth))    / (upTick2 *= outHeight);
			int conv  = (tid % (upTick *= nConvs))      / (upTick2 *= outWidth);
			int row   = (tid)                           / upTick; // This equals the next iteration of upTick2

			float _max = get(Y, row + xRows * conv, poolY + outHeight * poolX, xRows * nConvs);
			int _ind = get(I, row + xRows * conv, poolY + outHeight * poolX, xRows * nConvs);

			int x = _ind / poolHeight;
			int y = _ind % poolHeight;

			get(X, row, (poolY * poolHeight + y) + convHeight * ((poolX * poolWidth + x) + convWidth * conv), xRows) = _max;
		}
	}

	__global__
	void convolution_output_reorder_device(BatchParams bp, float * Y, float * X,
		int m, int nBranches, int aSize) {
		
		iftid {
			// tid = i + aSize * ( branch + nBranches * trainingExample )
			int i = tid % aSize;
			int curBranch = (tid % (nBranches * aSize)) / aSize;
			int training = tid / (nBranches * aSize);

			get(Y, training, i + curBranch * aSize, m) = get(X, tid, i, m * nBranches);
		}
	}

	__global__
	void convolution_output_reorder_reverse_device(BatchParams bp, float * X, float * Y,
		int m, int nBranches, int aSize) {
		
		iftid {
			// tid = i + aSize * ( branch + nBranches * trainingExample )
			int i = tid % aSize;
			int curBranch = (tid % (nBranches * aSize)) / aSize;
			int training = tid / (nBranches * aSize);

			get(X, tid, i, m * nBranches) = get(Y, training, i + curBranch * aSize, m);
		}
	}

	__global__
	void block_device(BatchParams bp, float * out, float * in, int i, int j, int outRows, int inRows) {
		iftid {
			int y = tid % outRows;
			int x = tid / outRows;

			get(out, y, x, outRows) = get(in, i + y, j + x, inRows);
		}
	}
}