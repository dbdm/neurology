#ifndef timer_h_
#define timer_h_

#include <chrono>

namespace neuro {

	typedef std::chrono::hours hours;
	typedef std::chrono::minutes minutes;
	typedef std::chrono::seconds seconds;
	typedef std::chrono::milliseconds milliseconds;
	typedef std::chrono::microseconds microseconds;
	typedef std::chrono::nanoseconds nanoseconds;

	struct Timer {
		typedef std::chrono::high_resolution_clock ClockType;

		ClockType::time_point getTimePoint() { return ClockType::now(); }

		template <typename T>
		typename T::rep getTime(ClockType::time_point startPoint) {
			return std::chrono::duration_cast<T>(ClockType::now() - startPoint).count();
		}

		template <typename T>
		typename T::rep getTime() {
			return std::chrono::duration_cast<T>(ClockType::now() - startTime).count();
		}

		void start() { startTime = getTimePoint(); }

		ClockType::time_point startTime;
	};

	extern Timer mTimer;

};

#endif timer_h_