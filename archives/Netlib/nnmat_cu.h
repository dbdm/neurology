#ifndef nnmat_cu_h_
#define nnmat_cu_h_

#include <iostream>
#include <driver_types.h>
#include <cublas_v2.h>
#include <vector>
#include <unordered_map>

#include "vglm.h"

namespace neuro {

	void handleError(cudaError_t err, const char * file, int line);
	#define HANDLE_ERROR(err) (handleError(err, __FILE__, __LINE__))

	void handleAssert(bool test, const char * text, const char * file, int line);
	#define ASSERT(test) (handleAssert(test, #test, __FILE__, __LINE__))

//#define NEURO_DEBUG

#ifdef NEURO_DEBUG
	#define DEBUG_PRINTLN(X) std::cout << X << std::endl;
#else
	#define DEBUG_PRINTLN(X) {}
#endif
	
	struct CudaStack;
	extern CudaStack * mStack;


	template <typename T>
	struct CudaPtr {
		typedef T Type;

		int size = 0, paddedSize = 0;
		T * _ptr = 0;

		operator T*() { return _ptr; }
		bool null() const { return size <= 0 || _ptr == 0; }

		void free();
	};

#ifdef NEURO_BENCHMARK
	struct benchVars {
		unsigned long long time = 0;
		unsigned long long nCalls = 0;
		unsigned long long nElements = 0;
	};
#endif

	struct CudaStack {
		cublasHandle_t handle;

		char * _data;
		size_t _curDataPos = 0;
		std::vector<int> _stack;

#ifdef NEURO_BENCHMARK
		std::unordered_map<std::string, benchVars> benchmark;
#endif

		CudaStack(size_t allocSize);
		virtual ~CudaStack();

		template <typename T>
		CudaPtr<T> malloc(int nElements) {
			CudaPtr<T> ret;
			ret.size = nElements * sizeof(T);
			ret.paddedSize = (ret.size % 16 == 0 ? ret.size : ret.size + (16 - ret.size % 16));

			ret._ptr = (T*) (_data + _curDataPos);

			_stack.push_back(ret.paddedSize);
			_curDataPos += ret.paddedSize;

			DEBUG_PRINTLN(" nAlloc " << _stack.size() << " (" << _stack.back() << "): " << ret._ptr);

			return ret;
		}

		template <typename T>
		void memcpy(const CudaPtr<T>& destination, T * from, int nElements = -1) {
			int size = (nElements == -1 ? destination.size : nElements * sizeof(T));
			ASSERT(!destination.null() && destination.size >= size);

			DEBUG_PRINTLN("memcpy h>d: " << from << " --> " << destination._ptr << " (" << size << ")");

			HANDLE_ERROR(cudaMemcpy(destination._ptr, from, size, cudaMemcpyHostToDevice));
		}

		template <typename T>
		void memcpy(T * destination, const CudaPtr<T>& from, int nElements = -1) {
			int size = (nElements == -1 ? from.size : nElements * sizeof(T));
			ASSERT(!from.null() && from.size >= size);

			DEBUG_PRINTLN("memcpy d>h: " << from._ptr << " --> " << destination << " (" << size << ")");

			HANDLE_ERROR(cudaMemcpy(destination, from._ptr, size, cudaMemcpyDeviceToHost));
		}

		template <typename ... Args>
		void free(Args& ... args) {
			_doNothing(_free<Args>(args)...);
		}

	private:
		template <typename T>
		int _free(T& arg) {
			arg.free();
			return 0;
		}

		void _doNothing(...) {}
	};


	template <typename T>
	void CudaPtr<T>::free() {
		if (!null()) {
			mStack->_curDataPos -= paddedSize;
			mStack->_stack.pop_back();
			DEBUG_PRINTLN("-nAlloc " << mStack->_stack.size() << " (" << paddedSize << "): " << _ptr);

			size = paddedSize = 0;
			_ptr = 0;
		}
	}

	void CUDA_INIT(size_t stackSize);
	void CUDA_FREE();


	struct GPUMat {

		GPUMat();
		GPUMat(const GPUMat& copy);
		GPUMat(int m, int n);
		GPUMat(int m, int n, float * hostData);
		GPUMat(int m, int n, CudaPtr<float>& deviceData);

		void populate(float * hostData);
		void fetch(float * hostPtr) const;

		void allocate();
		void free();

		void resize(int m, int n);

		operator float*();
		CudaPtr<float>& data();
		
		int rows() const;
		int cols() const;
		int size() const;

	private:
		CudaPtr<float> _data;
		int _rows, _cols;
	};
}

// Cereal will use these, don't touch 'em
namespace cereal {
	template <typename Archive>
	void save(Archive& a, neuro::GPUMat const& m) {
		int rows = m.rows(), cols = m.cols();
		a(rows, cols);
		float * data = new float[m.size()];
		m.fetch(data);
		for (int i = 0; i < m.size(); i++)
			a(data[i]);
		delete data;
	}
	template <typename Archive>
	void load(Archive& a, neuro::GPUMat& m) {
		int rows, cols;
		a(rows, cols);
		m.resize(rows, cols);
		float * data =  new float[m.size()];
		for (int i = 0; i < m.size(); i++)
			a(data[i]);
		m.populate(data);
		delete data;
	}
}

#endif /* nnmat_cu_h_ */