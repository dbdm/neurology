#include "nnfuncs_cu.h"
#include "nndevice_cu.cpp"

#ifdef NEURO_BENCHMARK
#include "timer.h"
#endif

namespace neuro {
	bool sameSize(GPUMat& a, GPUMat& b) {
		return (a.rows() == b.rows() && a.cols() == b.cols());
	}

	bool isSize(GPUMat& a, int m, int n) {
		return (a.rows() == m && a.cols() == n);
	}


	template <typename ... Args>
	struct VecFunc {
		void(*device_func)(BatchParams, Args...);
		std::string func_name;
	};


	template <typename ... Args>
	VecFunc<Args...> createFunc(void(*device_func)(BatchParams, Args...), std::string func_name) {
		VecFunc<Args...> ret;
		ret.device_func = device_func;
		ret.func_name = func_name;
		return ret;
	}

	#define vf(x) createFunc(&(x), std::string(__FUNCTION__))
	#define cf(x) x

#ifdef NEURO_BENCHMARK
	#define callCublasFunction(x, ...) \
		std::string func_name(__FUNCTION__); \
		auto start = mTimer.getTimePoint(); \
		x##(mStack->handle, __VA_ARGS__); \
		HANDLE_ERROR(cudaGetLastError()); \
		(mStack->benchmark[func_name]).time += mTimer.getTime<microseconds>(start); \
		(mStack->benchmark[func_name]).nCalls++; \
		(mStack->benchmark[func_name]).nElements += 1;
#else
	#define callCublasFunction(x, ...) \
		x(mStack->handle, __VA_ARGS__); \
		HANDLE_ERROR(cudaGetLastError());
#endif

	template <typename ... Args>
	void callCudaVectorFunction(unsigned long long size, VecFunc<Args...> func, Args ... args) {
		unsigned long long blocksPerGrid = (size + BLOCK_SIZE - 1) / BLOCK_SIZE;

#ifdef NEURO_BENCHMARK
		auto start = mTimer.getTimePoint();
#endif NEURO_BENCHMARK

		int curBlock = 0;

		while(curBlock < blocksPerGrid) {
			int i = min(blocksPerGrid - curBlock, THREAD_DIM);

			func.device_func<<<i, BLOCK_SIZE>>>({size, curBlock}, args...);

			curBlock += i;
		}

		HANDLE_ERROR(cudaGetLastError());

#ifdef NEURO_BENCHMARK
		(mStack->benchmark[func.func_name]).time += mTimer.getTime<microseconds>(start);
		(mStack->benchmark[func.func_name]).nCalls++;
		(mStack->benchmark[func.func_name]).nElements += size;
#endif
	}

	// MatC = 1 / (1 + exp(MatA))
	GPUMat& sigmoid_host(GPUMat& c, GPUMat& a) {
		ASSERT(sameSize(c, a));

		callCudaVectorFunction(a.size(), vf(sigmoid_device), (float *) c, (float *) a);

		return c;
	}

	// Inplace version of above
	GPUMat& sigmoid_host(GPUMat& a) {

		callCudaVectorFunction(a.size(), vf(sigmoid_device), (float *) a, (float *) a);

		return a;
	}

	// MatA = S(MatA) * (1 - S(MatA))
	GPUMat& sigmoid_derivative_host(GPUMat& a) {

		callCudaVectorFunction(a.size(), vf(sigmoid_derivative_inplace_device), (float *) a);

		return a;
	}

	// Y = X .* (1 - X)
	GPUMat& sigmoid_derivative_optimized_host(GPUMat& Y, GPUMat& X) {
		ASSERT(sameSize(Y, X));

		callCudaVectorFunction(X.size(), vf(sigmoid_derivative_optimized_device), (float *) Y, (float *) X);

		return Y;
	}

	// J = - 1 / m * (Y .* ln(H) + (1 - Y) .* ln(1 - H))
	// ret = sum(J)
	float logistic_cost_host(GPUMat& J, GPUMat& H, GPUMat& Y) {
		ASSERT(sameSize(J, H) && sameSize(J, H));

		callCudaVectorFunction(J.size(), vf(logistic_cost_nosum_device), (float *) J, (float *) H, (float *) Y, 1.f / J.rows());

		float sum = 0;
		callCublasFunction(cf(cublasSasum), J.size(), (const float *) J, 1, &sum);

		return sum;
	}

	// MatC = MatA + MatB
	GPUMat& add_host(GPUMat& c, GPUMat& a, GPUMat& b, float alpha, float beta) {
		ASSERT(sameSize(c, a) && sameSize(c, b));

		callCublasFunction(cf(cublasSgeam),
			CUBLAS_OP_N, CUBLAS_OP_N, a.rows(), a.cols(),
			&alpha, (const float *) a, a.rows(), &beta, (const float *) b, b.rows(), (float *) c, c.rows());
		
		return c;
	}

	// MatC = MatA + n
	GPUMat& add_host(GPUMat& c, GPUMat& a, float n) {
		ASSERT(sameSize(c, a));
		
		callCudaVectorFunction(a.size(), vf(add_device), (float *) c, (float *) a, n);

		return c;
	}

	// MatC = MatA - MatB
	GPUMat& sub_host(GPUMat& c, GPUMat& a, GPUMat& b) {

		add_host(c, a, b, 1, -1);

		return c;
	}

	// MatC = n - MatA
	GPUMat& sub_host(GPUMat& c, float n, GPUMat& a) {
		ASSERT(sameSize(c, a));
		
		callCudaVectorFunction(a.size(), vf(sub_device), (float *) c, (float *) a, n);

		return c;
	}

	// MatC = n * MatA
	GPUMat& mult_host(GPUMat& c, GPUMat& a, float n) {

		add_host(c, a, a, n, 0);

		return c;
	}

	// MatC = MatA * MatB
	GPUMat& mult_host(GPUMat& c, GPUMat& a, GPUMat& b, float alf) {
		ASSERT(isSize(c, a.rows(), b.cols()) && a.cols() == b.rows());

		const int m = a.rows(), n = b.cols(), k = a.cols();
		int lda = m, ldb = k, ldc = m;
		const float bet = 0;
		const float *alpha = &alf;
		const float *beta = &bet;

		callCublasFunction(cf(cublasSgemm),
			CUBLAS_OP_N, CUBLAS_OP_N, m, n, k,
			alpha, (const float *) a, lda, (const float *) b, ldb, beta, (float *) c, ldc);

		return c;
	}

	// MatC = MatA' * MatB
	GPUMat& mult_transpose_first_host(GPUMat& c, GPUMat& a, GPUMat& b, float alf) {
		ASSERT(isSize(c, a.cols(), b.cols()) && a.rows() == b.rows());
		const int m = a.cols(), n = b.cols(), k = a.rows();
		int lda = k, ldb = k, ldc = m;
		const float bet = 0;
		const float *alpha = &alf;
		const float *beta = &bet;

		callCublasFunction(cf(cublasSgemm),
			CUBLAS_OP_T, CUBLAS_OP_N, m, n, k,
			alpha, (const float *) a, lda, (const float *) b, ldb, beta, (float *) c, ldc);

		return c;
	}

	// MatC = MatA * MatB'
	GPUMat& mult_transpose_second_host(GPUMat& c, GPUMat& a, GPUMat& b, float alf) {
		ASSERT(isSize(c, a.rows(), b.rows()) && a.cols() == b.cols());
		const int m = a.rows(), n = b.rows(), k = a.cols();
		int lda = m, ldb = n, ldc = m;
		const float bet = 0;
		const float *alpha = &alf;
		const float *beta = &bet;

		callCublasFunction(cf(cublasSgemm),
			CUBLAS_OP_N, CUBLAS_OP_T, m, n, k,
			alpha, (const float *) a, lda, (const float *) b, ldb, beta, (float *) c, ldc);

		return c;
	}

	// MatC = MatA .* MatB
	GPUMat& mult_comp_host(GPUMat& c, GPUMat& a, GPUMat& b) {
		ASSERT(sameSize(c, a) && sameSize(c, b));

		float alpha = 1;
		float beta = 0;

		callCublasFunction(cf(cublasSgemv),
			CUBLAS_OP_N, 1, a.size(),
			&alpha, (const float *) a, 1, (const float *) b, 1, &beta, (float *) c, 1);

		return c;
	}

	// MatC = MatA'
	GPUMat& transpose_host(GPUMat& c, GPUMat& a) {
		ASSERT(isSize(c, a.cols(), a.rows()));
		float alpha = 1, beta = 0;
		int m = a.rows();
		int n = a.cols();

		callCublasFunction(cf(cublasSgeam),
			CUBLAS_OP_T, CUBLAS_OP_T, n, m,
			&alpha, (const float *) a, m, &beta, (const float *) a, m, (float *) c, n);
		
		return c;
	}

	// MatC = MatA
	GPUMat& copy_host(GPUMat& ret, GPUMat& in) {
		ASSERT(sameSize(ret, in));

		add_host(ret, in, in, 1, 0);

		return ret;
	}

	// MatA = {0}^n*m
	GPUMat& zeros_host(GPUMat& a) {

		add_host(a, a, a, 0, 0);

		return a;
	}

	GPUMat& classify_host(GPUMat& c, GPUMat& a) {
		ASSERT(a.rows() == c.rows() && c.cols() == 1);

		callCudaVectorFunction(a.rows(), vf(classify_device), (float *) c, (float *) a, a.cols());

		return c;
	}

	GPUMat& pad_ones_inplace_host(GPUMat& a) {

		callCudaVectorFunction(a.rows(), vf(set_device), (float *) a, 1.f);

		return a;
	}

	GPUMat& unpad_ones_host(GPUMat& c, GPUMat& a) {
		ASSERT(isSize(c, a.rows(), a.cols() - 1));

		CudaPtr<float> dataSection = a.data();				// Store the pointer to a's data so we can modify it
		dataSection._ptr += a.rows();						// Make it point to the section of a that aren't ones
		GPUMat tempMat(c.rows(), c.cols(), dataSection);	// Create a matrix wrapper around the pointet (no alloc)
		copy_host(c, tempMat);								// Copy the section of data not ones from a to c

		return c;
	}

	GPUMat& fill_host(GPUMat& a, float n) {
		callCudaVectorFunction(a.size(), vf(set_device), (float *) a, n);

		return a;
	}

	GPUMat& block_host(GPUMat& c, GPUMat& a, int i, int j) {
		callCudaVectorFunction(c.size(), vf(block_device), (float *) c, (float *) a, i, j, c.rows(), a.rows());

		return c;
	}


	// Applies maxpooling to the input matrix, whose format is (nFeatures) x (convSize * m) then reorders the
	//  output into (nFeatures * m) x (convSize), thereby treating features as training examples.
	// Matrix I stores all relative indices chosen
	GPUMat& maxpool_reorder_host(GPUMat& Y, GPUMat& I, GPUMat& X, glm::ivec2 convSize, int nConvs, glm::ivec2 poolSize) {
		ASSERT(convSize.x % poolSize.x == 0 && convSize.y % poolSize.y == 0);
		ASSERT(X.cols() == convSize.x * convSize.y * nConvs);
		ASSERT(isSize(Y, X.rows() * nConvs, convSize.x * convSize.y / poolSize.x / poolSize.y));
		ASSERT(sameSize(I, Y));

		// poolY + (convHeight / poolHeight) * ( poolX + (convWidth / poolWidth) * ( conv + nConvs * row ) )
		int size = X.rows() * nConvs * convSize.x * convSize.y / poolSize.x / poolSize.y;
		callCudaVectorFunction(size, vf(maxpool_reorder_device),
			(float *) Y, (float *) I, (float *) X, X.rows(), convSize.x, convSize.y, nConvs, poolSize.x, poolSize.y);

		return Y;
	}

	GPUMat& maxpool_reorder_reverse_host(GPUMat& X, GPUMat& Y, GPUMat& I, glm::ivec2 convSize, int nConvs, glm::ivec2 poolSize) {
		ASSERT(convSize.x % poolSize.x == 0 && convSize.y % poolSize.y == 0);
		ASSERT(X.cols() == convSize.x * convSize.y * nConvs);
		ASSERT(isSize(Y, X.rows() * nConvs, convSize.x * convSize.y / poolSize.x / poolSize.y));
		ASSERT(sameSize(I, Y));

		int size = X.rows() * nConvs * convSize.x * convSize.y / poolSize.x / poolSize.y;
		callCudaVectorFunction(size, vf(maxpool_reorder_reverse_device),
			(float *) X, (float *) Y, (float *) I, X.rows(), convSize.x, convSize.y, nConvs, poolSize.x, poolSize.y);

		return X;
	}

	GPUMat& convolution_output_reorder_host(GPUMat& out, GPUMat& a, int m, int nBranches, glm::ivec2 a_size) {
		ASSERT(isSize(out, m, OP(a_size) * nBranches));

		int size = m * nBranches * OP(a_size);
		callCudaVectorFunction(size, vf(convolution_output_reorder_device),
			(float *) out, (float *) a, m, nBranches, OP(a_size));

		return out;
	}

	GPUMat& convolution_output_reorder_reverse_host(GPUMat& a, GPUMat& out, int m, int nBranches, glm::ivec2 a_size) {
		ASSERT(isSize(out, m, OP(a_size) * nBranches));

		int size = m * nBranches * OP(a_size);
		callCudaVectorFunction(size, vf(convolution_output_reorder_reverse_device),
			(float *) a, (float *) out, m, nBranches, OP(a_size));

		return a;
	}

	// Automatically pads columns
	GPUMat& im2cols_pad_host(GPUMat& Y, GPUMat& X, glm::ivec2 imageSize, glm::ivec2 convSize, glm::ivec2 scanSize, glm::ivec2 stride) {
		ASSERT(X.cols() == imageSize.x * imageSize.y);
		ASSERT((imageSize.x - scanSize.x) % stride.x == 0 && (imageSize.y - scanSize.y) % stride.y == 0);
		ASSERT(scanSize.x >= stride.x && scanSize.y >= stride.y);
		ASSERT(convSize == (imageSize - scanSize) / stride + 1);
		ASSERT(isSize(Y, 1 + OP(scanSize), OP(convSize) * X.rows()));

		fill_host(Y, 1.f); // For padding

		// y + scanHeight * ( x + scanWidth * ( scanY + convHeight * ( scanX + convWidth * row ) ) )
		int size = X.rows() * scanSize.x * scanSize.y * convSize.x * convSize.y;
		callCudaVectorFunction(size, vf(im2cols_device),
			(float *) Y, (float*) X, imageSize.x, imageSize.y, convSize.x, convSize.y,
									 scanSize.x, scanSize.y, stride.x, stride.y, X.rows());

		return Y;
	}

	GPUMat& im2cols_reverse_unpad(GPUMat& X, GPUMat& Y, glm::ivec2 imageSize, glm::ivec2 convSize, glm::ivec2 scanSize, glm::ivec2 stride) {
		ASSERT(X.cols() == imageSize.x * imageSize.y);
		ASSERT((imageSize.x - scanSize.x) % stride.x == 0 && (imageSize.y - scanSize.y) % stride.y == 0);
		ASSERT(scanSize.x >= stride.x && scanSize.y >= stride.y);
		ASSERT(convSize == (imageSize - scanSize) / stride + 1);
		ASSERT(isSize(Y, 1 + OP(scanSize), OP(convSize) * X.rows()));

		zeros_host(X); // Answer will be sum of convolutional outputs

		// y + scanHeight * ( x + scanWidth * ( scanY + convHeight * ( scanX + convWidth * row ) ) )
		int size = X.rows() * scanSize.x * scanSize.y * convSize.x * convSize.y;
		callCudaVectorFunction(size, vf(im2cols_reverse_device),
			(float *) X, (float*) Y, imageSize.x, imageSize.y, convSize.x, convSize.y,
									 scanSize.x, scanSize.y, stride.x, stride.y, X.rows());

		return Y;
	}


	GPUMat pad_ones(GPUMat& in) {
		GPUMat ret(in.rows(), in.cols() + 1);
		pad_ones_inplace_host(ret);

		CudaPtr<float> ptr = ret.data();
		ptr._ptr += ret.rows();
		GPUMat tempMat(in.rows(), in.cols(), ptr);
		copy_host(tempMat, in);

		return ret;
	}

	GPUMat unpad_ones(GPUMat& in) {
		GPUMat ret(in.rows(), in.cols() - 1);

		callCudaVectorFunction(ret.size(), vf(unpad_ones_device), (float *) ret, (float *) in, in.rows());

		return ret;
	}

	GPUMat copy(GPUMat& in) {
		GPUMat ret(in.rows(), in.cols());

		return copy_host(ret, in);
	}

	GPUMat zeros(int m, int n) {
		GPUMat ret(m, n);

		return zeros_host(ret);
	}
}