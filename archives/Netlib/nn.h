#ifndef nn_h_
#define nn_h_

#include <vector>

#include "nnmat_cu.h"
#include "types.h"

namespace neuro {

	enum NetworkType : unsigned int {
		UNDEFINED,
		FEED_FORWARD,
		CONVOLUTIONAL
	};


	/*              BASE              */


	struct GPUNeuralNetworkBase {
	protected:
		int _nIn = 0, _nOut = 0;
		float _normVal = 0;

	public:
		unsigned int netType = NetworkType::UNDEFINED;

		GPUNeuralNetworkBase() {}
		GPUNeuralNetworkBase(NetworkType type, int inputLength, int outputLength):
			_nIn(inputLength), _nOut(outputLength), netType(type) {}

		virtual void train(Matrix X, Matrix Y, int iter = 100, int gpuSections = 1, int sections = 1) = 0;
		virtual Matrix operator()(Matrix X) = 0;

		Matrix classify(Matrix X, int sections = 1);

		virtual ~GPUNeuralNetworkBase() {}
	};

	// These are untested lol
	Matrix GPUMatToMatrix(GPUMat m);
	std::vector<Matrix> GPUMatToMatrix_vector(std::vector<GPUMat> v);

	GPUMat MatrixToGPUMat(Matrix m);
	std::vector<GPUMat> MatrixtoGPUMat_vector(std::vector<Matrix> v);












	/*          FEED FORWARD          */


	struct FeedForwardData {
		std::vector<GPUMat> a, z;

		virtual ~FeedForwardData();
	};

	struct GPUConvolutionalNeuralNetwork;

	struct GPUFeedForwardNeuralNetwork : GPUNeuralNetworkBase {
		friend struct GPUConvolutionalNeuralNetwork;

		GPUFeedForwardNeuralNetwork() : GPUNeuralNetworkBase() {}
		GPUFeedForwardNeuralNetwork(int inputLength, int outputLength, std::vector<int> hiddenLayerSizes);

		// If you want to use fully-connected networks by themselves
		void grad(GPUMat * out, GPUMat& X, GPUMat& Y);
		
		// If you want to use them as part of a convNet
		void grad(GPUMat * out, GPUMat& X, GPUMat& Y, GPUMat& lastDeltaOut, GPUMat& H, FeedForwardData * ff, bool takeFF);

		void train(Matrix X, Matrix Y, int iter = 100, int gpuSections = 1, int sections = 1);
		
		Matrix operator()(Matrix X);

		float cost(GPUMat& X, GPUMat& Y);

		void free();


		template <typename Archive>
		void serialize(Archive& a) {
			a(netType, _nIn, _nOut, _layers, _weights);
		}

	private:
		int _layers;
		std::vector<GPUMat> _weights;

		FeedForwardData * _feedForward(GPUMat& output, GPUMat& X);
	};






	/*          CONVOLUTIONAL          */


	struct ConvolutionalFeedForwardData {
		std::vector<GPUMat> x, // Padded output of im2cols
							h, // Activation (pre-pooling)
							p; // Indices maxpooling chose

		GPUMat convOut;			// The input of the fully-connected layers
		FeedForwardData * ffd;	// For fully-connected layers

		virtual ~ConvolutionalFeedForwardData();
	};

	struct ConvolutionalIteration {
		int nFeatures;
		glm::ivec2 featureSize;
		glm::ivec2 poolSize;
		glm::ivec2 stride;

		// These variables are calculated on init
		int nBranches;			// nFeatures[n-1].!
		glm::ivec2 inSize;		// Input Size
		glm::ivec2 convSize;	// Intermediate hidden layer size
		glm::ivec2 outSize;		// Output Size (after pooling)

		ConvolutionalIteration() {}
		ConvolutionalIteration(int nFeatures, glm::ivec2 featureSize, glm::ivec2 poolSize, glm::ivec2 stride = {1, 1}) :
			nFeatures(nFeatures), featureSize(featureSize), poolSize(poolSize), stride(stride) {};

		template <typename Archive>
		void serialize(Archive& a) {
			a(nFeatures, featureSize, poolSize, stride,
			  nBranches, inSize, convSize, outSize);
		}
	};

	struct GPUConvolutionalNeuralNetwork : GPUNeuralNetworkBase {

		GPUConvolutionalNeuralNetwork() : GPUNeuralNetworkBase() {}
		GPUConvolutionalNeuralNetwork(glm::ivec2 inputSize, int outputSize,
			std::vector<ConvolutionalIteration> iters, std::vector<int> fullyConnectedHiddenLayerSizes);

		void grad(GPUMat * out, GPUMat& X, GPUMat& Y);
		void train(Matrix X, Matrix Y, int iter = 100, int gpuSections = 1, int sections = 1);

		Matrix operator()(Matrix X);

		float cost(GPUMat& X, GPUMat& Y);

		void free();

		template <typename Archive>
		void serialize(Archive& a) {
			a(netType, _nIn, _nOut, _inSize, _iters, _init, _weights)

			if (!_initialized && (_initialized = true)) new GPUFeedForwardNeuralNetwork();
			a(*_net);
		}

		GPUFeedForwardNeuralNetwork * _net;
		std::vector<ConvolutionalIteration> _iters;
		std::vector<GPUMat>	_weights;
		glm::ivec2 _inSize;
		bool _initialized = false;

		ConvolutionalFeedForwardData * _feedForward(GPUMat& output, GPUMat& X);
	};
}

#endif /* nn_h_ */