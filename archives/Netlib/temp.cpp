Yd = SigmoidDerivative(h);
deltas = Yd .* delta_h;

for(int n = 0; n < N; n++) {
for(int feature = 0; feature < nFeatures; feature++) {
for(int prevFeature = 0; prevFeature < nPrevFeatures; prevFeature++) {
  Eigen::MatrixXd& Wtmp = W[feature][prevFeature];
  Eigen::MatrixXd& Wdtmp = Wd[feature][prevFeature];
  
  for(int row = 0; row < maxRow; row++) {
  for(int col = 0; col < maxCol; col++) {
  for(int kr = 0, colBase = prevFeature * prevFeatureSize + row * inCols + col;
    kr < kernelRows; kr++, colBase += inCols) {
    
      out.block(n, colBase, 1, kernelCols) += Wtmp.row(kr) * deltas(n, feature * featureSize + col + maxCol * row);
      Wdtmp.row(kr) += (*x).block(n, colBase, 1, kernelCols) * deltas(n, feature * featureSize + col + maxCol * row);
  
  }
  }
  }
}
}
}