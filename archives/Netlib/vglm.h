/*
 * vglm.h
 *
 *  Created on: May 24, 2015
 *      Author: Dani
 */

#ifndef VGLM_H_
#define VGLM_H_

#define GLM_FORCE_RADIANS

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>

// Because I'm fed up with typing it out
#define OP(X) (X##.x * X##.y)

#endif /* VGLM_H_ */
