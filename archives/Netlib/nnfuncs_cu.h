#ifndef nnfuncs_cu_h_
#define nnfuncs_cu_h_

#include "vglm.h"
#include "nnmat_cu.h"

namespace neuro {

	#define BLOCK_DIM 16
	#define BLOCK_SIZE BLOCK_DIM * BLOCK_DIM

	const long long int THREAD_DIM = 65535;
	const long long int THREAD_SIZE = THREAD_DIM * THREAD_DIM;



	GPUMat& sigmoid_host(GPUMat& c, GPUMat& a);
	GPUMat& sigmoid_host(GPUMat& a);

	GPUMat& sigmoid_derivative_host(GPUMat& a);

	float logistic_cost_host(GPUMat& J, GPUMat& H, GPUMat& Y);

	GPUMat& add_host(GPUMat& c, GPUMat& a, GPUMat& b, float alpha = 1, float beta = 1);
	GPUMat& add_host(GPUMat& c, GPUMat& a, float n);

	GPUMat& sub_host(GPUMat& c, GPUMat& a, GPUMat& b);
	GPUMat& sub_host(GPUMat& c, float n, GPUMat& a);

	GPUMat& mult_host(GPUMat& c, GPUMat& a, float n);
	GPUMat& mult_host(GPUMat& c, GPUMat& a, GPUMat& b, float alpha = 1);
	GPUMat& mult_transpose_first_host(GPUMat& c, GPUMat& a, GPUMat& b, float alpha = 1);
	GPUMat& mult_transpose_second_host(GPUMat& c, GPUMat& a, GPUMat& b, float alpha = 1);
	GPUMat& mult_comp_host(GPUMat& c, GPUMat& a, GPUMat& b);

	GPUMat& transpose_host(GPUMat& c, GPUMat& a);
	GPUMat& copy_host(GPUMat& c, GPUMat& a);
	GPUMat& block_host(GPUMat& c, GPUMat& a, int i, int j);
	GPUMat& zeros_host(GPUMat& a);
	GPUMat& fill_host(GPUMat& a, float n);

	GPUMat& pad_ones_inplace_host(GPUMat& a);
	GPUMat& unpad_ones_host(GPUMat& c, GPUMat& a);
	GPUMat& classify_host(GPUMat& c, GPUMat& a);
	GPUMat& maxpool_reorder_host(GPUMat& Y, GPUMat& I, GPUMat& X, glm::ivec2 convSize, int nConvs, glm::ivec2 poolSize);
	GPUMat& maxpool_reorder_reverse_host(GPUMat& X, GPUMat& Y, GPUMat& I, glm::ivec2 convSize, int nConvs, glm::ivec2 poolSize);
	GPUMat& convolution_output_reorder_host(GPUMat& out, GPUMat& a, int m, int nBranches, glm::ivec2 a_size);
	GPUMat& convolution_output_reorder_reverse_host(GPUMat& a, GPUMat& out, int m, int nBranches, glm::ivec2 a_size);
	GPUMat& im2cols_pad_host(GPUMat& Y, GPUMat& X, glm::ivec2 imageSize, glm::ivec2 convSize, glm::ivec2 scanSize, glm::ivec2 stride);
	GPUMat& im2cols_reverse_unpad(GPUMat& X, GPUMat& Y, glm::ivec2 imageSize, glm::ivec2 convSize, glm::ivec2 scanSize, glm::ivec2 stride);
	GPUMat& sigmoid_derivative_optimized_host(GPUMat& Y, GPUMat& X);

	// NOTE: These allocate a new Matrix
	GPUMat pad_ones(GPUMat& in);
	GPUMat unpad_ones(GPUMat& in);
	GPUMat copy(GPUMat& in);
	GPUMat zeros(int m, int n);
}

#endif /* nnfuncs_cu_h_ */