#include "nnmat_cu.h"

#ifdef NEURO_BENCHMARK
#include "timer.h"
#endif

#include <ctime>
#include <cstdlib>

namespace neuro {

	void handleError(cudaError_t err, const char * file, int line) {
		if (err != cudaSuccess) {
			std::cout << "<CUDA ERROR> " << cudaGetErrorString(err) << " in "
				<< file << " on line " << line << "." << std::endl;
			exit(EXIT_FAILURE);
		}
	}

	void handleAssert(bool test, const char * text, const char * file, int line) {
		if (!test) {
			std::cout << "<ASSERTION FAILED> " << text << " in "
				<< file << " on line " << line << "." << std::endl;
			exit(EXIT_FAILURE);
		}
	}

	CudaStack * mStack;
	void CUDA_INIT(size_t stackSize) {
		mStack = new CudaStack(stackSize);
		srand(time(0));
		cublasCreate(&mStack->handle);
	}
	void CUDA_FREE() {
		delete mStack;
	}

	CudaStack::CudaStack(size_t allocSize) {
		HANDLE_ERROR(cudaMalloc((void**)&_data, allocSize));

#ifdef NEURO_BENCHMARK
		mTimer.start();
#endif
	} 

	CudaStack::~CudaStack() {
#ifdef NEURO_BENCHMARK
		std::cout << std::endl << "BENCHMARK" << std::endl;
		float totalTime = mTimer.getTime<microseconds>();
		for (auto& i : mStack->benchmark)
			std::cout << i.first.c_str() << ": " 
				<< i.second.time / 1000000.f << "s ("
				<< ((float) i.second.time / totalTime * 100) << "%)\t\t"
				<< (i.second.nCalls) << " calls on " << i.second.nElements << " elements\t\t"
				<< i.second.time / 1000.f / i.second.nCalls << "ms, " << (float) i.second.time / i.second.nElements << " microseconds"
				<< std::endl;
#endif

		HANDLE_ERROR(cudaFree(_data));
	}

	GPUMat::GPUMat() : GPUMat(1, 1) {};
	GPUMat::GPUMat(const GPUMat& cpy) { _rows = cpy._rows; _cols = cpy._cols; _data = cpy._data; };
	GPUMat::GPUMat(int m, int n) : _rows(m), _cols(n) {}
	GPUMat::GPUMat(int m, int n, float * hostData) : GPUMat(m, n) { populate(hostData); }
	GPUMat::GPUMat(int m, int n, CudaPtr<float>& deviceData) : GPUMat(m, n) { _data = deviceData; };

	void GPUMat::resize(int m, int n) {
		_rows = m;
		_cols = n;
	}

	void GPUMat::populate(float * hostData) {
		mStack->memcpy(data(), hostData);
	}
	void GPUMat::fetch(float * hostPtr) const {
		mStack->memcpy(hostPtr, _data);
	}

	void GPUMat::allocate() {
		if (_data.null() || _data.size < size() * sizeof(float)) {
			_data.free();
			_data = mStack->malloc<float>(size());
		}
	}
	void GPUMat::free() { _data.free(); }

	GPUMat::operator float*() { return data(); }
	CudaPtr<float>& GPUMat::data() { allocate(); return _data; }
	int GPUMat::rows() const { return _rows; }
	int GPUMat::cols() const { return _cols; }
	int GPUMat::size() const { return _rows * _cols; }
}