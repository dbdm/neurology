// #include "nn.h"
// #include "nnfuncs_cu.h"
// #include "types.h"



// void printMat(float * mat, int m, int n) {
// 	for (int row = 0; row < m; row++) {
// 		std::cout << "\t";
// 		for (int col = 0; col < n; col++)
// 			std::cout << mat[row + col * m] << " ";
// 		std::cout << std::endl;
// 	}
// }

// void printGPUMat(GPUMat& a) {
// 	float * derp = new float[a.size()];
// 	a.fetch(derp);
// 	printMat(derp, a.rows(), a.cols());
// 	delete derp;
// }




	// caffe::GlobalInit(&argc, &argv);

	// string solver_dir = "res/proto/mnist/lenet_solver.prototxt";

	// caffe::SolverParameter solver_param;
	// caffe::ReadSolverParamsFromTextFileOrDie(solver_dir, &solver_param);

	// Caffe::set_mode(Caffe::CPU);

	// caffe::SignalHandler signal_handler(caffe::SolverAction::SNAPSHOT, caffe::SolverAction::STOP);

 // 	shared_ptr<caffe::Solver<float>> solver(caffe::SolverRegistry<float>::CreateSolver(solver_param));

 // 	solver->SetActionFunction(signal_handler.GetActionFunction());

 // 	std::cout << "START" << std::endl;
 // 	solver->Solve();
 // 	std::cout << "END" << std::endl;


	// CUDA_INIT(1024 * 1024 * 1024 / 4);
	
	// int nTraining = 100;
	// int nOffs = 0;
	// int nBatches = 1; nTraining / 100;

	// Matrix images = loadMNISTImages("res/train-images.idx3-ubyte", nTraining, nOffs);
	// Matrix labels = loadMNISTLabels("res/train-labels.idx1-ubyte", nTraining, nOffs);

	// int nTest = 10000;
	// int nBatchesTest = 1;

	// Matrix testImages = loadMNISTImages("res/t10k-images.idx3-ubyte", nTest);
	// Matrix testLabels = loadMNISTLabels("res/t10k-labels.idx1-ubyte", nTest);




	// // GPUFeedForwardNeuralNetwork net = loadNetwork<GPUFeedForwardNeuralNetwork>("feedSigmoid");


	// GPUFeedForwardNeuralNetwork net(images.cols(), labels.cols(), {75, 75});
	// net.train(images, labels, 100, 1, nBatches);

	// // saveNetwork("feedSigmoid75-75", net);

	// // std::cout << loadBMPGrayscale("test.bmp") << std::endl;
	
	// // GPUConvolutionalNeuralNetwork net({28, 28}, 10,
	// // 	{ConvolutionalIteration{10, {9, 9}, {2, 2}},			// ConvSize is 20 x 20
	// // 	 ConvolutionalIteration{ 4, {3, 3}, {2, 2}}}, {50});	// ConvSize is  8 x 8

	// // net.train(images, labels, 500, nBatches);

	// Matrix test = net.classify(testImages);

	// float nCorrect = 0;
	// for (int i = 0; i < nTest; i++) {
	// 	if (testLabels(i, test(i)) == 1) nCorrect++;
	// }

	// std::cout << "Test set: " << (nCorrect / nTest * 100.f) << "%" << std::endl;

	// net.free();
	
	// // float input[7 * 7] = {
	// // 	0, 0, 0, 0, 0, 0, 0,
	// // 	0, 1, 1, 1, 1, 0, 0,
	// // 	0, 1, 1, 2, 0, 0, 0,
	// // 	0, 2, 2, 2, 0, 2, 0,
	// // 	0, 1, 2, 0, 0, 0, 0,
	// // 	0, 1, 1, 0, 0, 1, 0,
	// // 	0, 0, 0, 0, 0, 0, 0
	// // };

	// // GPUConvolutionalNeuralNetwork net({7, 7}, 10, {ConvolutionalIteration{2, {2, 2}, {2, 2}}, ConvolutionalIteration{2, {2, 2}, {2, 2}}}, {10, 10});
	
	// // GPUMat X(7, 7, input);
	// // std::cout << "X";
	// // printGPUMat(X);
	// // std::cout << std::endl;
	// // X.resize(1, 7*7);
	
	// // GPUMat Y(1, 10);
	
	// // delete net._feedForward(Y, X);


	// // mStack->free(X);

	// CUDA_FREE();