/*
 * io.h
 *
 *  Created on: Jan 23, 2016
 *      Author: Dani
 */

#ifndef IO_H_
#define IO_H_

#include "types.h"

#include <unordered_map>
#include <iostream>
#include <sstream>
#include <fstream>

#include <cereal/archives/binary.hpp>
#include <cereal/types/vector.hpp>
#include <cereal/types/utility.hpp>
#include <cereal/types/string.hpp>
#include <cereal/types/unordered_map.hpp>

// Cereal will use these, don't touch 'em
namespace cereal {
	template <typename Archive>
	void save(Archive& a, neuro::Matrix const& m) {
		int rows = m.rows(), cols = m.cols();
		a(rows, cols);
		a(cereal::binary_data(m.data(), sizeof(neuro::Scalar) * rows * cols));
	}
	template <typename Archive>
	void load(Archive& a, neuro::Matrix& m) {
		int rows, cols;
		a(rows, cols);
		m.resize(rows, cols);
		a(cereal::binary_data(m.data(), sizeof(neuro::Scalar) * rows * cols));
	}

	template <typename Archive>
	void serialize(Archive& a, glm::ivec2& v) {
		a(v.x, v.y);
	}
}

namespace neuro {
	// Returns the byte offset if there is one and -1 if there isn't
	int getPacketOffs(char * beg, int numBytes, int packetSize, const char * cmp);
	int char4ToIntLE(char * beg); // Big Endian
	void printIOError(std::string fileName, std::string message);

	// The data is indexed (Training, Image) Output is 28*28 x [0, 1]
	// Output for the images are standard y + height * y
	Matrix loadMNISTImages(std::string fileName, unsigned int numImages = 0, unsigned int offs = 0);
	// The data is indexed (Training, Label) Output is 10 x (0 or 1)
	Matrix loadMNISTLabels(std::string fileName, unsigned int numLabels = 0, unsigned int offs = 0);

	Matrix loadBMPGrayscale(std::string fileName);

	inline std::string getNetworkString(std::string name) {
		return std::string("net/") + name + std::string(".net");	
	}

	template <typename T>
	void saveNetwork(std::string name, T network) {
		std::ofstream ofs(getNetworkString(name), std::ios::binary);
		if (!ofs.is_open()) {
			printIOError(getNetworkString(name), std::string("Could not save network ") + name + std::string("."));
			return;
		}
		cereal::BinaryOutputArchive archive(ofs);
		archive(network);
		ofs.close();
	}
	template <typename T>
	T loadNetwork(std::string name) {
		std::ifstream ifs(getNetworkString(name), std::ios::binary);
		if (!ifs.is_open())
			printIOError(getNetworkString(name), std::string("Could not load network ") + name + std::string("."));
		cereal::BinaryInputArchive archive(ifs);
		T out;
		archive(out);
		ifs.close();
		return out;
	}

	struct GPUNeuralNetworkBase;

	// Use this if you want multiple possible network types. Remember to delete the network!
	GPUNeuralNetworkBase * loadBaseNetwork(std::string name);
}



#endif /* IO_H_ */
