/*
 * io.cpp
 *
 *  Created on: Jan 23, 2016
 *      Author: Dani
 */

#include "io.h"
#include "nn.h"

#include <iostream>
#include <fstream>
#include <string>

namespace neuro {
	GPUNeuralNetworkBase * loadBaseNetwork(std::string name) {
		unsigned int type;
		{
			std::ifstream ifs(getNetworkString(name), std::ios::binary);
			if (!ifs.is_open())
				printIOError(getNetworkString(name), std::string("Could not load network ") + name + std::string("."));
			cereal::BinaryInputArchive archive(ifs);
			archive(type);
			ifs.close();
		}

		std::ifstream ifs(getNetworkString(name), std::ios::binary);
		if (!ifs.is_open())
			printIOError(getNetworkString(name), std::string("Could not load network ") + name + std::string("."));
		cereal::BinaryInputArchive archive(ifs);

		switch (type) {
		case FEED_FORWARD: {
			GPUFeedForwardNeuralNetwork * out = new GPUFeedForwardNeuralNetwork();
			archive(*out);
			return out;
		} case CONVOLUTIONAL: {
			// GPUConvolutionalNeuralNetwork * out = new GPUConvolutionalNeuralNetwork();
			// archive(*out);
			// return out;
		} default:
			return 0;
		}
	}

	int char4ToIntLE(char * beg) {
		return (beg[0] << 24) + (beg[1] << 16) + (beg[2] << 8) + beg[3];
	}

	int getPacketOffs(char * beg, int numBytes, int packetSize, const char * cmp) {
		for (int i = 0; i < numBytes; i += packetSize) {
			if (memcmp(&beg[i], cmp, packetSize) == 0) return i;
		}
		return -1;
	}

	void printIOError(std::string fileName, std::string message) {
		 std::cout << "<IO ERROR> Error with " << fileName << ": " << message << std::endl;
	}

	std::vector<char> loadBytes(std::string fileName) {
		std::ifstream ifs(fileName.c_str(), std::ios::binary|std::ios::ate);

		if (!ifs.is_open()) {
			printIOError(fileName, "File not found.");
			return std::vector<char>();
		}

		std::ifstream::pos_type pos = ifs.tellg();
		std::vector<char> data(pos);

		ifs.seekg(0, std::ios::beg);
		ifs.read(&data[0], pos);

		return data;
	}


	Matrix loadBMPGrayscale(std::string fileName) {
		int i;
		FILE* f = fopen(fileName.c_str(), "rb");

		if (f == NULL)
		throw "Argument Exception";

		unsigned char info[54];
		fread(info, sizeof(unsigned char), 54, f); // read the 54-byte header

		// extract image height and width from header
		int width = *(int*)&info[18];
		int height = *(int*)&info[22];

		int row_padded = (width*3 + 3) & (~3);
		unsigned char* data = new unsigned char[row_padded];
		unsigned char tmp;

		Matrix out(height, width);

		for(int i = 0; i < height; i++) {
			fread(data, sizeof(unsigned char), row_padded, f);
			for(int j = 0; j < width * 3; j += 3) {
				// Convert (B, G, R) to (R, G, B)
				tmp = data[j];
				data[j] = data[j+2];
				data[j+2] = tmp;

				int x = j / 3;
				int y = height - i - 1;
				out(y, x) = data[j + 0] / 255.f * 0.2989f
						  + data[j + 1] / 255.f * 0.5870f
						  + data[j + 2] / 255.f * 0.1140f;
			}
		}

		fclose(f);

		delete data;

		return out;
	}

	Matrix loadMNISTImages(std::string fileName, unsigned int numImages, unsigned int offset) {
		auto data = loadBytes(fileName);
		if (data.size() == 0)
			return Matrix();

		auto offs = [&](int n){ return &data[n * 4]; };

		// Check Magic Number
		if (char4ToIntLE(offs(0)) != 0x00000803) {
			printIOError(fileName, "Magic Number does not match training data.");
			return Matrix();
		}

		unsigned int numTrainingData = char4ToIntLE(offs(1));
		unsigned int h = char4ToIntLE(offs(2)), w = char4ToIntLE(offs(3));
		unsigned int curByte = 16;

		if (offset + numImages < numTrainingData && offset + numImages > 0) numTrainingData = numImages;
		else numTrainingData -= offset;
		Matrix out(numTrainingData, w * h);
		curByte += offset;

		for (unsigned int i = 0; i < numTrainingData; i++) {
			Matrix tempImage = Matrix(h, w);
			for (unsigned int y = 0; y < h; y++)
				for (unsigned int x = 0; x < w; x++)
					tempImage(y, x) = (Scalar) ((unsigned char)data[curByte + x + y * w + i * w * h] / 255.f);
			out.row(i) = Eigen::Map<Vector>(tempImage.data(), tempImage.size());
		}

		return out;
	}

	Matrix loadMNISTLabels(std::string fileName, unsigned int numLabels, unsigned int offset) {
		auto data = loadBytes(fileName);
		if (data.size() == 0)
			return Matrix();

		auto offs = [&](int n){ return &data[n * 4]; };

		// Check Magic Number
		if (char4ToIntLE(offs(0)) != 0x00000801) {
			printIOError(fileName, "Magic Number does not match label data.");
			return Matrix();
		}

		unsigned int numTrainingData = char4ToIntLE(offs(1));
		unsigned int curByte = 8;

		if (offset + numLabels < numTrainingData && offset + numLabels > 0) numTrainingData = numLabels;
		else numTrainingData -= offset;
		Matrix out(numTrainingData, 10);
		curByte += offset;

		for (unsigned int i = 0; i < numTrainingData; i++) {
			unsigned char y = data[curByte++];
			for (unsigned int j = 0; j < 10; j++)
				out(i, j) = (y == j? 1 : 0);
		}

		return out;
	}

}
