/*
 * activation.h
 *
 *  Created on: Jan 13, 2016
 *      Author: Dylan
 */

#ifndef SRC_ACTIVATION_H_
#define SRC_ACTIVATION_H_

#include "types.h"

#include <vector>
#include <functional>

namespace neuro {

	template <typename T>
	struct IsSomeKindOfAFunction {
		static std::vector<T> functions;
		unsigned int ID = 0;

		template <typename Archive>
		void save(Archive& a) const {
			a(ID);
		}
	};

	template <typename T>
	std::vector<T> IsSomeKindOfAFunction<T>::functions;

	template <typename T, int ID>
	struct FunctionDefinerThingy {
		static T define(T&& t) {
			t.ID = ID;
			T::functions.push_back(t);
			return t;
		}
	};


	//---------------------ACTIVATION FUNCTIONS----------------------
	struct ActivationFunction : public IsSomeKindOfAFunction<ActivationFunction> {
		ComponentwiseFunction activation, derivative;

		ActivationFunction() : ActivationFunction(sigmoid, sigmoid_derivative) {}
		ActivationFunction(ComponentwiseFunction activation, ComponentwiseFunction derivative)
			: activation(activation), derivative(derivative) {}

		template <typename Archive>
		void load(Archive& a) {
			a(ID);
			activation = ActivationFunction::functions[ID].activation;
			derivative = ActivationFunction::functions[ID].derivative;
		}

		static ActivationFunction sigmoidActivationFunction;
		static ActivationFunction defaultActivationFunction;
	};






	//---------------------POOLING FUNCTIONS----------------------
	struct PoolingFunction : public IsSomeKindOfAFunction<PoolingFunction>  {
		typedef std::function<Matrix(Matrix, Size, Size)> FunctionType;

		FunctionType pool;

		PoolingFunction() {}
		PoolingFunction(FunctionType pool) : pool(pool) {};

		template <typename Archive>
		void load(Archive& a) {
			a(ID);
			pool = PoolingFunction::functions[ID].pool;
		}

		static PoolingFunction maxPoolingFunction;
		static PoolingFunction L2PoolingFunction;
		static PoolingFunction defaultPoolingFunction;
	};
}



#endif /* SRC_ACTIVATION_H_ */
