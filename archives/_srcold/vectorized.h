/*
 * vectorized.h
 *
 *  Created on: Jan 31, 2016
 *      Author: Dani
 */

#ifndef VECTORIZED_H_
#define VECTORIZED_H_

#include "types.h"

#include <vector>

namespace neuro {

	// THIS PLACE IS ONLY FOR FULLY-VECTORIZED FUNCTIONS
	//  IF YOU ARE NOT A VECTORIZED FUNCTION, LEAVE NOW.

	struct Size;

	// Input images should be unrolled
	Matrix im2col(std::vector<Matrix> in, Size blockSize);


	// Rolls images into the correct format to the network: y + x * height
	Matrix rollImages(std::vector<Matrix> images);
	// Same as previous, but this format is with the images side-by-side instead of in a vector
	Matrix rollImages(Matrix images, Size imageSize);

	// Unrolls them into their own matrices
	std::vector<Matrix> unrollImages(Matrix images, Size imageSize);
}



#endif /* VECTORIZED_H_ */
