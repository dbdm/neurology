#include <iostream>
#include <chrono>
#include <cstdlib>
#include <ctime>
#include <functional>


#include "types.h"
#include "neuralnet.h"
#include "io.h"
#include "vectorized.h"

using namespace neuro;


int main() {
	srand(time(0));

	int nTraining = 10000;
	int nOffs = 0;
	Matrix images = loadMNISTImages("res/train-images.idx3-ubyte", nTraining, nOffs);
	Matrix labels = loadMNISTLabels("res/train-labels.idx1-ubyte", nTraining, nOffs);

	int nTest = 10000;
	Matrix testImages = loadMNISTImages("res/t10k-images.idx3-ubyte", nTest);
	Matrix testLabels = loadMNISTLabels("res/t10k-labels.idx1-ubyte", nTest);

	TrainingDataSet data(images, labels,testImages, testLabels, Matrix(0,0), Matrix(0,0));

	FeedForwardNeuralNetwork net(images.cols(),labels.cols(), {25, 25}, data);
	//LogisticRegressionLearning net(images.cols(), labels.cols(), data);

	//saveNetwork("feed", net);

	Vector test = net.classify(testImages);

	float nCorrect = 0;
	for (int i = 0; i < test.size(); i++) {
		if (testLabels(i, test(i)) == 1) nCorrect++;
	}

	std::cout << "Test set: " << (nCorrect / testImages.rows() * 100.f) << "%" << std::endl;

	return 0;
}
