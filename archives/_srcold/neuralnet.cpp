/*
 * neuralnet.cpp
 *
 *  Created on: Jan 13, 2016
 *      Author: Dylan
 */

#include "neuralnet.h"
#include "vectorized.h"

#include <limits>

namespace neuro {

	Matrix padOnes(Matrix in) {
		Matrix out = Matrix(in.rows(),in.cols()+1);
		out.col(0) = Matrix::Ones(in.rows(), 1);
		out.bottomRightCorner(in.rows(), in.cols()) = in;
		return out;
	}

	Matrix unpad(Matrix in) {
		return in.block(0, 1, in.rows(), in.cols() - 1);
	}

	void shuffleXYMatrices(Matrix& X, Matrix& Y) {
		int m = X.rows();
		for(int i = 0; i < m; i++) {
			int randIndex = (((rand()%(1<<16))<<16)+(rand()<<1%16))%m; // i dont know why i used such a complicated expression here

			Vector tempX = X.row(randIndex);
			X.row(randIndex) = X.row(i%m);
			X.row(i%m) = tempX;

			Vector tempY = Y.row(randIndex);
			Y.row(randIndex) = Y.row(i%m);
			Y.row(i%m) = tempY;
		}
	}

	float regularizationCost(Matrix weights, float lambda) {
		return weights.cwiseProduct(weights).sum() * lambda / weights.rows() / 2.f;
	}

	float regularizationCost(std::vector<Matrix> weights, float lambda) {
		float ret = 0;
		for(Matrix m:weights) {
			ret += regularizationCost(m,lambda);
		}
		return ret;
	}


	Matrix regularizationGrad(Matrix weights, float lambda) {
		return weights * lambda / weights.rows();
	}

	std::vector<Matrix> regularizationGrad(std::vector<Matrix> weights, float lambda) {
		std::vector<Matrix> ret;
		for(Matrix m:weights) {
			ret.push_back(regularizationGrad(m,lambda));
		}
		return ret;
	}

	TrainingDataSet::TrainingDataSet(Matrix X, Matrix Y, float trainingSet, float testSet, float cvSet) {
		bool useTraining, useTest, useCV;

		useTraining = trainingSet > 0.00001f;
		useTest = testSet > 0.00001f;
		useCV = cvSet > 0.00001f;

		std::vector<Matrix*> Xs;
		std::vector<Matrix*> Ys;
		std::vector<float> divs;

		if(!useTraining) { this->trainingSetX = Matrix(0,0); this->trainingSetY = Matrix(0,0); }
		else { Xs.push_back(&this->trainingSetX); Ys.push_back(&this->trainingSetY); divs.push_back(trainingSet); }

		if(!useTest)     { this->testSetX = Matrix(0,0); this->testSetY = Matrix(0,0); }
		else { Xs.push_back(&this->testSetX); Ys.push_back(&this->testSetY); divs.push_back(testSet); }

		if(!useCV)       { this->cvSetX = Matrix(0,0); this->cvSetY = Matrix(0,0); }
		else { Xs.push_back(&this->cvSetX); Ys.push_back(&this->cvSetY); divs.push_back(cvSet); }

		int m = X.rows();
		int n = X.cols();
		int k = Y.cols();

		shuffleXYMatrices(X,Y);

		int curRow = 0;
		int curTotalRow = 0;
		for(int i = 0; i < (int)Xs.size(); i++) {
			curTotalRow += divs[i] * m;
			(*Xs[i]) = Matrix(curTotalRow,n);
			(*Ys[i]) = Matrix(curTotalRow,k);
			while(curRow <= curTotalRow) {
				curRow++;
				Xs[i]->row(curRow) = X.row(curRow);
				Ys[i]->row(curRow) = Y.row(curRow);
			}
		}
	}

	Matrix randInitializeWeights(int m, int n) {
		return Matrix::Random(m, n);
	}

	LogisticRegressionLearning::LogisticRegressionLearning(int inputLength, int outputLength, TrainingDataSet dataSet,
			ActivationFunction activationFunction): NeuralNetworkBase(LogisticRegression, inputLength+1, outputLength, activationFunction) {
		weights = randInitializeWeights(this->inputLength, outputLength);
		train(dataSet);
	}

	void unrollInto(Matrix& m, const NEWMAT::ColumnVector& derp) {
		for (int i = 0; i < m.rows(); i++)
			for (int j = 0; j < m.cols(); j++)
				m(i, j) = derp(i + j * m.rows() + 1);
	}

	void rollInto(NEWMAT::ColumnVector& derp, Matrix m) {
		for (int i = 0; i < m.rows(); i++)
			for (int j = 0; j < m.cols(); j++)
				derp(i + j * m.rows() + 1) = m(i, j);
	}

	void unrollInto(std::vector<Matrix>& mats, const NEWMAT::ColumnVector& derp) {
		int curIndex = 1;
		for(Matrix& m:mats) {
			for(int i = 0; i < m.rows(); i++) {
				for(int j = 0; j < m.cols(); j++) {
					m(i,j) = derp(curIndex++);
				}
			}
		}
	}

	void rollInto(NEWMAT::ColumnVector& derp, std::vector<Matrix> mats) {
		int curIndex = 1;
		for(Matrix m:mats) {
			for(int i = 0; i < m.rows(); i++) {
				for(int j = 0; j < m.cols(); j++) {
					derp(curIndex++) = m(i,j);
				}
			}
		}
	}

	int sumSize(std::vector<Matrix> mats) {
		int ret = 0;
		for(auto m:mats) {
			ret += m.size();
		}
		return ret;
	}

	void TrainingDataSet::normalize() {
		normVal = trainingSetX.sum() / (Scalar)trainingSetX.size();
		trainingSetX = trainingSetX.array() - normVal;
	}

	template <typename T>
	void Optimizer<T>::optimize(T * trainThis, TrainingDataSet * curSet, int nDim, int maxIter) {
		const char * status_file = "test.out";

		Optimizer<T>::curSet = curSet;
		Optimizer<T>::trainThis = trainThis;

		OPTPP::NLF1 nlp(nDim, T::opt_func, T::opt_init);

		OPTPP::OptLBFGS optimizer(&nlp);
		optimizer.setUpdateModel(T::opt_updateModel);
		if (!optimizer.setOutputFile(status_file, 0))
			std::cout << "failed to open output file" << std::endl;
		optimizer.setGradTol(1.e-6);
		optimizer.setMaxBacktrackIter(10);
		optimizer.setMaxIter(maxIter);
		optimizer.optimize();
		optimizer.cleanup();
	}

	void LogisticRegressionLearning::opt_init(int ndim, NEWMAT::ColumnVector& theta) {
		if (ndim != Opt::trainThis->weights.size()) return;

		for (int i = 0; i < Opt::trainThis->weights.size(); i++)
			theta(i+1) = Opt::trainThis->weights.data()[i];
	}
	void LogisticRegressionLearning::opt_func(int mode, int ndim, const NEWMAT::ColumnVector& theta, double& fx, NEWMAT::ColumnVector& g, int& result) {
		if (ndim != Opt::trainThis->weights.size()) return;
		unrollInto(Opt::trainThis->weights, theta);
		result = 0;

		if (mode & OPTPP::NLPFunction) {
			fx = Opt::trainThis->cost(Opt::curSet->trainingSetX, Opt::curSet->trainingSetY);
			result |= OPTPP::NLPFunction;
		}

		if (mode & OPTPP::NLPGradient) {
			rollInto(g, Opt::trainThis->grad(Opt::curSet->trainingSetX, Opt::curSet->trainingSetY));
			result |= OPTPP::NLPGradient;
		}
	}
	void LogisticRegressionLearning::opt_updateModel(int k, int ndim, NEWMAT::ColumnVector theta) {
		std::cout << "Iteration number: " << k << std::endl;
	}

	void FeedForwardNeuralNetwork::opt_init(int ndim, NEWMAT::ColumnVector& theta) {
		if (ndim != sumSize(Opt::trainThis->weights)) return;

		rollInto(theta,Opt::trainThis->weights);
	}
	void FeedForwardNeuralNetwork::opt_func(int mode, int ndim, const NEWMAT::ColumnVector& theta, double& fx, NEWMAT::ColumnVector& g, int& result) {
		if (ndim != sumSize(Opt::trainThis->weights)) return;
		unrollInto(Opt::trainThis->weights, theta);
		result = 0;

		if (mode & OPTPP::NLPFunction) {
			fx = Opt::trainThis->cost(Opt::curSet->trainingSetX, Opt::curSet->trainingSetY);
			result |= OPTPP::NLPFunction;
		}

		if (mode & OPTPP::NLPGradient) {
			rollInto(g, Opt::trainThis->grad(Opt::curSet->trainingSetX, Opt::curSet->trainingSetY));
			result |= OPTPP::NLPGradient;
		}
	}
	void FeedForwardNeuralNetwork::opt_updateModel(int k, int ndim, NEWMAT::ColumnVector theta) {
		std::cout << "Iteration number: " << k << std::endl;
	}

	Scalar LogisticRegressionLearning::cost(Matrix X, Matrix Y) {
		Scalar m = X.rows();

		Matrix H = activationFunction.activation(X * weights);

		Matrix J = - (1 / m) * (Y.cwiseProduct(ln(H)) + add(-Y, 1.f).cwiseProduct(ln(add(-H, 1.f))));

		return J.sum() + regularizationCost(weights,regularizationParameter);
	}

	Matrix LogisticRegressionLearning::grad(Matrix X, Matrix Y) {
		Scalar m = X.rows();

		Matrix H = activationFunction.activation(X * weights);

		Matrix G = (1 / m) * ((Matrix) X.transpose() * (H - Y));

		return G + regularizationGrad(weights,regularizationParameter);
	}

	void LogisticRegressionLearning::train(TrainingDataSet dataSet) {
		normVal = dataSet.normVal;
		Opt::optimize(this, &dataSet, weights.size());

		std::cout << "Training set cost: " << cost(dataSet.trainingSetX, dataSet.trainingSetY) << std::endl;
		std::cout << "Test set cost: " << cost(dataSet.testSetX, dataSet.testSetY) << std::endl;
	}

	Matrix LogisticRegressionLearning::operator()(Matrix input)  {
		return activationFunction.activation(padOnes((Matrix)(input.array() - normVal)) * weights);
	}

	Vector NeuralNetworkBase::classify(Matrix input) {
		Matrix temp = (*this)(input);
		Vector ret(temp.rows());

		for (int i = 0; i < temp.rows(); i++) {
			Scalar max = std::numeric_limits<Scalar>::min(), index = 0;
			for (int j = 0; j < temp.cols(); j++) {
				if (temp(i, j) > max) {
					max = temp(i, j);
					index = j;
				}
			}
			ret(i) = index;
		}

		return ret;
	}

	FeedForwardNeuralNetwork::FeedForwardNeuralNetwork(int inputLength, int outputLength, std::vector<int> hiddenLayerSizes,
			TrainingDataSet dataSet, ActivationFunction activationFunction):
				NeuralNetworkBase(FeedForward, inputLength+1, outputLength, activationFunction), layers((int)hiddenLayerSizes.size()+2) {
		std::vector<int> allLayers;

		allLayers.push_back(this->inputLength);
		for(int i = 0; i < layers - 2; i++) allLayers.push_back(hiddenLayerSizes[i]);
		allLayers.push_back(this->outputLength);

		for(int i = 0; i < layers - 1; i++) {
			 Matrix w = randInitializeWeights(allLayers[i]+1, allLayers[i+1]);
			 weights.push_back(w);
		}
		train(dataSet);
	}

	Scalar FeedForwardNeuralNetwork::cost(Matrix X, Matrix Y) {
		X = padOnes(X);
		Scalar m = X.rows();

		Matrix H = getFeedForwardData(X).output;

		Matrix J = - (1 / m) * (Y.cwiseProduct(ln(H)) + add(-Y, 1.f).cwiseProduct(ln(add(-H, 1.f))));

		return J.sum() + regularizationCost(weights,regularizationParameter);
	}

	std::vector<Matrix> addVec(std::vector<Matrix> a, std::vector<Matrix> b) {
		std::vector<Matrix> ret;
		for(unsigned int i = 0; i < a.size(); i++)
			ret.push_back(a[i]+b[i]);
		return ret;
	}

	std::vector<Matrix> FeedForwardNeuralNetwork::grad(Matrix X, Matrix Y) {
		X = padOnes(X);
		int m = X.rows();

		std::vector<Matrix> ret;
		for(unsigned int i = 0; i < weights.size(); i++) {
			ret.push_back(Matrix::Zero(weights[i].rows(), weights[i].cols()));
		}

		std::vector<Matrix> delta;

		delta.push_back(Matrix(1,1));
		for(unsigned int i = 1; i < weights.size()+1; i++) {
			delta.push_back(Matrix::Zero(weights[i-1].rows(),1));
		}

		FeedForwardData ffd = getFeedForwardData(X);

		delta[layers-1] = ffd.output - Y;

		for(int l = layers-2; l >= 1; l--) {
			delta[l] = unpad( delta[l+1] * ((Matrix) (weights[l]).transpose()) ).cwiseProduct( activationFunction.derivative( ffd.z[l] ) );
		}

		for(unsigned int l = 0; l < ret.size(); l++) {
			ret[l] += 1/((Scalar)m) * ( (Matrix) ffd.a[l].transpose()) * delta[l+1];
		}

		return addVec(ret,regularizationGrad(weights,regularizationParameter));
	}

	void FeedForwardNeuralNetwork::train(TrainingDataSet dataSet) {
		normVal = dataSet.normVal;
		Opt::optimize(this, &dataSet, sumSize(weights));

		std::cout << "Training set cost: " << cost(dataSet.trainingSetX, dataSet.trainingSetY) << std::endl;
		std::cout << "Test set cost: " << cost(dataSet.testSetX, dataSet.testSetY) << std::endl;
	}

	Matrix FeedForwardNeuralNetwork::operator () (Matrix input) {
		return getFeedForwardData(padOnes((Matrix)(input.array() - normVal))).output;
	}

	FeedForwardData FeedForwardNeuralNetwork::getFeedForwardData(Matrix input) {
		FeedForwardData ret;

		ret.a.push_back(padOnes(input));


		ret.z.push_back(Matrix(1,1)); // this doesn't exist
		for(int i = 0; i < layers-1; i++) {
			ret.z.push_back(ret.a[i] * weights[i]);
			if(i < layers - 2)
				ret.a.push_back(padOnes(activationFunction.activation(ret.z[i+1])));
			else
				ret.a.push_back(activationFunction.activation(ret.z[i+1]));
		}

		ret.output = ret.a[layers-1];

		return ret;
	}

	ConvolutionalNeuralNetwork::ConvolutionalNeuralNetwork(Size imageSize, int outputLength,
		std::vector<ConvolutionalIteration> steps, std::vector<int> fullyConnectedLayers, TrainingDataSet data,
		ActivationFunction activation)
		: NeuralNetworkBase(Convolutional, imageSize.w * imageSize.h, outputLength, activationFunction),
		imageSize(imageSize), steps(steps), nFullyConnectedLayers(fullyConnectedLayers.size()) {

		// Calculate this to initialize hidden layers
		Size prevStep = imageSize;

		for (auto& s : steps) {
			featureDetectors.push_back(Matrix(s.featureSize.w * s.featureSize.h, s.nFeatureDetectors));

			s.hiddenLayerSize = Size{prevStep.w - s.featureSize.w + 1, prevStep.h - s.featureSize.h + 1};
			prevStep = s.poolingLayerSize = Size{s.hiddenLayerSize.w / s.poolingSize.w, s.hiddenLayerSize.h / s.poolingSize.h};
		}

		transitionWeights = Matrix(prevStep.w * prevStep.h * steps.back().nFeatureDetectors + 1, fullyConnectedLayers[0]);

		fullyConnectedLayers.push_back(outputLength);
		for (int i = 0; i < nFullyConnectedLayers; i++)
			fullyConnectedWeights[i] = Matrix(fullyConnectedLayers[i] + 1, fullyConnectedLayers[i+1]);

		train(data);
	}

	Matrix ConvolutionalNeuralNetwork::propagate(Matrix input) {
		// Only do 1 step for now because I'm lazy
		ConvolutionalIteration step = steps[0];
		//return activationFunction.activation(padOnes(rollImages(step.poolingFunction.pool(featureDetectors[0] * im2col(unrollImages(input, imageSize), step.featureSize)), step.poolingLayerSize)) * transitionWeights);
		return Matrix(1,1);
	}

	Matrix ConvolutionalNeuralNetwork::operator()(Matrix input)  {
		return Matrix();
	}

	void ConvolutionalNeuralNetwork::train(TrainingDataSet dataSet) {
	}
}
