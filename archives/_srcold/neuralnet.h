/*
 * neuralnet.h
 *
 *  Created on: Jan 13, 2016
 *      Author: Dylan
 */

#ifndef SRC_NEURALNET_H_
#define SRC_NEURALNET_H_

#define HAVE_STD
#define HAVE_NAMESPACES
#define USE_FLOATS
#undef  USE_DOUBLES

#include "types.h"
#include "activation.h"

#include <algorithm>
#include <iostream>
#include <cstdlib>
#include <utility>
#include <OPT/OptLBFGS.h>
#include <OPT/NLF.h>
#include <OPT/newmat.h>

namespace neuro {

	Matrix padOnes(Matrix in); // add bias to el matrix

#define regularizationParameter 0.00f // TODO: find someplace better for this poor soul

	// I give up
	enum NetworkType : unsigned int {
		Undefined,
		LogisticRegression,
		FeedForward,
		Convolutional
	};

	// adds the x0 component to all the data automatically
	struct TrainingDataSet {
		Matrix trainingSetX, trainingSetY, testSetX, testSetY, cvSetX, cvSetY;
		Scalar normVal = 0;
		Vector mul, add; // multiply by 'mul', then add

		TrainingDataSet(Matrix X, Matrix Y, float trainingSet = 0.7f, float testTest = 0.15, float cvSet = 0.15f);
		TrainingDataSet(Matrix trainingSetX, Matrix trainingSetY, Matrix testSetX, Matrix testSetY, Matrix cvSetX, Matrix cvSetY):
			trainingSetX(trainingSetX), trainingSetY(trainingSetY), testSetX(testSetX), testSetY(testSetY), cvSetX(cvSetX), cvSetY(cvSetY) {}

		void normalize();
	};

	// transforms a vector of length inputLength into a vector of length outputLength
	class NeuralNetworkBase {
	protected:
		int inputLength = 0, outputLength = 0;
		ActivationFunction activationFunction;
		Scalar normVal = 0;

	public:
		unsigned int netType = NetworkType::Undefined;

		// For unserialization only, create a blank class
		NeuralNetworkBase() {}
		NeuralNetworkBase(NetworkType type, int inputLength, int outputLength, ActivationFunction a):
			inputLength(inputLength), outputLength(outputLength), activationFunction(a), netType(type) {}

		// Assumes size of input == inputLength
		virtual Matrix operator () (Matrix input) = 0;
		virtual void train(TrainingDataSet data) = 0;
		Vector classify(Matrix input);

		virtual ~NeuralNetworkBase() {}

	};

	template <typename T>
	struct Optimizer {
		static T * trainThis;
		static TrainingDataSet * curSet;

		static void optimize(T * trainThis, TrainingDataSet * curSet, int nDim, int maxIter = 100);
	};

	template <typename T>
	T * Optimizer<T>::trainThis;
	template <typename T>
	TrainingDataSet * Optimizer<T>::curSet;

	struct LogisticRegressionLearning: public NeuralNetworkBase {
		typedef Optimizer<LogisticRegressionLearning> Opt;

		Matrix weights;

		// For unserialization only, create a blank class
		LogisticRegressionLearning() : NeuralNetworkBase() {};

		// Use predefined weights
		LogisticRegressionLearning(int inputLength, int outputLength, Matrix weights, ActivationFunction activationFunction = ActivationFunction::defaultActivationFunction):
			NeuralNetworkBase(LogisticRegression, inputLength+1, outputLength, activationFunction), weights(weights) {}

		// Train with the given data, random weights
		LogisticRegressionLearning(int inputLength, int outputLength, TrainingDataSet dataSet, ActivationFunction activationFunction = ActivationFunction::defaultActivationFunction);

		Scalar cost(Matrix X, Matrix Y);
		Matrix grad(Matrix X, Matrix Y);

		void train(TrainingDataSet dataSet);

		Matrix operator () (Matrix input);

		static void opt_init(int ndim, NEWMAT::ColumnVector& theta);
		static void opt_func(int mode, int ndim, const NEWMAT::ColumnVector& theta, double& fx, NEWMAT::ColumnVector& g, int& result);
		static void opt_updateModel(int k, int ndim, NEWMAT::ColumnVector theta);

		template <typename Archive>
		void serialize(Archive& a) {
			a(netType, inputLength, outputLength, normVal, weights, activationFunction);
		}
	};

	struct FeedForwardData {
		std::vector<Matrix> a;
		std::vector<Matrix> z; // z_0 is not defined;
		Matrix output;

		FeedForwardData(): output(1,1) {}
	};

	int sumSize(std::vector<Matrix> mats);

	struct FeedForwardNeuralNetwork: public NeuralNetworkBase {
		typedef Optimizer<FeedForwardNeuralNetwork> Opt;

		int layers;
		std::vector<Matrix> weights;

		// For unserialization only, create a blank class
		FeedForwardNeuralNetwork() : NeuralNetworkBase(), layers(0) {};

		// Predefined weights
		FeedForwardNeuralNetwork(int inputLength, int outputLength, std::vector<Matrix> weights, ActivationFunction activationFunction = ActivationFunction::defaultActivationFunction):
			NeuralNetworkBase(FeedForward, inputLength+1, outputLength, activationFunction), layers((int)weights.size()-1), weights(weights) {
		}

		// Train with given data, random weights
		FeedForwardNeuralNetwork(int inputLength, int outputLength, std::vector<int> hiddenLayerSizes, TrainingDataSet dataSet,
				ActivationFunction activationFunction = ActivationFunction::defaultActivationFunction);

		Scalar cost(Matrix X, Matrix Y);
		std::vector<Matrix> grad(Matrix X, Matrix Y);

		void train(TrainingDataSet dataSet);

		// Note: this pads by ones
		Matrix operator () (Matrix input);
		// This does NOT pad by ones
		FeedForwardData getFeedForwardData(Matrix input);

		static void opt_init(int ndim, NEWMAT::ColumnVector& theta);
		static void opt_func(int mode, int ndim, const NEWMAT::ColumnVector& theta, double& fx, NEWMAT::ColumnVector& g, int& result);
		static void opt_updateModel(int k, int ndim, NEWMAT::ColumnVector theta);

		template <typename Archive>
		void serialize(Archive& a) {
			a(netType, inputLength, outputLength, normVal, layers, weights, activationFunction);
		}
	};

	struct ConvolutionalIteration {
		Size featureSize;
		int nFeatureDetectors;
		Size poolingSize;		// Note: previousIterationSize - featureSize must be divisible by poolingSize
		PoolingFunction poolingFunction;

		Size hiddenLayerSize = {0, 0};
		Size poolingLayerSize = {0, 0};

		template <typename Archive>
		void serialize(Archive& a) {
			a(featureSize, nFeatureDetectors, poolingSize, poolingFunction, hiddenLayerSize, poolingLayerSize);
		}
	};

	struct ConvolutionalNeuralNetwork : public NeuralNetworkBase {
	private:
		Size imageSize;
		
		std::vector<ConvolutionalIteration> steps;
		std::vector<Matrix> featureDetectors; // Each are featureSize x nFeatures
		
		Matrix transitionWeights;

		int nFullyConnectedLayers = 0;
		std::vector<Matrix> fullyConnectedWeights;

	public:
		typedef Optimizer<ConvolutionalNeuralNetwork> Opt;

		// For serialization yada yada yada
		ConvolutionalNeuralNetwork() : NeuralNetworkBase() {};

		ConvolutionalNeuralNetwork(Size imageSize, int outputLength, std::vector<ConvolutionalIteration> steps,
			std::vector<int> fullyConnectedLayers, TrainingDataSet data,
			ActivationFunction activation = ActivationFunction::defaultActivationFunction);

		Matrix propagate(Matrix input);

		// Note: this pads by ones
		Matrix operator () (Matrix input);
		void train(TrainingDataSet data);

		static void opt_init(int ndim, NEWMAT::ColumnVector& theta);
		static void opt_func(int mode, int ndim, const NEWMAT::ColumnVector& theta, double& fx, NEWMAT::ColumnVector& g, int& result);
		static void opt_updateModel(int k, int ndim, NEWMAT::ColumnVector theta);

		template <typename Archive>
		void serialize(Archive& a) {
			a(netType, inputLength, outputLength, normVal, imageSize, steps, featureDetectors, transitionWeights, nFullyConnectedLayers, fullyConnectedWeights, activationFunction);
		}
	};

}




#endif /* SRC_NEURALNET_H_ */
