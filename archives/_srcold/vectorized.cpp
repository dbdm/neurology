/*
 * vectorized.cpp
 *
 *  Created on: Jan 31, 2016
 *      Author: Dani
 */

#include "vectorized.h"
#include "neuralnet.h"

namespace neuro {

	Matrix im2col(std::vector<Matrix> in, Size blockSize) {
		Size hiddenSize = {in[0].cols() - blockSize.w + 1, in[0].rows() - blockSize.h + 1};
		Matrix out = Matrix(blockSize.w * blockSize.h, hiddenSize.w * hiddenSize.h * in.size());

		for (int n = 0; n < (int)in.size(); n++)
			for (int j = 0; j < hiddenSize.w; j++)
				for (int i = 0; i < hiddenSize.h; i++) {
					Matrix b = in[n].block(i, j, blockSize.h, blockSize.w);
					out.col(i + j * hiddenSize.h + n * hiddenSize.h * hiddenSize.w)
							= Eigen::Map<Matrix>(b.data(), blockSize.h * blockSize.w, 1);
				}

		return out;
	}



	Matrix rollImages(std::vector<Matrix> images) {
		Matrix out = Matrix(images.size(), images[0].rows() * images[0].cols());

		for (unsigned int i = 0; i < images.size(); i++)
			out.row(i) = Eigen::Map<Vector>(images[i].data(), images[i].size());

		return out;
	}

	Matrix rollImages(Matrix images, Size imageSize) {
		Matrix out = Matrix(images.size() / imageSize.w / imageSize.h, imageSize.w * imageSize.h);

		for (int i = 0; i < out.rows(); i++) {
			Matrix b = images.block(0, i * imageSize.w, imageSize.h, imageSize.w);
			out.row(i) = Eigen::Map<Vector>(b.data(), b.size());
		}

		return out;
	}

	std::vector<Matrix> unrollImages(Matrix images, Size imageSize) {
		std::vector<Matrix> out;

		for (int i = 0; i < images.rows(); i++) {
			Matrix r = images.row(i);
			out.push_back(Matrix(Eigen::Map<Matrix>(r.data(), imageSize.h, imageSize.w)));
		}

		return out;
	}
}
