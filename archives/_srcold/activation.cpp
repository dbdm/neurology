/*
 * activation.cpp
 *
 *  Created on: Jan 13, 2016
 *      Author: Dylan
 */
#include "activation.h"

#include <cmath>

namespace neuro {

	//---------------------ACTIVATION FUNCTIONS----------------------
	#define DefineActivationFunction(a, b) \
			FunctionDefinerThingy<ActivationFunction, __COUNTER__>::define({a, b})

	ActivationFunction ActivationFunction::sigmoidActivationFunction =
			DefineActivationFunction(sigmoid, sigmoid_derivative);

	ActivationFunction ActivationFunction::defaultActivationFunction =
			ActivationFunction::sigmoidActivationFunction;


	//---------------------POOLING FUNCTIONS----------------------
	#define DefinePoolingFunction(a) FunctionDefinerThingy<PoolingFunction, __COUNTER__>::define({a})

	// Will change this later cuz hardcoding is bad

	Matrix maxPooling(Matrix m, Size inputSize, Size poolSize) {
		Size resSize = {inputSize.w / poolSize.w, inputSize.h / poolSize.h};

		Matrix out = Matrix(m.rows(), resSize.prod());

		// Will clean this up I swear
		for (int i = 0; i < out.rows(); i++)
			for (int j = 0; j < out.cols(); j++)
				out(i, j) = m.block(i*poolSize.h, j*poolSize.w, poolSize.h, poolSize.w).maxCoeff();


		return out;
	}

	Matrix L2Pooling(Matrix m, Size inputSize, Size poolSize) {
		Matrix out = Matrix(m.rows() / poolSize.h, m.cols() / poolSize.w);

		// Will clean this up I swear
		for (int i = 0; i < out.rows(); i++)
			for (int j = 0; j < out.cols(); j++)
				out(i, j) = std::sqrt(m.block(i*poolSize.h, j*poolSize.w, poolSize.h, poolSize.w).sum());


		return out;
	}


	PoolingFunction PoolingFunction::maxPoolingFunction = DefinePoolingFunction(maxPooling);

	PoolingFunction PoolingFunction::L2PoolingFunction = DefinePoolingFunction(L2Pooling);

	PoolingFunction PoolingFunction::defaultPoolingFunction = PoolingFunction::maxPoolingFunction;
}



