#ifndef VISUALIZER_CU_H
#define VISUALIZER_CU_H

#include <SDL.h>

#include <GL/glew.h>
#include <GL/gl.h>
// CUDA includes
#include <cuda_runtime.h>
#include <cuda_gl_interop.h>

// CUDA utilities and system includes
#include <helper_cuda.h>
#include <helper_cuda_gl.h>

#include <helper_functions.h>
#include <rendercheck_gl.h>

namespace neuro {

	class NetVisualizer {
	public:
		void init();
		void visualizeFeedforwardPass(GPU_MAT* a, GPU_MAT* z); // for feedforward networks (not very interesting)
	};

}


#endif