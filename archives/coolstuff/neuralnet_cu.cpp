
#include <vector>
#include <cmath>

#include <tuple>
#include <algorithm>
#include <string>
#include <cublas_v2.h>

#include "neuralnet_cu.h"
#include "reduction_kernel.h"

#define CUDA_WARN(XXX) \
    do { if (XXX != cudaSuccess) { std::cout << "CUDA Error: " << \
        cudaGetErrorString(XXX) << ", at line " << __LINE__ << " in file " << __FILE__ \
        << std::endl; exit(1); } cudaDeviceSynchronize(); } while (0)

#define GET_CUDA_ERROR() \
        { cudaError_t err = cudaGetLastError(); if(std::string(cudaGetErrorString(err)) != "no error") CUDA_WARN(err); }

// The correct place to put this, trust me
// Friendship is all about faith, trust, and pixie dust
#define regularizationParameter 0.015f

namespace neuro {

	template<typename... Args> 
	void call_host(int numElements, void(*device_function)(int, Args...), Args... args) {
		int blocksPerGrid = (numElements + THREADS_PER_BLOCK - 1) / THREADS_PER_BLOCK;
		device_function<<<blocksPerGrid, THREADS_PER_BLOCK>>>(numElements,args...);
		//GET_CUDA_ERROR();
		cudaDeviceSynchronize();
	}

	template<typename... Args>
	void call_host_bigMatrices(int matRows, int matCols, void(*device_function)(int, int, Args...), Args... args) {
		int blockRows = (matRows + THREAD_DIM.x - 1) / THREAD_DIM.x;
		int blockCols = (matRows + THREAD_DIM.x - 1) / THREAD_DIM.x;
	}

	float regularizationCost(GPU_MAT weights, float lambda) {
		// lambda/2m * sum(weights .* weights)
		GPU_MAT _a = stackAllocateGPUMAT(weights.rows,weights.cols);

		cwiseProduct_host(weights,weights,_a);
		float ret = sum_host(_a);

		mDeviceStack->pop(1);

		return lambda * ret / 2.f / weights.rows;
	}

	float regularizationCost(GPU_MAT* weights, int layers, float lambda) {
		float ret = 0;
		for(int i = 0; i < layers-1; i++) {
			ret += regularizationCost(weights[i],lambda);
		}
		return ret;
	}

	void addRegularizationGrad(GPU_MAT grad, GPU_MAT weights, float lambda) {
		// grad += lambda / m * weights

		GPU_MAT _a = stackAllocateGPUMAT(weights.rows,weights.cols);

		multiply_host(lambda/(float)weights.rows, weights, _a); // _a = lambda / m * weights
		addEquals_host(grad, _a); // grad += _a

		mDeviceStack->pop(1);
	}

	void addRegularizationGrad(GPU_MAT* grad, GPU_MAT* weights, int layers, float lambda) {
		for(int i = 0; i < layers-1; i++) {
			addRegularizationGrad(grad[i],weights[i],lambda);
		}
	}
	
	float costfunc_host(GPU_MAT X, GPU_MAT Y, GPU_MAT H, GPU_MAT* weights, int layers) {
		
		int m = X.rows;
		
		// more than 4 matrices needed here, but we can reuse them
		GPU_MAT _a = stackAllocateGPUMAT(H.rows,H.cols);
		GPU_MAT _b = stackAllocateGPUMAT(H.rows,H.cols);
		GPU_MAT _c = stackAllocateGPUMAT(H.rows,H.cols);
		GPU_MAT _d = stackAllocateGPUMAT(H.rows,H.cols);
		
		// -(1/m) * ( (Y .* (ln(H))) + ((1-Y) .* ln(1-H)) );
		
		ln_host(H,_a); // store ln(H) in _a
		cwiseProduct_host(Y,_a,_b); // store y.*_a in b
		
		// _a is no longer needed, thus we can overwrite
		
		subtract_host(1.f,H,_a); // store 1-H in _a
		ln_host(_a,_c); // store ln(_a) in _c
		
		// _a is no longer needed, thus we can overwrite
		
		subtract_host(1.f,Y,_a); // store 1-Y in _a
		cwiseProduct_host(_a,_c,_d); // store _a .* _c in _d
		
		// _a and _c are no longer needed
		
		add_host(_b,_d,_a); // store _b + _d in _a
		
		// _b and _d are no longer needed (_b, _c, and _d available)
		
		multiply_host(-1.f/m, _a, _b); // store -a/m in _b
	
		float J = sum_host(_b) + regularizationCost(weights,layers,regularizationParameter);
	
		mDeviceStack->pop(4);
		
		return J;
	}
	
	/*
	 * Device input; Device output
	 * Assumes all input matrices have already been allocated
	 */
	float feedforward_costfunc_host(GPU_MAT X, GPU_MAT Y, GPUFeedForwardNeuralNetwork* net) {
		GPU_MAT H = stackAllocateGPUMAT(Y.rows,Y.cols);
		
		feedforward_propagate_host(X,net,H);
		cudaDeviceSynchronize();
		
		float J = costfunc_host(X,Y,H,net->weights,net->layers);
		cudaDeviceSynchronize();
		
		mDeviceStack->pop(1);
		
		return J;
	}
	
	// allocates appropriate z and a matrices
	// Creates 2L matrices
	void createZandAMatrices(int batch, GPUFeedForwardNeuralNetwork* net, GPU_MAT* z, GPU_MAT* a, bool stack) {
		if(stack) {
			a[0] = stackAllocateGPUMAT(batch,net->weights[0].rows);
			z[0] = stackAllocateGPUMAT(1,1);
			for(int i = 1; i < net->layers; i++) {
				z[i] = stackAllocateGPUMAT(a[i-1].rows,net->weights[i-1].cols);
				a[i] = stackAllocateGPUMAT(z[i].rows,z[i].cols + 1);
			}
		} else {
			a[0] = GPU_MAT(batch,net->weights[0].rows);
			z[0] = GPU_MAT(1,1);
			for(int i = 1; i < net->layers; i++) {
				z[i] = GPU_MAT(a[i-1].rows,net->weights[i-1].cols);
				a[i] = GPU_MAT(z[i].rows,z[i].cols + 1);
			}
		}
		a[net->layers-1].cols -= 1;
	}
	
	/* 
	* Device input; Device output
	* Assumes input matrices have already been allocated
	*/
	void feedforward_propagate_host(GPU_MAT X, GPUFeedForwardNeuralNetwork* net, GPU_MAT* z, GPU_MAT* a, GPU_MAT H) {
		//z[0] does not exist, simply not mess with it
		padOnes_host(X,a[0]); // a[0] is just input layer
		GET_CUDA_ERROR();

		for(int i = 0; i < net->layers-1; i++) {
			multiply_host(a[i],net->weights[i],z[i+1]); // store a*theta into z
			GET_CUDA_ERROR();

			GPU_MAT tempA;
			if(i == net->layers-2)
				tempA = a[i+1];
			else
				tempA = unpadOnes(a[i+1]);
			GET_CUDA_ERROR();

			sigmoid_host(z[i+1],tempA); // store sigmoid(z) into a

			if(i != net->layers-2)
				padOnes_inplace_host(a[i+1]);
			GET_CUDA_ERROR();

			//CPU_MAT aa = createmat(a[i+1].rows,a[i+1].cols);
			//copyGPUMATtoCPUMAT(a[i+1],aa);
			//printCPUMAT(aa);
			//delete aa.data;
		}

		clone(a[net->layers-1],H);
		cudaDeviceSynchronize();
		GET_CUDA_ERROR();
	}
	
	/* 
	 * Device input; Device output
	 * Assumes input matrices have already been allocated
	 */
	void feedforward_propagate_host(GPU_MAT X, GPUFeedForwardNeuralNetwork* net, GPU_MAT H) {
		GPU_MAT* a = new GPU_MAT[net->layers];
		GPU_MAT* z = new GPU_MAT[net->layers];
		createZandAMatrices(X.rows,net,z,a,true);
		feedforward_propagate_host(X,net,z,a,H);
		mDeviceStack->pop(2*(net->layers));
		
		delete[] a;
		delete[] z;
	}

	void create_batch_feedforward(int start, int batchSize, GPUFeedForwardNeuralNetwork* net,
			GPU_MAT  X, GPU_MAT  Y, 
			GPU_MAT* x, GPU_MAT* y, GPU_MAT* H, GPU_MAT* a,  GPU_MAT* z, GPU_MAT* delta) {

		*x = stackAllocateGPUMAT(batchSize,X.cols+1);

		GPU_MAT ux = unpadOnes(*x);
		getRows_host(X,ux,start);
		padOnes_inplace_host(*x);

		*y = stackAllocateGPUMAT(batchSize,Y.cols);
		getRows_host(Y,*y,start);
		*H = stackAllocateGPUMAT(batchSize,Y.cols);
		createZandAMatrices(batchSize, net, z, a, true);
		for(int i = 1; i < net->layers; i++) {
			delta[i] = stackAllocateGPUMAT(batchSize, net->weights[i-1].cols);
		}
	}

	void delete_batch_feedforward(int layers) {
		mDeviceStack->pop(3*layers+2);
	}
	
	/*
	 * Device input; Device output
	 * Assumes all input matrices have already been allocated (this includes grad)
	 */
	void feedforward_backprop_host(GPU_MAT X, GPU_MAT Y, GPUFeedForwardNeuralNetwork* net, GPU_MAT* grad) {
		int m = X.rows;

		int batches = m / BATCH_SIZE;
		if(m%BATCH_SIZE > 0) batches += 1;

		GPU_MAT* delta = new GPU_MAT[net->layers];
		GPU_MAT* a = new GPU_MAT[net->layers];
		GPU_MAT* z = new GPU_MAT[net->layers];

		for(int i = 0; i < net->layers-1; i++) {
			zeros(grad[i]);
		}

		for(int i = 0; i < batches; i++) {

			int thisBatch = min(BATCH_SIZE, m-i*BATCH_SIZE);

			GPU_MAT x,y,H;

			create_batch_feedforward(i*BATCH_SIZE, thisBatch, net, X, Y, &x, &y, &H, a, z, delta); 

			feedforward_propagate_host(x,net,z,a,H);

			subtract_host(H,y, delta[net->layers-1]);
			GET_CUDA_ERROR();

			for(int l = net->layers-2; l >= 1; l--) {
				GPU_MAT _a = stackAllocateGPUMAT(net->weights[l].cols,net->weights[l].rows);
				GPU_MAT _b = stackAllocateGPUMAT(delta[l+1].rows,net->weights[l].rows);
				GPU_MAT _c = stackAllocateGPUMAT(delta[l+1].rows,net->weights[l].rows-1);
				GPU_MAT _d = stackAllocateGPUMAT(delta[l+1].rows,net->weights[l].rows);

				transpose_host(net->weights[l],_a); // store the transpose of weights[l] in _a
				GET_CUDA_ERROR();
				multiply_host(delta[l+1],_a,_b); // store the product of delta[l+1] and _a into _b
				GET_CUDA_ERROR();
				_b = unpadOnes(_b); // unpad _b
				GET_CUDA_ERROR();
				sigmoidDerivative_host(z[l],_c); // store the derivative of z[l] in _c
				GET_CUDA_ERROR();
				cwiseProduct_host(_b,_c,delta[l]); // store a .* b in delta[l]
				GET_CUDA_ERROR();
				
				mDeviceStack->pop(4); // pop all matrices that have been created for this iteration
			}

			for(unsigned int l = 0; l < net->layers-1; l++) {
				GPU_MAT _a = stackAllocateGPUMAT(a[l].cols,a[l].rows);
				GPU_MAT _b = stackAllocateGPUMAT(grad[l].rows,grad[l].cols);
				GPU_MAT _c = stackAllocateGPUMAT(grad[l].rows,grad[l].cols);
				
				transpose_host(a[l],_a); // store the transpose of a[l] in _a
				multiply_host(_a, delta[l+1], _b); // store _a * delta[l+1] in _b
				multiply_host(1.f/m,_b,_c); // store _b / m in grad
				addEquals_host(grad[l],_c);
				GET_CUDA_ERROR();
				
				mDeviceStack->pop(3);
			}

			delete_batch_feedforward(net->layers);
		}

		addRegularizationGrad(grad,net->weights,net->layers,regularizationParameter);
		
		delete[] a;
		delete[] z;
		delete[] delta;
		GET_CUDA_ERROR();
	}
	
	void GPUFeedForwardNeuralNetwork::train(GPU_MAT X, GPU_MAT Y) {
		float alpha = 3;
		
		GET_CUDA_ERROR();
		GPU_MAT* grad = new GPU_MAT[layers-1];
		for(int i = 0; i < layers-1; i++)
			grad[i] = stackAllocateGPUMAT(weights[i].rows,weights[i].cols);
		GET_CUDA_ERROR();

		for(int i = 0; i < 1000; i++) {
			
			// End console spam today
			if(i%50==0) {
				std::cout << "iteration: " << i << std::endl;
			}

			// Cost function currently too costly to be running it every iteration
			if(i%50==-1) {
				float cost = feedforward_costfunc_host(X,Y,this);
				GET_CUDA_ERROR();
				std::cout << cost << std::endl;
			}

			feedforward_backprop_host(X,Y,this,grad);
			GET_CUDA_ERROR();

			// CPU_MAT g = createmat(grad[layers-2].rows,grad[layers-2].cols);
			// copyGPUMATtoCPUMAT(grad[layers-2],g);
			// printCPUMAT(g);
			// delete[] g.data;

			for(int l = 0; l < layers-1; l++) {
				GPU_MAT _a = stackAllocateGPUMAT(weights[l].rows,weights[l].cols);
				multiply_host(alpha,grad[l],_a);
				GET_CUDA_ERROR();
				subtractEquals_host(weights[l], _a);
				GET_CUDA_ERROR();
				mDeviceStack->pop(1);
			}
		}

		mDeviceStack->pop(layers - 1);
		delete[] grad;
	}
	
	GPUFeedForwardNeuralNetwork::GPUFeedForwardNeuralNetwork(std::vector<CPU_MAT> host_weights) {
		layers = host_weights.size()+1;
		weights = new GPU_MAT[host_weights.size()];
		for(unsigned int i = 0; i < host_weights.size(); i++) {
			weights[i] = GPU_MAT(host_weights[i].rows,host_weights[i].cols);
			copyCPUMATtoGPUMAT(host_weights[i],weights[i]);
		}
		inputSize = host_weights[0].rows;
	}
	
	void GPUFeedForwardNeuralNetwork::train(CPU_MAT X, CPU_MAT Y) {
		GPU_MAT gX = stackAllocateGPUMAT(X.rows,X.cols);
		GPU_MAT gY = stackAllocateGPUMAT(Y.rows,Y.cols);
		copyCPUMATtoGPUMAT(X,gX);
		copyCPUMATtoGPUMAT(Y,gY);
		train(gX,gY);
		mDeviceStack->pop(2);
	}

	__global__ void classify_global(int numBatches, float* H, int numCols, int* out) {
		int batchNumber = threadIdx.x + blockIdx.x * blockDim.x;
		if(batchNumber < numBatches) {
			int winner = 0;
			float confidence = 0.f;
			for(int i = 0; i < numCols; i++) {
				if(H[batchNumber + i * numBatches] > confidence) {
					winner = i;
					confidence = H[batchNumber + i * numBatches];
				}
			}
			out[batchNumber] = winner;
		}
	}

	void GPUFeedForwardNeuralNetwork::classify(GPU_MAT X, int* out) {
		int batch = X.rows;
		GPU_MAT H = stackAllocateGPUMAT(batch,weights[layers-2].cols);
		feedforward_propagate_host(X,this,H);
		call_host(X.rows, &classify_global, H.data, H.cols, out);
		mDeviceStack->pop(1);
	}

	int* GPUFeedForwardNeuralNetwork::classify(CPU_MAT X) {
		std::cout << mDeviceStack->objects.size() << std::endl;
		GPU_MAT _X = stackAllocateGPUMAT(X.rows,X.cols);
		int* gpu_out = mDeviceStack->makeNew<int>(X.rows);

		copyCPUMATtoGPUMAT(X,_X);
		classify(_X,gpu_out);

		int* ret = new int[X.rows];
		CUDA_WARN(cudaMemcpy(ret, gpu_out, X.rows * sizeof(int), cudaMemcpyDeviceToHost));

		mDeviceStack->pop(2);
		return ret;
	}
	
}