#include <vector>
#include <cmath>

#include <tuple>
#include <algorithm>
#include <string>
#include <cublas_v2.h>
#include <iostream>

#include "matmath_cu.h"

#define CUDA_WARN(XXX) \
    do { if (XXX != cudaSuccess) { std::cout << "CUDA Error: " << \
        cudaGetErrorString(XXX) << ", at line " << __LINE__ << " in file " << __FILE__ \
        << std::endl; exit(1); } cudaDeviceSynchronize(); } while (0)

#define GET_CUDA_ERROR() \
        { cudaError_t err = cudaGetLastError(); if(std::string(cudaGetErrorString(err)) != "no error") CUDA_WARN(err); }

namespace neuro {
	cublasHandle_t mCUBLAS;

	void CUDA_CHAD_INIT() {
		// Allocate 0.125 GB of memory on device
		mDeviceStack = new DeviceStack(1024 * 1024 * 1024);
		GET_CUDA_ERROR();
		cublasCreate(&mCUBLAS);
		GET_CUDA_ERROR();
		std::cout << "IT HAS BEGUN" << std::endl;
	}


	/**
	* Matrix multiplication (CUDA Kernel) on the device: C = A * B
	* wA is A's width and wB is B's width
	*/
	__global__
	void matrixMulCUDA(float *A, float *B, float *C, int wA, int wB)
	{
	    // Block index
	    int bx = blockIdx.x;
	    int by = blockIdx.y;

	    // Thread index
	    int tx = threadIdx.x;
	    int ty = threadIdx.y;

	    // Index of the first sub-matrix of A processed by the block
	    int aBegin = wA * BLOCK_SIZE * by;

	    // Index of the last sub-matrix of A processed by the block
	    int aEnd   = aBegin + wA - 1;

	    // Step size used to iterate through the sub-matrices of A
	    int aStep  = BLOCK_SIZE;

	    // Index of the first sub-matrix of B processed by the block
	    int bBegin = BLOCK_SIZE * bx;

	    // Step size used to iterate through the sub-matrices of B
	    int bStep  = BLOCK_SIZE * wB;

	    // Csub is used to store the element of the block sub-matrix
	    // that is computed by the thread
	    float Csub = 0;

	    // Loop over all the sub-matrices of A and B
	    // required to compute the block sub-matrix
	    for (int a = aBegin, b = bBegin;
	         a <= aEnd;
	         a += aStep, b += bStep)
	    {

	        // Declaration of the shared memory array As used to
	        // store the sub-matrix of A
	        __shared__ float As[BLOCK_SIZE][BLOCK_SIZE];

	        // Declaration of the shared memory array Bs used to
	        // store the sub-matrix of B
	        __shared__ float Bs[BLOCK_SIZE][BLOCK_SIZE];

	        // Load the matrices from device memory
	        // to shared memory; each thread loads
	        // one element of each matrix
	        As[ty][tx] = A[a + wA * ty + tx];
	        Bs[ty][tx] = B[b + wB * ty + tx];

	        // Synchronize to make sure the matrices are loaded
	        __syncthreads();

	        // Multiply the two matrices together;
	        // each thread computes one element
	        // of the block sub-matrix
	#pragma unroll

	        for (int k = 0; k < BLOCK_SIZE; ++k)
	        {
	            Csub += As[ty][k] * Bs[k][tx];
	        }

	        // Synchronize to make sure that the preceding
	        // computation is done before loading two new
	        // sub-matrices of A and B in the next iteration
	        __syncthreads();
	    }

	    // Write the block sub-matrix to device memory;
	    // each thread writes one element
	    int c = wB * BLOCK_SIZE * by + BLOCK_SIZE * bx;
	    C[c + wB * ty + tx] = Csub;
	}
	
	#define createmat(m, n) CPU_MAT(m, n, new float[m*n])

	void gpu_blas_mmul(cublasHandle_t &handle, const float *A, const float *B, float *C, const int m, const int k, const int n) {
		int lda=m,ldb=k,ldc=m;
		const float alf = 1;
		const float bet = 0;
		const float *alpha = &alf;
		const float *beta = &bet;

		// Do the actual multiplication
		cublasSgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc);
	}
	
	void multiply_host(GPU_MAT a, GPU_MAT b, GPU_MAT c) {
		gpu_blas_mmul(mCUBLAS, a.data, b.data, c.data, a.rows, a.cols, b.cols);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	template<typename... Args> 
	void call_host(int numElements, void(*device_function)(int, Args...), Args... args) {
		int blocksPerGrid = (numElements + THREADS_PER_BLOCK - 1) / THREADS_PER_BLOCK;
		device_function<<<blocksPerGrid, THREADS_PER_BLOCK>>>(numElements,args...);
		//GET_CUDA_ERROR();
		cudaDeviceSynchronize();
	}

	template<typename... Args>
	void call_host_bigMatrices(int matRows, int matCols, void(*device_function)(int, int, Args...), Args... args) {
		int blockRows = (matRows + THREAD_DIM.x - 1) / THREAD_DIM.x;
		int blockCols = (matRows + THREAD_DIM.x - 1) / THREAD_DIM.x;
	}
	
	DeviceStack* mDeviceStack;
	
	void clone(GPU_MAT in, GPU_MAT out) {
		cudaMemcpy(out.data,in.data,in.rows*in.cols*sizeof(float),cudaMemcpyDeviceToDevice);
		//GET_CUDA_ERROR();
		cudaDeviceSynchronize();
	}

	void add_host(GPU_MAT a, GPU_MAT b, GPU_MAT c) {
		float alpha = 1, beta = 1;
		cublasSgeam(mCUBLAS, CUBLAS_OP_N, CUBLAS_OP_N, a.rows, a.cols, &alpha, a.data, a.rows, &beta, b.data, b.rows, c.data, c.rows);
	}

	void subtract_host(GPU_MAT a, GPU_MAT b, GPU_MAT c) {
		float alpha = 1, beta = -1;
		cublasSgeam(mCUBLAS, CUBLAS_OP_N, CUBLAS_OP_N, a.rows, a.cols, &alpha, a.data, a.rows, &beta, b.data, b.rows, c.data, c.rows);
	}

	GPU_MAT stackAllocateGPUMAT(int rows, int cols) {
		float* data = mDeviceStack->makeNew<float>(rows * cols);
		cudaDeviceSynchronize();
		return GPU_MAT(rows,cols,data);
	}

	__global__ void padOnes_inplace_global(int numElements, float* in) {
		int index = threadIdx.x + blockIdx.x * blockDim.x;
		if(index < numElements)
		in[index] = 1.f;
	}

	void padOnes_inplace_host(GPU_MAT in) {
		call_host(in.rows, &padOnes_inplace_global, in.data);
	}

	__global__ void getRows_global(int numElements, float* a, float* out, int inRows, int inCols, int startRow, int outRows) {
		int index = threadIdx.x + blockIdx.x * blockDim.x;
		int inRow = index%outRows + startRow;
		int inCol = index/outRows;
		
		if(inCol < inCols && inRow < inRows) {
			out[inRow - startRow + inCol * outRows] = a[inRow + inCol * inRows];
		}
	}
	
	void getRows_host(GPU_MAT in, GPU_MAT out, int start) {
		call_host(out.rows * out.cols, &getRows_global, in.data, out.data, in.rows, in.cols, start, out.rows);
	}
	
	GPU_MAT getCols_host(GPU_MAT in, int start, int numCols) {
		GPU_MAT ret = GPU_MAT(in.rows,numCols,in.data + start * in.rows);
		return ret;
	}
	
	GPU_MAT getCol_host(GPU_MAT in, int col) {
		return getCols_host(in,col,1);
	}
	
	__global__ void cwiseProduct_global(int numElements, float* _a, float* _b, float* out) {
		int index = threadIdx.x + blockIdx.x * blockDim.x;
		
		if(index < numElements) {
			out[index] = _a[index] * _b[index];
		}
	}
	
	void addEquals_host(GPU_MAT a, GPU_MAT b) {
		float alpha = 1, beta = 1;
		cublasSgeam(mCUBLAS, CUBLAS_OP_N, CUBLAS_OP_N, a.rows, a.cols, &alpha, a.data, a.rows, &beta, b.data, b.rows, a.data, a.rows);
	}
	
	void subtractEquals_host(GPU_MAT a, GPU_MAT b) {
		float alpha = 1, beta = -1;
		cublasSgeam(mCUBLAS, CUBLAS_OP_N, CUBLAS_OP_N, a.rows, a.cols, &alpha, a.data, a.rows, &beta, b.data, b.rows, a.data, a.rows);
	}
	
	void cwiseProduct_host(GPU_MAT a, GPU_MAT b, GPU_MAT c) {
		call_host(a.rows * a.cols, &cwiseProduct_global, a.data, b.data, c.data);
	}
	
	__global__ void add_global_b(int numElements, float a, float* b, float* out) {
		int index = threadIdx.x + blockIdx.x * blockDim.x;
		
		if(index < numElements) {
			out[index] = a + b[index];
		}
	}
	
	__global__ void subtract_global_b(int numElements, float* a, float b, float* out) {
		int index = threadIdx.x + blockIdx.x * blockDim.x;
		
		if(index < numElements) {
			out[index] = a[index] - b;
		}
	}
	
	__global__ void subtract_global_c(int numElements, float a, float* b, float* out) {
		int index = threadIdx.x + blockIdx.x * blockDim.x;
		
		if(index < numElements) {
			out[index] = a - b[index];
		}
	}
	
	void add_host(float a, GPU_MAT b, GPU_MAT c) {
		call_host(b.rows*b.cols,&add_global_b,a,b.data,c.data);
	}
	
	void subtract_host(GPU_MAT a, float b, GPU_MAT c) {
		call_host(a.rows*a.cols,&subtract_global_b,a.data,b,c.data);
	}
	
	void subtract_host(float a, GPU_MAT b, GPU_MAT c) {
		call_host(b.rows*b.cols,&subtract_global_c,a,b.data,c.data);
	}
	
	__global__ void multiply_global(int numElements, float a, float* b, float* out) {
		int index = threadIdx.x + blockIdx.x * blockDim.x;
		
		if(index < numElements) {
			out[index] = a * b[index];
		}
	}
	
	void multiply_host(float a, GPU_MAT b, GPU_MAT c) {
		float alpha = a, beta = 0;
		cublasSgeam(mCUBLAS, CUBLAS_OP_N, CUBLAS_OP_N, b.rows, b.cols, &alpha, b.data, b.rows, &beta, b.data, b.rows, c.data, c.rows);
	}
	
	__device__ float sigmoid_device(float in) {
		return 1.f/(1.f+exp(-in));
	}
	
	__global__ void sigmoid_global(int numElements, float* in, float* out) {
		int index = threadIdx.x + blockIdx.x * blockDim.x;
		
		if(index < numElements) {
			out[index] = sigmoid_device(in[index]);
		}
	}
	
	void sigmoid_host(GPU_MAT in, GPU_MAT out) {
		call_host(in.rows*in.cols,&sigmoid_global,in.data,out.data);
	}
	
	__device__ float sigmoidDerivative_device(float in) {
		float s = sigmoid_device(in);
		return s*(1.f-s);
	}
	
	__global__ void sigmoidDerivative_global(int numElements, float* in, float* out) {
		int index = threadIdx.x + blockIdx.x * blockDim.x;
		
		if(index < numElements) {
			out[index] = sigmoidDerivative_device(in[index]);
		}
	}
	
	void sigmoidDerivative_host(GPU_MAT in, GPU_MAT out) {
		call_host(in.rows*in.cols,&sigmoidDerivative_global,in.data,out.data);
	}
	
	void transpose_host(GPU_MAT in, GPU_MAT out) {
		// geam preforms  C = a*op(A) + b*op(B) , so we set op(A) to transpose, a to 1, and b to 0
		float alpha = 1, beta = 0;
		int m = in.rows;
		int n = in.cols;
		cublasSgeam(mCUBLAS, CUBLAS_OP_T, CUBLAS_OP_T, n, m, &alpha, in.data, m, &beta, in.data, m, out.data, n);
	}

	void zeros(GPU_MAT out) {
		float alpha = 0, beta = 0;
		cublasSgeam(mCUBLAS, CUBLAS_OP_N, CUBLAS_OP_N, out.rows, out.cols, &alpha, out.data, out.rows, &beta, out.data, out.rows, out.data, out.rows);
	}
	
	GPU_MAT unpadOnes(GPU_MAT in) {
		return getCols_host(in,1,in.cols-1);
	}
	
	__global__ void padOnes_global(int numElements, float* in, float* out, int outRows, int outCols) {
		int index = threadIdx.x + blockIdx.x * blockDim.x;
			
		int outCol = index/outRows;
		
		if(outCol < 1) {
			out[index] = 1.f;
		} else if(outCol < outCols) {
			out[index] = in[index - outRows];
		}
	}
	
	void padOnes_host(GPU_MAT in, GPU_MAT out) {
		call_host(out.rows * out.cols, &padOnes_global, in.data, out.data, out.rows, out.cols);
	}
	
	__global__ void ln_global(int numElements, float* in, float* out) {
		int index = threadIdx.x + blockIdx.x * blockDim.x;
		
		if(index < numElements) {
			out[index] = log(in[index]);
		}
	}
	
	void ln_host(GPU_MAT in, GPU_MAT out) {
		call_host(in.rows*in.cols,&ln_global,in.data,out.data);
	}
	
	float sum_host(GPU_MAT in) {
		float* idata = in.data;
		int threads = THREADS_PER_BLOCK;
		int smemSize = (threads <= 32) ? 2 * threads * sizeof(float) : threads * sizeof(float);
		int size = in.rows*in.cols;
		int whichKernel = 6;
		int blocksPerGrid = (size + THREADS_PER_BLOCK - 1) / THREADS_PER_BLOCK;
		float* odata = mDeviceStack->makeNew<float>(smemSize);
		
		//reduce<float>(size,threads,blocksPerGrid,whichKernel,idata,odata);
		float ret = 0;
		//cudaMemcpy(odata,&ret, sizeof(float),cudaMemcpyDeviceToHost);

		float * derp = new float[in.size()];

		cudaMemcpy(derp, idata, in.size() * sizeof(float),cudaMemcpyDeviceToHost);
		cudaDeviceSynchronize();
		for (int i = 0; i < in.size(); i++)
		ret += derp[i];

		delete[] derp;

		mDeviceStack->pop(1);

		return ret;
	}
	
	float magnitude_host(GPU_MAT in) {
		GPU_MAT _a = stackAllocateGPUMAT(in.rows,in.cols);
		cudaDeviceSynchronize();
		cwiseProduct_host(in,in,_a);
		cudaDeviceSynchronize();
		float ret = sqrt(sum_host(_a));
		cudaDeviceSynchronize();
		mDeviceStack->pop(1);
		cudaDeviceSynchronize();
		return ret;
	}
	
	void getRows_array(GPU_MAT* arr, int num, int begin, GPU_MAT* ret) {
		for(int i = 0; i < num; i++) {
			getRows_host(arr[i],ret[i],begin);
		}
	}


	
	DeviceStack::DeviceStack(size_t size) { // should be sizeof(dataType) * size
		CUDA_WARN(cudaMalloc((void**) &handle, size));
	}
	
	DeviceStack::~DeviceStack() {
		CUDA_WARN(cudaFree(handle));
	}
	
	GPU_MAT::GPU_MAT(int rows, int cols): rows(rows), cols(cols) {
		CUDA_WARN(cudaMalloc((void**) &data, sizeof(float) * rows * cols));
	}
	
	void GPU_MAT::dealloc() {
		CUDA_WARN(cudaFree(data));
	}

	void copyGPUMATtoCPUMAT(GPU_MAT in, CPU_MAT out) {
		CUDA_WARN(cudaMemcpy(out.data,in.data,in.size()*sizeof(float),cudaMemcpyDeviceToHost));
	}

	void copyCPUMATtoGPUMAT(CPU_MAT in, GPU_MAT out) {
		CUDA_WARN(cudaMemcpy(out.data,in.data,in.size()*sizeof(float),cudaMemcpyHostToDevice));
	}













	/***************************
	 *
	 *       FOR TESTING
	 *
	 ***************************/


	struct ivec2 { int x; int y; };

	// He's a fuuuuuun guyyyyy
	void runAndPrint(void (*fun)(GPU_MAT), ivec2 inSize, std::string fname = "After applying a fun guy") {
		CPU_MAT in =  CPU_MAT(inSize.x,  inSize.y,  new float[inSize.x  * inSize.y]);

		randInitializeWeights(in);

		GPU_MAT _in = stackAllocateGPUMAT(in.rows,in.cols);

		std::cout << "in: " << std::endl;
		printCPUMAT(in);

		copyCPUMATtoGPUMAT(in, _in);
		fun(_in);
		copyGPUMATtoCPUMAT(_in, in);

		std::cout << std::endl << fname << ":" << std::endl;
		printCPUMAT(in);

		delete[] in.data;
		mDeviceStack->pop(1);
	}

	// He's a fuuuuuun guyyyyy
	void runAndPrint(void (*fun)(GPU_MAT,GPU_MAT), ivec2 inSize, ivec2 outSize, std::string fname = "After applying a fun guy") {
		CPU_MAT in =  CPU_MAT(inSize.x,  inSize.y,  new float[inSize.x  * inSize.y]);
		CPU_MAT out = CPU_MAT(outSize.x, outSize.y, new float[outSize.x * outSize.y]);

		randInitializeWeights(in);

		GPU_MAT _in = stackAllocateGPUMAT(in.rows,in.cols);
		GPU_MAT _out = stackAllocateGPUMAT(out.rows,out.cols);

		copyCPUMATtoGPUMAT(in, _in);
		fun(_in,_out);
		copyGPUMATtoCPUMAT(_out, out);

		std::cout << "in: " << std::endl;
		printCPUMAT(in);
		std::cout << std::endl << fname << ":" << std::endl;
		printCPUMAT(out);

		delete[] out.data;
		delete[] in.data;
		mDeviceStack->pop(2);
	}

	void runAndPrint(void (*fun)(GPU_MAT,GPU_MAT,GPU_MAT), ivec2 inSize, ivec2 in2Size, ivec2 outSize, std::string fname = "After applying a fun guy") {
		CPU_MAT a =  CPU_MAT(inSize.x,  inSize.y,  new float[inSize.x  * inSize.y]);
		CPU_MAT b =  CPU_MAT(in2Size.x,  in2Size.y,  new float[in2Size.x  * in2Size.y]);
		CPU_MAT out = CPU_MAT(outSize.x, outSize.y, new float[outSize.x * outSize.y]);

		randInitializeWeights(a);
		randInitializeWeights(b);

		GPU_MAT _a = stackAllocateGPUMAT(a.rows,a.cols);
		GPU_MAT _b = stackAllocateGPUMAT(b.rows,b.cols);
		GPU_MAT _out = stackAllocateGPUMAT(out.rows,out.cols);

		copyCPUMATtoGPUMAT(a, _a);
		copyCPUMATtoGPUMAT(b, _b);
		fun(_a, _b,_out);
		copyGPUMATtoCPUMAT(_out, out);

		std::cout << "a: " << std::endl;
		printCPUMAT(a);
		std::cout << "b: " << std::endl;
		printCPUMAT(b);
		std::cout << std::endl << fname << ":" << std::endl;
		printCPUMAT(out);

		delete[] out.data;
		delete[] a.data;
		delete[] b.data;
		mDeviceStack->pop(2);
	}

	void muloneFourth(GPU_MAT a, GPU_MAT b) { multiply_host(0.25,a,b); }
	void getSecondAndThirdRow(GPU_MAT a, GPU_MAT b) { return getRows_host(a,b,1); }

	void runFunctionTests() {
		std::vector< std::tuple<void(*)(GPU_MAT), ivec2, std::string> > unarySelfOps = {
			std::make_tuple<void(*)(GPU_MAT), ivec2, std::string> ( &padOnes_inplace_host, ivec2{5,3}, std::string("padded inplace") )
		};

		for(int i = 0; i < unarySelfOps.size(); i++) {
			void (*fun)(GPU_MAT);
			ivec2 inSize;
			std::string fname;

			std::tie   (fun, inSize, fname) = unarySelfOps[i];
			runAndPrint(fun, inSize, fname);
		}


		std::vector< std::tuple<void(*)(GPU_MAT,GPU_MAT), ivec2, ivec2, std::string> > unaryOps = {
			std::make_tuple<void(*)(GPU_MAT,GPU_MAT), ivec2, ivec2, std::string> ( &sigmoid_host, ivec2{2,5}, ivec2{2,5}, std::string("sigmoid") ),
			std::make_tuple<void(*)(GPU_MAT,GPU_MAT), ivec2, ivec2, std::string> ( &transpose_host, ivec2{3,5}, ivec2{5,3}, std::string("transpose") ),
			std::make_tuple<void(*)(GPU_MAT,GPU_MAT), ivec2, ivec2, std::string> ( &getSecondAndThirdRow, ivec2{5,3}, ivec2{2,3}, std::string("second and third row") ),
			std::make_tuple<void(*)(GPU_MAT,GPU_MAT), ivec2, ivec2, std::string> ( &padOnes_host, ivec2{5,2}, ivec2{5,3}, std::string("padded") ),
			std::make_tuple<void(*)(GPU_MAT,GPU_MAT), ivec2, ivec2, std::string> ( &muloneFourth, ivec2{5,5}, ivec2{5,5}, std::string("0.25 * X") )
		};

		for(int i = 0; i < unaryOps.size(); i++) {
			void (*fun)(GPU_MAT, GPU_MAT);
			ivec2 inSize;
			ivec2 outSize;
			std::string fname;

			std::tie   (fun, inSize, outSize, fname) = unaryOps[i];
			runAndPrint(fun, inSize, outSize, fname);
		}

		std::vector< std::tuple<void(*)(GPU_MAT,GPU_MAT,GPU_MAT), ivec2, ivec2, ivec2, std::string> > binaryOps = {
			std::make_tuple<void(*)(GPU_MAT,GPU_MAT,GPU_MAT), ivec2, ivec2, ivec2, std::string> ( &add_host, ivec2{4,4}, ivec2{4,4}, ivec2{4,4}, std::string("add") )
		};

		GPU_MAT a = stackAllocateGPUMAT(200,200);
		CPU_MAT b = createmat(200,200);
		for(int i = 0; i < 40000; i++) b.data[i] = 100;
		copyCPUMATtoGPUMAT(b,a);
		std::cout << "This should be 4e+06: " << sum_host(a) << std::endl;
		delete[] b.data;
		mDeviceStack->pop(1);

		for(int i = 0; i < binaryOps.size(); i++) {
			void (*fun)(GPU_MAT, GPU_MAT, GPU_MAT);
			ivec2 inSize;
			ivec2 in2Size;
			ivec2 outSize;
			std::string fname;

			std::tie   (fun, inSize, in2Size, outSize, fname) = binaryOps[i];
			runAndPrint(fun, inSize, in2Size, outSize, fname);
		}

		CPU_MAT aa = createmat(5,4);
		randInitializeWeights(aa);
		std::cout << "input: " << std::endl;
		printCPUMAT(aa);
		GPU_MAT _aa = stackAllocateGPUMAT(5,4);
		copyCPUMATtoGPUMAT(aa,_aa);
		_aa = unpadOnes(_aa);
		copyGPUMATtoCPUMAT(_aa,aa);
		aa.cols = _aa.cols;
		std::cout << "unpadded: " << std::endl;
		printCPUMAT(aa);	
	}
}