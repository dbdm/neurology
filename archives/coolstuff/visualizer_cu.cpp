#include "visualizer_cu.h"
#include <helper_cuda.h>

namespace neuro {

	// clamp x to range [a, b]
	__device__ float clamp(float x, float a, float b) {
		return max(a, min(b, x));
	}

	__device__ int clamp(int x, int a, int b) {
		return max(a, min(b, x));
	}

	// convert floating point rgb color to 8-bit integer
	__device__ int rgbToInt(float r, float g, float b) {
		return (int(b)<<16) | (int(g)<<8) | int(r);
	}

	__global__ void cudaProcess(int numElements, unsigned int *g_odata, int imgw) {
		extern __shared__ uchar4 sdata[];

		int tx = threadIdx.x;
		int ty = threadIdx.y;
		int bw = blockDim.x;
		int bh = blockDim.y;
		int x = blockIdx.x*bw + tx;
		int y = blockIdx.y*bh + ty;

		uchar4 c4 = make_uchar4((x & 0x20)?100:0,0,(y & 0x20)?100:0,0);
		g_odata[y*imgw+x] = rgbToInt(c4.z, c4.y, c4.x);
	}



}