/*
 * io.h
 *
 *  Created on: Jan 23, 2016
 *      Author: Dani
 */

#ifndef IO_H_
#define IO_H_

#include "types.h"

#include <unordered_map>
#include <iostream>
#include <sstream>
#include <fstream>

namespace neuro {
	// Returns the byte offset if there is one and -1 if there isn't
	int getPacketOffs(char * beg, int numBytes, int packetSize, const char * cmp);
	int char4ToIntLE(char * beg); // Big Endian
	void printIOError(std::string fileName, std::string message);

	// The data is indexed (Training, Image) Output is 28*28 x [0, 1]
	// Output for the images are standard y + height * y
	Matrix loadMNISTImages(std::string fileName, unsigned int numImages = 0, unsigned int offs = 0);
	// The data is indexed (Training, Label) Output is 10 x (0 or 1)
	Matrix loadMNISTLabels(std::string fileName, unsigned int numLabels = 0, unsigned int offs = 0);

	inline std::string getNetworkString(std::string name) {
		return std::string("Networks/") + name + std::string(".net");	
	}
}



#endif /* IO_H_ */
