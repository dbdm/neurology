/*
 * neuralnet.cuh
 */
#ifndef SRC_NEURALNET_CU_CUH_
#define SRC_NEURALNET_CU_CUH_

#include <vector>
#include <iostream>

#include "matmath_cu.h"

namespace neuro {
	
	// Simply stores the data necessary for the device implementation of feed forward neural nets
	class GPUFeedForwardNeuralNetwork {
	public:
		GPU_MAT* weights; // This is an array stored on the host, but the GPU_MAT::data is stored on the device
		int inputSize, layers;
		
		GPUFeedForwardNeuralNetwork(std::vector<CPU_MAT> host_weights);
		
		GPUFeedForwardNeuralNetwork(): weights(nullptr), inputSize(0), layers(0) {}
		
		void train(GPU_MAT X, GPU_MAT Y);
		void train(CPU_MAT X, CPU_MAT Y);
		
		void classify(GPU_MAT X, int* out);
		int* classify(CPU_MAT X);
	};

	float feedforward_costfunc_host(GPU_MAT X, GPU_MAT Y, GPUFeedForwardNeuralNetwork* net);
	void feedforward_backprop_host(GPU_MAT X, GPU_MAT Y, GPUFeedForwardNeuralNetwork* net, GPU_MAT* grad);
	void feedforward_propagate_host(GPU_MAT X, GPUFeedForwardNeuralNetwork* net, GPU_MAT* z, GPU_MAT* a, GPU_MAT H);
	void feedforward_propagate_host(GPU_MAT X, GPUFeedForwardNeuralNetwork* net, GPU_MAT H);
	
} // namespace neuro

#endif /* SRC_NEURALNET_CUH_ */